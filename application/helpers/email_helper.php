<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package        CodeIgniter
 * @author        Sunil Khandade
 * @copyright           Copyright (c) 2016, Tenans.com
 * @since        Version 1.0
 */
if (!function_exists('customer_email_data')) {

    function customer_email_data($params) {
        $request_details = $params['request_details'];
        $email_type = $params['email_type'];
        $top_text = $params['top_text'];
        $action_text = $params['action_text'];
        $action_button_link = $params['action_button_link'];
        $action_button_text = $params['action_button_text'];

        $req_num = $request_details['req_num'];
        $user_fname = $request_details['user_fname'];
        $user_email = $request_details['user_email'];
        $ci = & get_instance();
        $ci->load->model('generic_model');
        $email_template = $ci->Generic_model->get_email_template(1, $email_type);
        $email_template['email_subject'] = str_replace("[SUBREFNUM]", $req_num, $email_template['email_subject']);
        $email_template['email_body'] = str_replace("[USERNAME]", $user_fname, $email_template['email_body']);
        $email_template['email_body'] = str_replace("[TOPTEXT]", $top_text, $email_template['email_body']);

        $message = request_details_email_body($params);
        $email_template['email_body'] = str_replace("[EMAILBODYWITHQUOTEDETAILS]", $message, $email_template['email_body']);
        $email_template['email_body'] = str_replace("[ACTIONTEXT]", $action_text, $email_template['email_body']);
        $email_template['email_body'] = str_replace("[ACTIONBUTTONLINK]", $action_button_link, $email_template['email_body']);
        $email_template['email_body'] = str_replace("[ACTIONBUTTONTEXT]", $action_button_text, $email_template['email_body']);

        $email_data = array(
            'email_subject' => $email_template['email_subject'],
            'email_body' => $email_template['email_body'],
            'email_to' => $user_email,
            'email_type' => $email_template['email_type']
        );
        return $email_data;
    }

}

if (!function_exists('customer_service_email_data')) {

    function customer_service_email_data($params) {
        $request_details = $params['request_details'];
        $email_type = $params['email_type'];
        $top_text = $params['top_text'];
        $action_text = $params['action_text'];
        $action_button_link = $params['action_button_link'];
        $action_button_text = $params['action_button_text'];

        $req_num = $request_details['req_num'];
        $user_fname = $request_details['user_fname'];
        $user_email = $request_details['user_email'];
        $ci = & get_instance();
        $ci->load->model('generic_model');
        $email_template = $ci->Generic_model->get_email_template(1, $email_type);
        $email_template['email_subject'] = str_replace("[SUBREFNUM]", $req_num, $email_template['email_subject']);
        $email_template['email_body'] = str_replace("[USERNAME]", $user_fname, $email_template['email_body']);
        $email_template['email_body'] = str_replace("[TOPTEXT]", $top_text, $email_template['email_body']);

        $params['emailtype'] = 'CUSTOMER';
        $message = service_details_email_body($params);
        $email_template['email_body'] = str_replace("[EMAILBODYWITHQUOTEDETAILS]", $message, $email_template['email_body']);
        $email_template['email_body'] = str_replace("[ACTIONTEXT]", $action_text, $email_template['email_body']);
        $email_template['email_body'] = str_replace("[ACTIONBUTTONLINK]", $action_button_link, $email_template['email_body']);
        $email_template['email_body'] = str_replace("[ACTIONBUTTONTEXT]", $action_button_text, $email_template['email_body']);

        $email_data = array(
            'email_subject' => $email_template['email_subject'],
            'email_body' => $email_template['email_body'],
            'email_to' => $user_email,
            'email_type' => $email_template['email_type']
        );
        return $email_data;
    }

}

if (!function_exists('partner_email_data')) {

    function partner_email_data($params) {
        $request_details = $params['request_details'];
        $email_type = $params['email_type'];
        $top_text = $params['top_text'];
        $action_text = $params['action_text'];
        $action_button_link = $params['action_button_link'];
        $action_button_text = $params['action_button_text'];

        $req_num = $request_details['req_num'];
        $bill_to = $request_details['bill_to'];
        $bill_to_email = $request_details['bill_to_email'];
        if ($bill_to == '') {
            $bill_to = $request_details['contact_person'];
        }
        if ($bill_to_email == '') {
            $bill_to_email = $request_details['email'];
        }

        $ci = & get_instance();
        $ci->load->model('generic_model');
        $email_template = $ci->Generic_model->get_email_template(1, $email_type);

        $message = request_details_email_body($params);
        $email_template['email_subject'] = str_replace("[SUBREFNUM]", $req_num, $email_template['email_subject']);
        $email_template['email_body'] = str_replace("[USERNAME]", $bill_to, $email_template['email_body']);
        $email_template['email_body'] = str_replace("[TOPTEXT]", $top_text, $email_template['email_body']);
        $email_template['email_body'] = str_replace("[EMAILBODYWITHQUOTEDETAILS]", $message, $email_template['email_body']);
        $email_template['email_body'] = str_replace("[ACTIONTEXT]", $action_text, $email_template['email_body']);
        $email_template['email_body'] = str_replace("[ACTIONBUTTONLINK]", $action_button_link, $email_template['email_body']);
        $email_template['email_body'] = str_replace("[ACTIONBUTTONTEXT]", $action_button_text, $email_template['email_body']);
        $email_data = array(
            'email_subject' => $email_template['email_subject'],
            'email_body' => $email_template['email_body'],
            'email_to' => $bill_to_email,
            'email_type' => $email_template['email_type']
        );
        return $email_data;
    }

}

if (!function_exists('partner_service_email_data')) {

    function partner_service_email_data($params) {
        $request_details = $params['request_details'];
        $email_type = $params['email_type'];
        $top_text = $params['top_text'];
        $action_text = $params['action_text'];
        $action_button_link = $params['action_button_link'];
        $action_button_text = $params['action_button_text'];

        $req_num = $request_details['req_num'];
        $bill_to = $request_details['contact_person'];
        $bill_to_email = $request_details['trade_email'];

        $ci = & get_instance();
        $ci->load->model('generic_model');
        $email_template = $ci->Generic_model->get_email_template(1, $email_type);

        $params['emailtype'] = 'PARTNER';
        $message = service_details_email_body($params);
        $email_template['email_subject'] = str_replace("[SUBREFNUM]", $req_num, $email_template['email_subject']);
        $email_template['email_body'] = str_replace("[USERNAME]", $bill_to, $email_template['email_body']);
        $email_template['email_body'] = str_replace("[TOPTEXT]", $top_text, $email_template['email_body']);
        $email_template['email_body'] = str_replace("[EMAILBODYWITHQUOTEDETAILS]", $message, $email_template['email_body']);
        $email_template['email_body'] = str_replace("[ACTIONTEXT]", $action_text, $email_template['email_body']);
        $email_template['email_body'] = str_replace("[ACTIONBUTTONLINK]", $action_button_link, $email_template['email_body']);
        $email_template['email_body'] = str_replace("[ACTIONBUTTONTEXT]", $action_button_text, $email_template['email_body']);
        $email_data = array(
            'email_subject' => $email_template['email_subject'],
            'email_body' => $email_template['email_body'],
            'email_to' => $bill_to_email,
            'email_type' => $email_template['email_type']
        );
        return $email_data;
    }

}

if (!function_exists('request_details_email_body')) {

    function request_details_email_body($params) {
        $request_details = $params['request_details'];
        $request_items = isset($params['request_items']) ? $params['request_items'] : '';
        $permalink = base_url() . 'properties/' . $request_details['permalink'];

        $message = '<table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="10%"> </td>
                            <td>
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border:1px solid #d6d6d6;border-radius:5px; box-shadow:0px 0px 4px 0px #cccccc;">
                                    <tr>
                                        <td>
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-top-left-radius:5px;border-top-right-radius:5px;background-color:#146880;">
                                                <tr>
                                                    <td width="5%"> </td>
                                                    <td>
                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td height="20"> </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="center" style="color:#ffffff;font-family:Verdana, Arial, sans-serif;font-size:18px;line-height:22px;">
                                                                ' . $request_details['addr_placeholder'] . '
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td height="20"> </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td width="5%"> </td>
                                                </tr>
                                            </table>
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="5%"> </td>
                                                    <td>
                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td height="20"> </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="color:#319fb5;font-family:Verdana, Arial, sans-serif;font-size:18px;line-height:26px; text-decoration: none;">
                                                                    <a href="' . $permalink . '" target="_blank" style="color:#319fb5;font-family:Verdana, Arial, sans-serif;font-size:16px;line-height:26px; text-decoration: none;">
                                                                    ' . $request_details['addr_placeholder'] . '
                                                                    </a></td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td style="color:#4f4f4f;font-family:Verdana, Arial, sans-serif;font-size:11px;line-height:18px">
                                                                                ' . $request_details['addr_placeholder'] . '
                                                                            </td>';
        if ($request_details['price'] > 0) {
            $message .= '<td align="right" style="color:#ff9305;font-family:Verdana, Arial, sans-serif;font-size:13px;line-height:18px">
                                                                                <strong>' . $request_details['currency_symbol'] . '' . $request_details['price'] . '</strong>
                                                                            </td>';
        }
        $message .= '                            </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td height="20" style="border-bottom:1px solid #a4a4a4"> </td>
                                                            </tr>
                                                            <tr>
                                                                <td height="20"> </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="color:#4f4f4f;font-family:Verdana, Arial, sans-serif;font-size:14px;line-height:20px">
                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td>Reference #</td>
                                                                            <td>: ' . $request_details['req_num'] . '</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td height="10"> </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Unit Name / Number</td>
                                                                            <td>: ' . $request_details['unit_name'] . ' / ' . $request_details['unit_num'] . '</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td height="10"> </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Rent</td>
                                                                            <td>: ' . $request_details['currency_symbol'] . '' . $request_details['price'] . '</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td height="10"> </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Available from</td>
                                                                            <td>: ' . date("d-M-Y", strtotime($request_details['from_date'])) . '</td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td height="20" style="border-bottom:1px solid #a4a4a4"> </td>
                                                            </tr>
                                                            <tr>
                                                                <td height="20"> </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="color:#4f4f4f;font-family:Verdana, Arial, sans-serif;font-size:11px;line-height:20px">
                                                                    Comments: ' . $request_details['comments'] . '
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td height="20"> </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td width="5%"> </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>';
        $message .= '</td>
                            <td width="10%"> </td>
                        </tr>
                    </table>';
        return $message;
    }

}

if (!function_exists('service_details_email_body')) {

    function service_details_email_body($params) {
        $request_details = $params['request_details'];
        $request_items = isset($params['request_items']) ? $params['request_items'] : '';
        $permalink = base_url() . 'service/' . $request_details['permalink'];

        $message = '<table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="10%"> </td>
                            <td>
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border:1px solid #d6d6d6;border-radius:5px; box-shadow:0px 0px 4px 0px #cccccc;">
                                    <tr>
                                        <td>
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-top-left-radius:5px;border-top-right-radius:5px;background-color:#146880;">
                                                <tr>
                                                    <td width="5%"> </td>
                                                    <td>
                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td height="20"> </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="center" style="color:#ffffff;font-family:Verdana, Arial, sans-serif;font-size:18px;line-height:22px;">Details:
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td height="20"> </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td width="5%"> </td>
                                                </tr>
                                            </table>
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="5%"> </td>
                                                    <td>';
        if ($params['emailtype'] == 'CUSTOMER') {
            $message .= '<table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td height="20"> </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="color:#319fb5;font-family:Verdana, Arial, sans-serif;font-size:18px;line-height:26px; text-decoration: none;">
                                                                    <a href="' . $permalink . '" target="_blank" style="color:#319fb5;font-family:Verdana, Arial, sans-serif;font-size:16px;line-height:26px; text-decoration: none;">
                                                                    ' . $request_details['trade_name'] . '
                                                                    </a></td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td style="color:#4f4f4f;font-family:Verdana, Arial, sans-serif;font-size:11px;line-height:18px">
                                                                                ' . $request_details['addr_placeholder'] . '
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td height="20" style="border-bottom:1px solid #a4a4a4"> </td>
                                                            </tr>
                                                            <tr>
                                                                <td height="20"> </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="color:#4f4f4f;font-family:Verdana, Arial, sans-serif;font-size:14px;line-height:20px">
                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td>Reference #</td>
                                                                            <td>: ' . $request_details['req_num'] . '</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td height="10"> </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Contact Person</td>
                                                                            <td>: ' . $request_details['contact_person'] . '</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td height="10"> </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Email</td>
                                                                            <td>: ' . $request_details['trade_email'] . '</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td height="10"> </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Phone</td>
                                                                            <td>: ' . $request_details['trade_phone'] . '</td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td height="20" style="border-bottom:1px solid #a4a4a4"> </td>
                                                            </tr>
                                                            <tr>
                                                                <td height="20"> </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="color:#4f4f4f;font-family:Verdana, Arial, sans-serif;font-size:11px;line-height:20px">
                                                                    Comments: ' . $request_details['comments'] . '
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td height="20"> </td>
                                                            </tr>
                                                        </table>';
        } else {
            $message .= '<table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td height="20"> </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="color:#4f4f4f;font-family:Verdana, Arial, sans-serif;font-size:14px;line-height:20px">
                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td>Reference #</td>
                                                                            <td>: ' . $request_details['req_num'] . '</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td height="10"> </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Customer Name</td>
                                                                            <td>: ' . $request_details['user_fname'] . '</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td height="10"> </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Customer Email</td>
                                                                            <td>: ' . $request_details['user_email'] . '</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td height="10"> </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Customer Phone</td>
                                                                            <td>: ' . $request_details['user_phone'] . '</td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td height="20" style="border-bottom:1px solid #a4a4a4"> </td>
                                                            </tr>
                                                            <tr>
                                                                <td height="20"> </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="color:#4f4f4f;font-family:Verdana, Arial, sans-serif;font-size:11px;line-height:20px">
                                                                    Comments: ' . $request_details['comments'] . '
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td height="20"> </td>
                                                            </tr>
                                                        </table>';
        }
        $message .= '</td>
                                                    <td width="5%"> </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>';
        $message .= '</td>
                            <td width="10%"> </td>
                        </tr>
                    </table>';
        return $message;
    }

}

if (!function_exists('customer_payment_email_data')) {

    function customer_payment_email_data($params) {
        $request_details = $params['request_details'];
        $email_type = $params['email_type'];

        $user_fname = $request_details['fname'];
        $user_email = $request_details['user_email'];

        $ci = & get_instance();
        $ci->load->model('generic_model');
        $email_template = $ci->Generic_model->get_email_template(1, $email_type);
        $email_template['email_body'] = str_replace("[USERNAME]", $user_fname, $email_template['email_body']);

        $message = payment_details_email_body($params);

        $email_template['email_body'] = str_replace("[EMAILBODYREPLACETEXT]", $message, $email_template['email_body']);
        $email_data = array(
            'email_subject' => $email_template['email_subject'],
            'email_body' => $email_template['email_body'],
            'email_to' => $user_email,
            'email_type' => $email_template['email_type']
        );
        return $email_data;
    }

}

if (!function_exists('partner_payment_email_data')) {

    function partner_payment_email_data($params) {
        $request_details = $params['request_details'];
        $email_type = $params['email_type'];

        $bill_to_email = $request_details['bill_to_email'];

        $ci = & get_instance();
        $ci->load->model('generic_model');
        $email_template = $ci->Generic_model->get_email_template(1, $email_type);

        $message = payment_details_email_body($params);

        $email_template['email_body'] = str_replace("[EMAILBODYREPLACETEXT]", $message, $email_template['email_body']);
        $email_data = array(
            'email_subject' => $email_template['email_subject'],
            'email_body' => $email_template['email_body'],
            'email_to' => $bill_to_email,
            'email_type' => $email_template['email_type']
        );
        return $email_data;
    }

}

if (!function_exists('payment_details_email_body')) {

    function payment_details_email_body($params) {
        $card_last4 = $params['payment_details']['last4_digit'];
        $paying_total = $params['payment_details']['amount_paid'];
        $due_total = $params['payment_details']['amount_due'];
        $date = $params['payment_details']['paid_on'];

        $payment_details = '<table>';
        $payment_details .= '<tr><td>Card Ending with: </td><td>' . $card_last4 . '</td></tr>';
        $payment_details .= '<tr><td>Amount Paid: </td><td>' . $paying_total . '</td></tr>';
        $payment_details .= '<tr><td>Amount Due: </td><td>' . $due_total . '</td></tr>';
        $payment_details .= '<tr><td>Paid on: </td><td>' . $date . '</td></tr>';
        $payment_details .= '</table>';
        return $payment_details;
    }

}