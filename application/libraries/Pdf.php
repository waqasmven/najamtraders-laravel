<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');



require_once dirname(__FILE__) . '/tcpdf/tcpdf_include.php';

class Pdf extends TCPDF {

    private $CI;

    function __construct() {
        //$this->CI =& get_instance();
        parent::__construct();
    }

    function a4($html, $pdfname, $retunoption = 'D') {
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        $pdf->SetMargins(10, 6, 10);
        $pdf->SetFooterMargin(4);

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Tenans.com');
        $pdf->SetTitle('Tenans Document');
        $pdf->SetSubject('Tenans Document');
        $pdf->SetKeywords('Tenans, Invoice, Purchase Order');

        //set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, 4);

        //set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        //set some language-dependent strings
        $pdf->setLanguageArray($l = null);

        $pdf->SetPrintHeader(false);
        $pdf->SetPrintFooter(false);

        $pdf->SetFont('helvetica', '', 10);
        $pdf->AddPage();

        $pdf->writeHTML($html, true, 0, true, 0);

        // output the HTML content
        // - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        // reset pointer to the last page
        $pdf->lastPage();

        // ---------------------------------------------------------
        //Close and output PDF document
        return $pdf->Output("$pdfname", "$retunoption");
    }

    function sla_master($html, $pdfname, $retunoption = 'D') {
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        $pdf->SetMargins(10, 6, 10);
        $pdf->SetFooterMargin(8);

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Tenans.com');
        $pdf->SetTitle('Tenans.com - Service Level Agreement');
        $pdf->SetSubject('Tenans.com - Service Level Agreement');
        $pdf->SetKeywords('Tenans, SLA, Contract');

        //set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, 15);

        //set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        //set some language-dependent strings
        $pdf->setLanguageArray($l = null);

        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        $PDF_HEADER_LOGO = ''; //base_url() . "theme/site/idream/img/logos/logo.png"; //any image file. check correct path.
        $PDF_HEADER_LOGO_WIDTH = "20";
        $PDF_HEADER_TITLE = "               Tenans.com - Service Level Agreement";
        $PDF_HEADER_STRING = "";
        $pdf->SetHeaderData($PDF_HEADER_LOGO, $PDF_HEADER_LOGO_WIDTH, $PDF_HEADER_TITLE, $PDF_HEADER_STRING);

        $pdf->SetPrintHeader(true);
        $pdf->SetPrintFooter(true);

        $pdf->SetFont('helvetica', '', 10);
        $pdf->AddPage();

        //$img = file_get_contents(base_url() . "theme/site/idream/img/logos/logo_high.png");
        //$pdf->Image('@' . $img, '', '', 60, 14, '', '', '', false, 300, '', false, false, 1, false, false, false);

        $pdf->writeHTML($html, true, 0, true, 0);

        // output the HTML content
        // - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        // reset pointer to the last page
        $pdf->lastPage();
        // ---------------------------------------------------------
        //Close and output PDF document
        return $pdf->Output("$pdfname", "$retunoption");
    }

}
