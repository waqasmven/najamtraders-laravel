<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin_con extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    function __construct() {
        parent::__construct();
        //$this->load->library('session');
        //$this->load->library('uri');
        $this->load->model('admin/Admin_model');
    }

    public function index() {
        $fullName = get_session_user_name();
        if ($fullName == '') {
            $data['heading'] = 'Login';
            $this->load->view('admin/signin', $data);
        } else {
            redirect(base_url('dashboard'));
        }
    }

    public function login() {
        $result = $this->Admin_model->login($this->input->post());
        if ($result['num_row'] > 0) {
            //print_r($result);die('Data reached Controller');
            //print_r($result);die;
            set_realnet_session_after_login($result);
            $data['status'] = "Success";
            $data['msg'] = "Congratulation, you are logged in.";
            $data['redirect'] = base_url('dashboard');
        } else {
            $data['status'] = 'error';
            $data['msg'] = 'You have entered invalid username or password';
        }
        print json_encode($data);
    }

    public function dashboard() {
        $fullName = get_session_user_name();
        if ($fullName == '') {
            redirect(base_url('admin'));
        } else {
            $data['main_page_data'] = $this->Admin_model->mainPageData();
            $data['heading'] = 'Dashboard';
            $this->load->view('admin/dashboard', $data);
        }
    }

    public function get_all_users() {
        $fullName = get_session_user_name();
        if ($fullName == '') {
            redirect(base_url('admin'));
        } else {
            //Pagination code
            $table_name = 'user_view';
            $base_url = base_url('admin/Admin_con/get_all_users');
            $total_rows = $this->Admin_model->get_count($table_name);
            $per_page = 9;
            $uri_segment = 4;

            $pagination = set_pagination($base_url, $total_rows, $per_page, $uri_segment);
            $page = $pagination['page'];
            $data['links'] = $pagination['links'];
            $data['users_data'] = $this->Admin_model->get_table_data_for_pagination_by_where($table_name, $per_page, $page, 1);
            $data['heading'] = 'Users List';

            $data['menu_id'] = 'view_all_user';
            $this->load->view('admin/users_list', $data);
        }
    }

    public function get_disabled_users() {
        $fullName = get_session_user_name();
        if ($fullName == '') {
            redirect(base_url('admin'));
        } else {
            //Pagination code
            $table_name = 'user_view';
            $base_url = base_url('admin/Admin_con/get_all_users');
            $total_rows = $this->Admin_model->get_count($table_name);
            $per_page = 9;
            $uri_segment = 4;

            $pagination = set_pagination($base_url, $total_rows, $per_page, $uri_segment);
            $page = $pagination['page'];
            $data['links'] = $pagination['links'];
            $data['users_data'] = $this->Admin_model->get_table_data_for_pagination_by_where($table_name, $per_page, $page, 0);
            $data['heading'] = 'Disabled User List';
            $data['menu_id'] = 'disabled_user';
            $this->load->view('admin/users_list', $data);
        }
    }

    public function addedit_user() {
        $fullName = get_session_user_name();
        if ($fullName == '') {
            redirect(base_url('admin'));
        } else {
            $id = $this->uri->segment(3);
            $data['user_type'] = $this->Admin_model->get_all_from_table('type');
            if ($id == '') {
                $data['heading'] = 'Add User';
                $this->load->view('admin/add_user', $data);
            } else {
                $data['user_data'] = $this->Admin_model->get_all_from_table_by_id('user_view', $id);
                $data['heading'] = 'Edit User';
                $this->load->view('admin/edit_user', $data);
            }
        }
    }

    public function enable_disable_user() {
        $fullName = get_session_user_name();
        if ($fullName == '') {
            redirect(base_url('admin'));
        } else {
            $data['user_type'] = $this->Admin_model->enable_disable_user($this->input->post('id'), $this->input->post('status'));
        }
    }

    public function adding_new_user() {
        $fullName = get_session_user_name();
        if ($fullName == '') {
            redirect(base_url('admin'));
        } else {
            // echo $arr['pic_title']; die;
            $arr = $this->input->post();

            $arr['pic_title'] = $arr['full_name'] . '_' . rand();
            // Start File operations
            $file_upload = isset($_FILES['userfile']['name']) ? $_FILES['userfile']['name'] : '';
            $orig_file_name = basename($file_upload);
            if ($orig_file_name != '') {
                $file_info = pathinfo($orig_file_name);
                $file_extension = $file_info['extension'];
                $new_file_name = str_replace(" ", "-", strtolower($arr['pic_title'])) . '-' . rand() . '.' . $file_extension;

                $desti = UPLOADPROFILEPICPATH . $new_file_name;
                @chmod($desti, 0755);
                if (!move_uploaded_file($_FILES['userfile']['tmp_name'], $desti)) {
                    echo 'File could not upload';
                    exit;
                }
                //end file upload
                $arr['new_pic_name'] = $desti;
                $result = $this->Admin_model->add_new_user($arr);
                if ($result > 0) {
                    $data['status'] = 'Success';
                    $data['msg'] = $arr['full_name'] . "  has been added successfully";
                }
            }
            print json_encode($data);
        }
    }

    public function edit_user_data() {
        $fullName = get_session_user_name();
        if ($fullName == '') {
            redirect(base_url('admin'));
        } else {
            // echo $arr['pic_title']; die;
            $arr = $this->input->post();

            $arr['pic_title'] = $arr['full_name'] . '_' . rand();
            // Start File operations
            $file_upload = isset($_FILES['userfile']['name']) ? $_FILES['userfile']['name'] : '';
            $orig_file_name = basename($file_upload);
            if ($orig_file_name != '') {
                $file_info = pathinfo($orig_file_name);
                $file_extension = $file_info['extension'];
                $new_file_name = str_replace(" ", "-", strtolower($arr['pic_title'])) . '-' . rand() . '.' . $file_extension;

                $desti = UPLOADPROFILEPICPATH . $new_file_name;
                @chmod($desti, 0755);
                if (!move_uploaded_file($_FILES['userfile']['tmp_name'], $desti)) {
                    echo 'File could not upload';
                    exit;
                }
                //end file upload

                $arr['new_pic_name'] = $desti;
            }
            $result = $this->Admin_model->edit_user($arr);
            if ($result > 0) {
                $data['status'] = 'Success';
                $data['msg'] = $arr['full_name'] . "  has been added successfully";
            }
            print json_encode($data);
        }
    }

    public function get_all_slide_show_data() {
        $fullName = get_session_user_name();
        if ($fullName == '') {
            redirect(base_url('admin'));
        } else {
            $data['slide_show_data'] = $this->Admin_model->get_all_slide_show_data();
            $data['heading'] = 'Slide Show';
            $this->load->view('admin/slide_show', $data);
        }
    }

    public function get_all_albums_data() {
        $fullName = get_session_user_name();
        if ($fullName == '') {
            redirect(base_url('admin'));
        } else {

            $data['heading'] = 'Slide Show';
            $this->load->view('admin/view_gallery', $data);
        }
    }

    public function get_all_gallery_data() {
        $fullName = get_session_user_name();
        if ($fullName == '') {
            redirect(base_url('admin'));
        } else {
            $data['albums_data'] = $this->Admin_model->get_all_albums_data();
            $data['gallery_data'] = $this->Admin_model->get_all_gallery_data();
            $data['heading'] = 'View Gallery';
            $this->load->view('admin/view_gallery', $data);
        }
    }

    public function delete_slide_show_pic() {

        $fullName = get_session_user_name();
        if ($fullName == '') {
            redirect(base_url('admin'));
        } else {
            $pic_id = $this->input->post('id');
            $result = $this->Admin_model->delete_slide_show_pic($pic_id);
            if ($result > 0) {
                $data['status'] = "Success";
                $data['msg'] = "Photo has been deleted successfully";
            }
        }
    }

    public function delete_album_by_id() {

        $fullName = get_session_user_name();
        if ($fullName == '') {
            redirect(base_url('admin'));
        } else {
            $album_id = $this->input->post('id');
            $result = $this->Admin_model->delete_album_by_id($album_id);
            if ($result > 0) {
                $data['status'] = "Success";
                $data['msg'] = "Album has been deleted successfully";
            }
        }
    }

    public function delete_gallery_photo_by_id() {

        $fullName = get_session_user_name();
        if ($fullName == '') {
            redirect(base_url('admin'));
        } else {
            $id = $this->input->post('id');
            $result = $this->Admin_model->delete_gallery_photo_by_id($id);
            if ($result > 0) {
                $data['status'] = "Success";
                $data['msg'] = "Photo has been deleted successfully";
            }
        }
    }

    public function update_main_page_text_data() {
        $fullName = get_session_user_name();
        if ($fullName == '') {
            redirect(base_url('admin'));
        } else {
            $result = $this->Admin_model->update_main_page_text_data($this->input->post());
            if ($result > 0) {
                $data['heading'] = 'Dashboard';
                $data['status'] = 'Success';
                $data['msg'] = 'Headings and their details is been updated';
                $data['redirect'] = base_url('dashboard');
            } else {
                $data['status'] = 'error';
                $data['msg'] = 'Something went wrong! Please try again';
            }
            print json_encode($data);
        }
    }

    public function insert_new_slide_show_pic() {
        $fullName = get_session_user_name();
        if ($fullName == '') {
            redirect(base_url('admin'));
        } else {

            //$data['main_page_data'] = $this->Admin_model->mainPageData();
            $data['heading'] = 'Insert New Slide Show Pic';
            $this->load->view('admin/insert_pic', $data);
        }
    }

    public function update_slide_show_pic() {
        $fullName = get_session_user_name();
        if ($fullName == '') {
            redirect(base_url('admin'));
        } else {
            $arr['pic_title'] = $this->input->post('pic_title');
            $arr['id'] = $this->input->post('pic_id');
            // Start File operations
            $file_upload = isset($_FILES['userfile']['name']) ? $_FILES['userfile']['name'] : '';
            $orig_file_name = basename($file_upload);
            if ($orig_file_name != '') {
                $file_info = pathinfo($orig_file_name);
                $file_extension = $file_info['extension'];
                $new_file_name = str_replace(" ", "-", strtolower($arr['pic_title'])) . '-' . rand() . '.' . $file_extension;

                $desti = UPLOADIMAGESPATH . $new_file_name;
                @chmod($desti, 0755);
                if (!move_uploaded_file($_FILES['userfile']['tmp_name'], $desti)) {
                    echo 'File could not upload';
                    exit;
                }
            }
            //end file upload
            $arr['new_pic_name'] = isset($new_file_name) ? $new_file_name : '';
            $arr['pic_path'] = isset($desti) ? $desti : '';
            $result = $this->Admin_model->update_slide_show_pic($arr);
            $data['status'] = 'Success';
            $data['msg'] = "Your image has been updated successfully";

            print json_encode($data);
        }
    }

    public function update_gallery_pic() {
        $fullName = get_session_user_name();
        if ($fullName == '') {
            redirect(base_url('admin'));
        } else {

            $arr['pic_title'] = $this->input->post('pic_title');
            $arr['id'] = $this->input->post('pic_id');
            // Start File operations
            $file_upload = isset($_FILES['userfile']['name']) ? $_FILES['userfile']['name'] : '';
            $orig_file_name = basename($file_upload);
            if ($orig_file_name != '') {
                $file_info = pathinfo($orig_file_name);
                $file_extension = $file_info['extension'];
                $new_file_name = str_replace(" ", "-", strtolower($arr['pic_title'])) . '-' . rand() . '.' . $file_extension;

                $desti = GALLERYIMAGESPATH . $new_file_name;
                @chmod($desti, 0755);
                if (!move_uploaded_file($_FILES['userfile']['tmp_name'], $desti)) {
                    echo 'File could not upload';
                    exit;
                }
            }
            //end file upload
            $arr['new_pic_name'] = isset($new_file_name) ? $new_file_name : '';
            $arr['pic_path'] = isset($desti) ? $desti : '';
            $result = $this->Admin_model->update_gallery_pic($arr);
            $data['status'] = 'Success';
            $data['msg'] = "Your image has been updated successfully";

            print json_encode($data);
        }
    }

    public function add_new_slide_pic() {
        $fullName = get_session_user_name();
        if ($fullName == '') {
            redirect(base_url('admin'));
        } else {
            $arr['pic_title'] = $this->input->post('pic_title');
            // Start File operations
            $file_upload = isset($_FILES['userfile']['name']) ? $_FILES['userfile']['name'] : '';
            $orig_file_name = basename($file_upload);
            if ($orig_file_name != '') {
                $file_info = pathinfo($orig_file_name);
                $file_extension = $file_info['extension'];
                $new_file_name = str_replace(" ", "-", strtolower($arr['pic_title'])) . '-' . rand() . '.' . $file_extension;

                $desti = UPLOADIMAGESPATH . $new_file_name;
                @chmod($desti, 0755);
                if (!move_uploaded_file($_FILES['userfile']['tmp_name'], $desti)) {
                    echo 'File could not upload';
                    exit;
                }
                //end file upload
                $arr['new_pic_name'] = $new_file_name;
                $result = $this->Admin_model->add_new_slide_pic($arr);
                $data['status'] = 'Success';
                $data['msg'] = "Your image has been updated successfully";
            }
            print json_encode($data);
        }
    }

    public function get_slide_show_pic_data($pic_id) {
        $fullName = get_session_user_name();
        if ($fullName == '') {
            redirect(base_url('admin'));
        } else {
            $pic_id = $this->uri->segment(4);
            $result = $this->Admin_model->get_slide_show_pic_detail($pic_id);
            if ($result > 0) {
                $data['msg'] = 'Headings and their details has been updated.';
                $data['main_page_data'] = $this->Admin_model->mainPageData();
                $img_link = base_url(SITETHEME . 'images/' . $result['pic_path']);
                $data['pic_data'] = $result;
                $data['heading'] = "Update Slide Show Pic ";
                $this->load->view('admin/update_slide_show_pic', $data);
            }
        }
    }

    public function get_gallery_pic_data($pic_id) {
        $fullName = get_session_user_name();
        if ($fullName == '') {
            redirect(base_url('admin'));
        } else {
            $pic_id = $this->uri->segment(4);
            $result = $this->Admin_model->get_gallery_pic_detail($pic_id);
            if ($result > 0) {
                $data['msg'] = 'Title and Pic has been updated.';
                $data['main_page_data'] = $this->Admin_model->mainPageData();
                $data['img_link'] = $result['pic_path'];
                $data['pic_data'] = $result;
                $data['heading'] = "Update Gallery Pic ";
                $this->load->view('admin/update_gallery_pic', $data);
            }
        }
    }

    public function logout() {
        $this->session->sess_destroy();
        redirect(base_url('admin'));
    }

    public function _404() {
        $this->output->set_status_header('404');
        $this->load->view('admin/404');
    }

}
