<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('common/Entity_model');
        $this->load->model('frontend/User_model');
        $this->load->model('common/Inquiry_model');
        $this->load->model('common/Generic_model');
    }

    public function index() {
        $user_id = $this->session->userdata('user_id');
        if ($user_id != '') {
            redirect('frontend/user/profile');
        } else {
            redirect('frontend/login');
        }
    }

    public function login() {
        $path = who_is_login();
        $user_id = $this->session->userdata('user_id');
        if ($path === false)
            $this->load->view('frontend/login');
        else if ($path === 'partner') {
            redirect('partner/dashboard');
        } else {
            redirect(base_url('dashboard'));
        }
    }

    public function forgot_password() {
        $user_id = $this->session->userdata('user_id');
        if ($user_id == '')
            $this->load->view('frontend/forgot_password');
        else
            redirect('home');
    }

    public function forgot_password_action() {
        $user_id = $this->session->userdata('user_id');
        //$this->load->library('recaptcha');
        if ($user_id == '') {
            $email = $this->input->post('email');
            //$recaptcha_response = $this->input->post('g-recaptcha-response');
            $recaptcha_response = true;
            if ($email != '' && $recaptcha_response != '') {
                /* $param = array(
                  'response' => $recaptcha_response,
                  'remoteip' => $this->input->ip_address()
                  ); */
                //$success = $this->recaptcha->verify_captcha($param);
                $success = 1;
                if ($success == 1) {
                    // Get User Details
                    $user_details = $this->User_model->get_user_details(array('email' => $email));
                    if (count($user_details) == 0) {
                        $data['status'] = 'error';
                        $data['msg'] = FORGOT_FAIL;
                    } else {
                        if ($user_details['status'] == 'DELETED') {
                            $data['status'] = 'error';
                            $data['msg'] = FORGOT_FAIL;
                        } else {
                            // Create a link
                            // Insert into Link Table
                            $link_hash = $user_details['hashval'];
                            $check_insert = $this->User_model->insert_user_profile_link('PASSWORD_RESET', $link_hash, $user_details['id']);
                            if ($check_insert != 0) {
                                // Insert successfull send email
                                $activation_link = base_url('/user/regenerate-password/' . $link_hash);
                                $email_template = $this->Generic_model->get_email_template(1, 'FORGOTPASSWORD');
                                // replace data in template
                                $email_template['email_body'] = str_replace("[LINK]", $activation_link, $email_template['email_body']);
                                // send email
                                $email_data = array(
                                    'email_subject' => $email_template['email_subject'],
                                    'email_body' => $email_template['email_body'],
                                    'email_to' => $user_details['email'],
                                    'email_type' => $email_template['email_type']
                                );
                                $mail_status = send_mail($email_data);
                                echo $mail_status;
                                die;
                                if ($mail_status) {
                                    $data['status'] = 'success';
                                    $data['msg'] = FORGOT_MSG;
                                    $data['redirect'] = base_url();
                                } else {
                                    $data['status'] = 'error';
                                    $data['msg'] = WENT_WRONG;
                                }
                            } else {
                                $data['status'] = 'error';
                                $data['msg'] = WENT_WRONG;
                            }
                        }
                    }
                } else {
                    $data['status'] = 'error';
                    $data['msg'] = FORGOT_FAIL;
                }
            } else {
                $data['status'] = 'error';
                $data['msg'] = FORGOT_FAIL;
            }
            print json_encode($data);
        } else {
            redirect('home');
        }
    }

    public function logout() {
        $this->load->model('common/Generic_model');
        $this->Generic_model->activity_log(array(
            'modified_by_id' => get_session_user_id(),
            'module' => 'USER',
            'activity' => 'LOGOUT',
            'data' => ''
        ));
        $this->session->sess_destroy();
        set_success_message(LOGOUT_SUCCESS);
        redirect(base_url());
    }

    public function register() {
        $user_id = $this->session->userdata('user_id');
        if ($user_id == '')
            $this->load->view('frontend/register');
        else
            redirect('home');
    }

    public function add_property() {
        $user_id = $this->session->userdata('user_id');
        if ($user_id == '')
            $this->load->view('frontend/add_property');
        else
            redirect('home');
    }

    public function refer($referrer = '') {
        if ($referrer != '') {
            set_cookie('referrer_id', $referrer, '86400');
        }
        $this->load->view('frontend/login');
    }

    public function user_login($params = array()) {
        $this->load->library('user_agent');
        $email = $this->input->post('email');
        $pass = $this->input->post('password');
        // This condition is to login user after registration
        if ($email == '') {
            $email = $this->input->post('reg_email');
        }
        if ($pass == '') {
            $pass = $this->input->post('pwd');
        }

        $user_input = array(
            'email' => $email,
            'pass' => $pass
        );

        if ($email != '' && $pass != '') {
            $result = $this->User_model->login($user_input);
            if ($result['num_row'] == 1) {
                if ($result['user_type'] == 'USER' || $result['user_type'] == 'PARTNER') {
                    // Set Session
                    set_user_session_after_login($this, $result);
                    // First time login Handler
                    if ($result['last_login'] == '0000-00-00 00:00:00') {
                        $data['redirect'] = base_url('frontend/user/profile');
                    } else {
                        $data['redirect'] = $this->session->redirect_to !== null ?
                                base_url($this->session->redirect_to) :
                                $this->agent->referrer();
                    }

                    $this->User_model->update_user_login_time($result['id']);
                    log_activity(get_session_user_id(), 'USER', 'LOGIN', '', getIP());
                    $data['status'] = 'success';
                    $data['msg'] = LOGIN_SUCC_MSG;
                } else {
                    // Set Session
                    set_realnet_session_after_login($result);
                    // First time login Handler

                    $this->User_model->update_user_login_time($result['id']);
                    log_activity(get_session_user_id(), 'USER', 'LOGIN', '', getIP());
                    //$data['redirect'] = base_url('partner/dashboard');
                    $data['redirect'] = base_url('/realnet/realnet-home');
                    $data['user_type'] = 'NOT_CUST';
                    $data['status'] = 'success';
                    $data['msg'] = LOGIN_SUCC_MSG;
                    print json_encode($data);
                    exit();
                }
            } else {
                $data['status'] = 'error';
                $data['msg'] = LOGIN_FAIL_MSG;
            }
        } else {
            $data['status'] = 'error';
            $data['msg'] = LOGIN_FAIL_MSG;
        }
        print json_encode($data);
    }

    public function get_user_details($params) {
        $userData = $this->User_model->login($params);
        return $userData;
    }

    public function get_all_user_details_without_login($params) {
        $userData = $this->User_model->userData($params);
        return $userData;
    }

    public function check_email() {
        $res = $this->User_model->checkEmail($this->input->post('email'));

        if ($res['num_row'] == 0) {
            //print_r(json_encode(array('email'=>true)));
            print_r(json_encode(true));
        } else {
            //print_r(json_encode(array('email'=>false)));
            print_r(json_encode(false));
        }
    }

    public function user_register($user_email = null) {
        $this->load->library('user_agent');
        $gen_user_id = '';
        $captcha_success = 1;
        if ($captcha_success == 1) {
            $referrer_id = get_cookie('referrer_id');
            if ($referrer_id == NULL) {
                $referrer_id = '';
            }
            delete_cookie('referrer_id');
            $fname = $this->input->post('fname');
            $lname = $this->input->post('lname');
            $email = $this->input->post('email');
            $phone = $this->input->post('phone');
            $password = $this->input->post('pwd');
            if ($email == '') {
                $email = $user_email;
            }
            $is_service_provider = $this->input->post('is_service_provider');
            $user_type = 'USER';
            if ($is_service_provider != '') {
                $user_type = 'PARTNER';
            }
            if ($email != '') {
                $user_array = array(
                    "fname" => $fname,
                    "lname" => $lname,
                    "email" => $email,
                    "phone" => $phone,
                    "user_type" => $user_type,
                    "referredby_id" => $referrer_id,
                    "password" => $password
                );
                // Insert
                $gen_user_id = $this->User_model->register($user_array);
                if ($gen_user_id != 0) {
                    // update user__master with hashval
                    //$user_hash = get_random_md5($gen_user_id);
                    //$this->User_model->update_user_hashval($user_hash, $gen_user_id);
                    // insert into activity log
                    log_activity($gen_user_id, 'USER', 'REGISTER', '', getIP());

                    $email_template = $this->Generic_model->get_email_template(1, 'REGISTRATION');
                    // replace data in template
                    $email_template['email_body'] = str_replace("[USERNAME]", $fname, $email_template['email_body']);
                    // send email
                    $email_data = array(
                        'email_subject' => $email_template['email_subject'],
                        'email_body' => $email_template['email_body'],
                        'email_to' => $email,
                        'email_type' => $email_template['email_type']
                    );
                    send_mail($email_data);

                    $login_arr['email'] = $email;
                    $login_arr['pass'] = $password;
                    $result = $this->User_model->login($login_arr);
                    if ($result['num_row'] == 1) {
                        if ($result['user_type'] == 'USER' || $result['user_type'] == 'PARTNER') {
                            // Set Session
                            set_user_session_after_login($this, $result);
                            // First time login Handler
                            if ($result['last_login'] == '0000-00-00 00:00:00') {
                                $data['redirect'] = base_url('frontend/user/profile');
                            } else {
                                $data['redirect'] = $this->session->redirect_to !== null ?
                                        base_url($this->session->redirect_to) :
                                        $this->agent->referrer();
                            }

                            $this->User_model->update_user_login_time($result['id']);
                            log_activity(get_session_user_id(), 'USER', 'LOGIN', '', getIP());
                            $data['status'] = 'success';
                            $data['msg'] = LOGIN_SUCC_MSG;
                        } else {
                            // Set Session
                            set_realnet_session_after_login($result);
                            // First time login Handler

                            $this->User_model->update_user_login_time($result['id']);
                            log_activity(get_session_user_id(), 'USER', 'LOGIN', '', getIP());
                            //$data['redirect'] = base_url('partner/dashboard');
                            $data['redirect'] = base_url('/realnet/realnet-home');
                            $data['user_type'] = 'NOT_CUST';
                            $data['status'] = 'success';
                            $data['msg'] = REG_SUCC_MSG;
                            print json_encode($data);
                            exit();
                        }
                    }

                    $data['status'] = 'success';
                    $data['msg'] = REG_SUCC_MSG;
                    $data['redirect'] = base_url();
                } else {
                    $data['status'] = 'error';
                    $data['msg'] = REG_FAIL_MSG;
                }
            } else {
                $data['status'] = 'error';
                $data['msg'] = REG_FAIL_MSG;
            }
        } else {
            $data['status'] = 'error';
            $data['msg'] = REG_FAIL_MSG;
        }
        if ($email != '') {
            print json_encode($data);
        } else {
            return $gen_user_id;
        }
    }

    public function activation($link_hash) {
        if ($link_hash != '') {
            $user_id_arr = $this->User_model->get_profile_link('ACTIVATION', $link_hash);
            $user_id_arr = isset($user_id_arr[0]) ? $user_id_arr[0] : '';
            if (isset($user_id_arr['user_id'])) {
                // update user_master and link table
                $this->User_model->user_status_change('ACTIVE', $user_id_arr['user_id']);
                $this->User_model->update_user_profile_link('INACTIVE', $link_hash, $user_id_arr['user_id']);
                log_activity($user_id_arr['user_id'], 'USER', 'ACTIVATION', '', getIP());
                set_success_message(ACT_SUCC_MSG);
            } else {
                set_error_message(ACT_FAIL_MSG);
            }
            redirect(base_url('user/activation-successful'));
        } else {
            redirect(base_url('404'));
        }
    }

    public function activation_successful() {
        $user_id = $this->session->userdata('user_id');
        if ($user_id == '')
            $this->load->view('frontend/user/activation_successful');
        else
            redirect('home');
    }

    public function regenerate_password($link_hash) {
        if ($link_hash != '') {
            $user_id_arr = $this->User_model->get_profile_link('PASSWORD_RESET', $link_hash);
            if (isset($user_id_arr[0])) {
                $user_id_arr = $user_id_arr[0];
                if (isset($user_id_arr['user_id'])) {
                    // update user_master and link table
                    $data['hash'] = $link_hash;
                    $data['user_id'] = $user_id_arr['user_id'];
                    $this->load->view('frontend/set_new_password', $data);
                } else {
                    set_error_message(ACT_FAIL_MSG);
                }
            } else {
                redirect(base_url('404'));
            }
        } else {
            redirect(base_url('404'));
        }
    }

    public function regenerate_password_action() {
        $user_id = $this->input->post('userid');
        $hash = $this->input->post('hashval');
        $password = $this->input->post('new_pass');
        $confirmpassword = $this->input->post('newconfpass');
        if ($password == $confirmpassword) {
            // update user_master and link table
            $result = $this->User_model->change_reset_password(array(
                "user_id" => $user_id,
                "hash" => $hash,
                "is_reset" => 'TRUE',
                "old_pass" => '',
                "new_pass" => $password
            ));
            if ($result['status'] == 'success') {
                $this->User_model->update_user_profile_link('INACTIVE', $hash, $user_id);
                $data['status'] = 'success';
                $data['msg'] = CHANGEPASS_SUCCESS;
                $user_array = $this->User_model->get_user_details(array('id' => $user_id));
                if ($user_array['status'] == 'ACTIVE') {
                    $data['redirect'] = base_url();
                } else {
                    $data['redirect'] = base_url('user/activation/' . $hash);
                }
            } else {
                $data['status'] = 'error';
                $data['msg'] = WENT_WRONG;
            }
        } else {
            $data['status'] = 'error';
            $data['msg'] = CHANGEPASS_FAILURE;
        }
        print json_encode($data);
    }

    public function profile() {
        if (is_login($this)) {
            $user_array = $this->User_model->get_user_details(array(
                'id' => $this->session->userdata('user_id')
            ));
            /* @var $user_array type */
            if (empty($user_array)) {
                redirect('404');
            }
            $data['heading'] = 'My Profile';
            $data['user_array'] = $user_array;
            $this->load->view('frontend/dashboard/profile', $data);
        }
    }

    public function profile_save() {
        if (is_login($this)) {
            $user_array = $this->User_model->get_user_details(array(
                'id' => $this->session->userdata('user_id')
            ));
            /* @var $user_array type */
            if (empty($user_array)) {
                redirect('404');
            }
            //File Upload
            $profile_pic_destination = '';
            $user_name = strtok($this->session->userdata('user_fullname'), " ");
            $logo_upload = isset($_FILES['photo']['name']) ? $_FILES['photo']['name'] : '';
            $orig_file_name = basename($logo_upload);
            if ($orig_file_name != '') {
                $file_info = pathinfo($orig_file_name);
                $file_extension = $file_info['extension'];
                $new_file_name = str_replace(" ", "-", strtolower($user_name . '_profilePic')) . '-' . rand() . '.' . $file_extension;

                $profile_pic_destination = 'uploads/profile-images/' . $new_file_name;
                @chmod($profile_pic_destination, 0755);
                if (!@move_uploaded_file($_FILES['photo']['tmp_name'], $profile_pic_destination)) {
                    echo 'File could not upload';
                    exit;
                }
            }
            $param = $this->input->post();
            // File upload finished
            $param['photo'] = $new_file_name;

            $res = $this->User_model->update_user_profile($param);
            if ($res['user_id'] != '') {
                $data['status'] = 'success';
                $data['msg'] = 'Profile updated successfully';
                $data['redirect'] = base_url('dashboard');
                log_activity(get_session_user_id(), 'USER', 'PROFILE_UPDATE', '', getIP());
            } else {
                $data['status'] = 'error';
                $data['msg'] = 'Sorry! We could not update your profile!';
                $data['redirect'] = base_url('dashboard');
            }
        }
        print json_encode($data);
    }

    public function change_password() {
        if (is_login($this)) {
            $user_array = $this->User_model->get_user_details(array(
                'id' => $this->session->userdata('user_id')
            ));
            /* @var $user_array type */
            if (empty($user_array)) {
                redirect('404');
            }
            $data['heading'] = "Change Password";
            $data['user_array'] = $user_array;
            $this->load->view('frontend/user/change_password', $data);
        }
    }

    public function update_password() {
        if (is_login($this)) {
            $user_array = $this->User_model->get_user_details(array(
                'id' => $this->session->userdata('user_id')
            ));
            /* @var $user_array type */
            if (empty($user_array)) {
                redirect('404');
            }
            $res = $this->User_model->change_reset_password($this->input->post());
            if ($res['status'] == 'success') {
                $data['status'] = $res['status'];
                $data['msg'] = $res['msg'];
                $data['redirect'] = base_url('user/profile');
                log_activity(get_session_user_id(), 'USER', 'PASSWORD_CHANGE', '', getIP());
            } else {
                $data['status'] = $res['status'];
                $data['msg'] = $res['msg'];
                $data['redirect'] = base_url('dashboard');
            }
        }
        print json_encode($data);
    }

    public function dashboard() {
        if (is_login($this)) {
          $user_array = $this->User_model->get_user_details(array(
          'id' => $this->session->userdata('user_id')
          ));
          /* @var $user_array type */
          if (empty($user_array)) {
          redirect('404');
          }
          $data['user_array'] = $user_array;
          $data['heading'] = 'Dashboard';
          $this->load->view('frontend/dashboard/dashboard', $data);
          }
    }

    public function autoredirect() {
        $redirect = $this->input->get('redirect');
        $encrypted_id = $this->input->get('p');
        $decrypted_id_raw = base64_decode($encrypted_id);
        $params['id'] = preg_replace(sprintf('/%s/', SALT), '', $decrypted_id_raw);
        $user_details = $this->User_model->get_user_details($params);

        if ($user_details['user_type'] == 'USER') {
            // Set Session
            set_user_session_after_login($this, $user_details);
        } else {
            // Set Session
            set_realnet_session_after_login($user_details);
        }
        $this->User_model->update_user_login_time($user_details['id']);
        log_activity(get_session_user_id(), 'USER', 'LOGIN', '', getIP());
        redirect($redirect);
        exit();
    }

    public function send_enquiry() {
        $this->load->model('common/generic_model');
        $user_id = $this->input->post('user_id');
        if ($user_id == '') {
            $userparams['fname'] = $this->input->post('fname');
            $userparams['email'] = $this->input->post('email');
            $userparams['password'] = $this->input->post('password');
            $userparams['user_type'] = 'USER';
            $userparams['phone'] = '';
            $userparams['referredby_id'] = '';
            $gen_user_id = $this->User_model->register($userparams); // Register user as a guest
        }

        $params = $this->input->post();
        $params['created_by'] = $user_id;
        $result = $this->Entity_model->send_enquiry($params);
        if ($result['req_hashval']) {
            $request_details = $this->Inquiry_model->get_customer_requests_details($result['req_hashval']);
            // Send Email to Customer
            $cust_params['request_details'] = $request_details;
            $req_num = $request_details['req_num'];
            $cust_params['email_type'] = 'REQ_SENT_CUST';
            $cust_params['top_text'] = 'Thank you for choosing Tenans.com! We’ve sent your viewing request to the Property owner. Meanwhile, Please review your request details for #' . $req_num . ' below:';
            $cust_params['action_text'] = 'While we are working to set up your viewing, You can explore more properties on Tenans.com<br /><br /> Want to see more properties?';

            $ab_link = base_url();
            $cust_params['action_button_link'] = $ab_link;
            $cust_params['action_button_text'] = 'Explore Tenans.com!';
            $email_data = customer_email_data($cust_params);
            send_mail($email_data);

            // Send Email to Venue provider
            $partner_params['request_details'] = $request_details;
            $partner_params['email_type'] = 'REQ_SENT_CUST_PARTNER';
            $partner_params['top_text'] = 'You just received a viewing request from a Tenans customer! Please review the request details for Ref. #' . $req_num . ' below:';
            $partner_params['action_text'] = 'Once you’re ready to confirm the request and deliver your quote to the interested customer, click the button below. <br />Please confirm or deny this request within 24 hours as per Tenans.com regulations.';
            $ab_link = base_url('realnet/realnet-requests/view-request/' . $request_details['req_hashval']);
            $encrypted_id = base64_encode($request_details['partner_user_id'] . SALT);
            $partner_params['action_button_link'] = base_url('user/autoredirect?p=' . $encrypted_id . '&redirect=' . $ab_link);
            $partner_params['action_button_text'] = 'Send Quotation';
            $email_data_v = partner_email_data($partner_params);
            send_mail($email_data_v);

            $data = '';
            $data = array(
                'status' => 'succsss',
                'redirect' => base_url(),
                'msg' => 'Thank you! Your inquiry is been sent to Property Owner.'
            );
        } else {
            $data = array(
                'status' => 'error',
                'redirect' => '',
                'msg' => 'Sorry! We cound not send your inquiry!'
            );
        }
        print_r(json_encode($data));
    }

    public function send_service_inquiry() {
        $this->load->model('common/generic_model');
        $user_id = $this->input->post('user_id');
        if ($user_id == '') {
            $userparams['fname'] = $this->input->post('fname');
            $userparams['email'] = $this->input->post('email');
            $userparams['password'] = $this->input->post('password');
            $userparams['user_type'] = 'USER';
            $userparams['phone'] = $this->input->post('phone');
            $userparams['referredby_id'] = '';
            $user_id = $this->User_model->register($userparams); // Register user as a guest
        }

        $params = $this->input->post();
        $params['user_id'] = $user_id;
        $result = $this->Entity_model->send_service_inquiry($params);
        if ($result['req_hashval']) {
            $request_details = $this->Inquiry_model->get_customer_requests_details($result['req_hashval']);
            // Send Email to Customer
            $cust_params['request_details'] = $request_details;
            $req_num = $request_details['req_num'];
            $cust_params['email_type'] = 'SERVICE_REQ_CUST';
            $cust_params['top_text'] = 'Thank you for choosing Tenans.com! We’ve sent your service request to the Service Provider. Meanwhile, Please review your inquiry details for #' . $req_num . ' below:';
            $cust_params['action_text'] = 'Service provider will be in touch with you shortly. We recommend to call or email for quick response.<br /><br /> Want to see more properties?';

            $ab_link = base_url();
            $cust_params['action_button_link'] = $ab_link;
            $cust_params['action_button_text'] = 'Explore Tenans.com!';
            $email_data = customer_service_email_data($cust_params);
            send_mail($email_data);

            // Send Email to Venue provider
            $partner_params['request_details'] = $request_details;
            $partner_params['email_type'] = 'SERVICE_REQ_PARTNER';
            $partner_params['top_text'] = 'You just received a service inquiry from a Tenans customer! Please review the request details for Ref. #' . $req_num . ' below:';
            $partner_params['action_text'] = 'Check with the customer for more details.';
            $ab_link = base_url('realnet/realnet-requests/view-request/' . $request_details['req_hashval']);
            $encrypted_id = base64_encode($request_details['trade_id'] . SALT);
            $partner_params['action_button_link'] = base_url('user/autoredirect?p=' . $encrypted_id . '&redirect=' . $ab_link);
            $partner_params['action_button_text'] = 'View Request';
            $email_data_v = partner_service_email_data($partner_params);
            send_mail($email_data_v);

            $data = '';
            $data = array(
                'status' => 'succsss',
                'redirect' => base_url(),
                'msg' => 'Thank you! Your inquiry is been sent to Property Owner.'
            );
        } else {
            $data = array(
                'status' => 'error',
                'redirect' => '',
                'msg' => 'Sorry! We cound not send your inquiry!'
            );
        }
        print_r(json_encode($data));
    }

    public function sent_requests() {
        /* Default User Page */
        if (is_login($this)) {
            $user_array = $this->User_model->get_user_details(array(
                'id' => $this->session->userdata('user_id')
            ));
            /* @var $user_array type */
            if (empty($user_array)) {
                redirect('404');
            }
            $data['heading'] = "Sent Requests";
            $data['sent_requests'] = $this->Inquiry_model->get_customer_requests();
            $this->load->view('frontend/user/sent_requests', $data);
        }
    }

    public function sent_request_details() {
        $request_hash = $this->uri->segment(3);
        /* Default User Page */
        if (is_login($this)) {
            $user_array = $this->User_model->get_user_details(array(
                'id' => $this->session->userdata('user_id')
            ));
            /* @var $user_array type */
            if (empty($user_array)) {
                redirect('404');
            }
            $data['request_details'] = $this->Inquiry_model->get_customer_requests_details($request_hash);
            $entity_hashval = $data['request_details']['entity_hashval'];
            $entity_id = $data['request_details']['entity_id'];
            $data['chat_data'] = $this->User_model->my_chat($data['request_details']);
            $data['request_items'] = $this->Inquiry_model->get_customer_requests_items($request_hash);
            $data['entity_details'] = $this->Entity_model->get_entity_details($entity_id);
            $data['entity_policy'] = $this->Entity_model->get_entity_policy($entity_hashval);
            $data['heading'] = 'Sent Request Detail';
            $this->load->view('frontend/user/sent_requests_details', $data);
        }
    }

    public function withdraw_request_cust() {
        /* Default User Page */
        if (is_login($this)) {
            $user_array = $this->User_model->get_user_details(array(
                'id' => $this->session->userdata('user_id')
            ));
            /* @var $user_array type */
            if (empty($user_array)) {
                redirect('404');
            }
            $data['heading'] = 'Quotation Details';
            $data['user_array'] = $user_array;
            $data['side_active_nav_id'] = 'user_requests';
            $data['top_active_nav_id'] = '';

            $result = $this->Inquiry_model->update_request_process_status($this->input->post());
            if ($result['request_hashval']) {
                $request_hashval = $result['request_hashval'];
                $request_details = $this->Inquiry_model->get_customer_requests_details($request_hashval);

                // Send Email to Customer
                $cust_params['email_type'] = 'REQUEST_WITHDRAWN_CUST';
                $cust_params['request_details'] = $request_details;
                $cust_params['top_text'] = 'You have withdrawn your enquiry. Please find the request details below:';
                $cust_params['action_text'] = 'Want to explore more venues? Please click on below button to search!';
                $cust_params['action_button_link'] = base_url();
                $cust_params['action_button_text'] = 'Find Venues';
                $email_data = customer_email_data($cust_params);
                send_mail($email_data);

                // Send Email to Venue provider
                $partner_params['email_type'] = 'REQUEST_WITHDRAWN_CUST_PARTNER';
                $partner_params['request_details'] = $request_details;
                $partner_params['top_text'] = 'You have rejected the customer enquiry for venue. Please find the request details below:';
                $partner_params['action_text'] = 'Click on below button to view all other pending requests! ';
                $ab_link = base_url('realnet/realnet-requests/list-pending-requests/');
                $encrypted_id = base64_encode($request_details['user_id'] . SALT);
                $partner_params['action_button_link'] = base_url('user/autoredirect?p=' . $encrypted_id . '&redirect=' . $ab_link);
                $partner_params['action_button_text'] = 'Pending Requests';
                $email_data_v = partner_email_data($partner_params);
                send_mail($email_data_v);

                $data['status'] = 'succsss';
                $data['msg'] = 'Your request is withdrawn!';
                $data['redirect'] = base_url('user/sent-requests');
            } else {
                $data['status'] = 'error';
                $data['msg'] = 'Sorry! Counld not withdraw your request';
            }
            print json_encode($data);
        }
    }

    public function quotes_received() {
        /* Default User Page */
        if (is_login($this)) {
            $user_array = $this->User_model->get_user_details(array(
                'id' => $this->session->userdata('user_id')
            ));
            /* @var $user_array type */
            if (empty($user_array)) {
                redirect('404');
            }
            $data['heading'] = "Quote Received";
            $this->load->model('frontend/request/Inquiry_model');
            $data['quotes_received'] = $this->Inquiry_model->get_quote_received_cust();
            $this->load->view('frontend/user/quotes_received', $data);
        }
    }

    public function quote_details() {
        $request_hash = $this->uri->segment(3);
        /* Default User Page */
        if (is_login($this)) {
            $user_array = $this->User_model->get_user_details(array(
                'id' => $this->session->userdata('user_id')
            ));
            /* @var $user_array type */
            if (empty($user_array)) {
                redirect('404');
            }
            $data['request_details'] = $this->Inquiry_model->get_customer_requests_details($request_hash);
            $data['chat_data'] = $this->User_model->my_chat($data['request_details']);
            $entity_hashval = $data['request_details']['entity_hashval'];
            $entity_id = $data['request_details']['entity_id'];
            $data['request_items'] = $this->Inquiry_model->get_customer_requests_items($request_hash);
            $data['entity_details'] = $this->Entity_model->get_entity_details($entity_id);
            $data['entity_policy'] = $this->Entity_model->get_entity_policy($entity_hashval);
            $data['my_chat_id'] = $this->session->userdata('user_id');
            $data['heading'] = "Quote Detail";

            $data['entity_data'] = isset($data['entity_details'][0]) ? $data['entity_details'][0] : '';
            $data['entity_images'] = $this->Entity_model->get_entity_images($data['entity_data']['entity_id']);
            //print_r($data['entity_images']);die;
            $this->load->view('frontend/user/quote_details', $data);
        }
    }

    public function my_chat_message() {
        $param = $this->input->post();
//       print_r($param);die;
        if ($param) {
            $this->User_model->insert_chat($param);
            $data['status'] = 'succsss';
            $data['msg'] = 'Your message has been sent successfully!';
        } else {
            $data['status'] = 'error';
            $data['msg'] = 'Sorry! Something went wrong';
        }
        print json_encode($data);
    }

    public function quote_reject_cust() {
        /* Default User Page */
        if (is_login($this)) {
            $user_array = $this->User_model->get_user_details(array(
                'id' => $this->session->userdata('user_id')
            ));
            /* @var $user_array type */
            if (empty($user_array)) {
                redirect('404');
            }
            $result = $this->Inquiry_model->update_request_process_status($this->input->post());
            if ($result['request_hashval']) {
                $request_hashval = $result['request_hashval'];
                $request_details = $this->Inquiry_model->get_customer_requests_details($request_hashval);
                $request_items = $this->Inquiry_model->get_customer_requests_items($request_hashval);

                // Send Email to Customer
                $cust_params['request_details'] = $request_details;
                $cust_params['request_items'] = $request_items;
                $cust_params['email_type'] = 'QUOTE_REJECT_CUST';
                $cust_params['top_text'] = 'You have rejected the quote sent by Venue provider. Please find the quote details below:';
                $cust_params['action_text'] = 'That&#39;s not a problem. We&#39;ve got matching venues for you!!';
                $entity_data = $this->Entity_model->get_entity_details($request_details['entity_hashval']);
                $search_key = $entity_data[0]['city_name'] . ', ' . $entity_data[0]['state_iso'] . ', ' . $entity_data[0]['country_name'];
                $lati = $entity_data[0]['lati'];
                $longi = $entity_data[0]['longi'];
                $eventdate = $request_details['from_date'];
                $ab_link = base_url('search') . '/?search_key=' . $search_key . '&lat=' . $lati . '&long=' . $longi . '&eventdate=' . $eventdate;
                $cust_params['action_button_link'] = $ab_link;
                $cust_params['action_button_text'] = 'Find Similar Venues';

                $email_data = customer_email_data($cust_params);
                send_mail($email_data);

                // Send Email to Venue provider
                $partner_params['request_details'] = $request_details;
                $partner_params['request_items'] = $request_items;
                $partner_params['email_type'] = 'QUOTE_REJECT_CUST_PARTNER';
                $partner_params['top_text'] = 'Customer has rejected your quote. Please find the quote details below:';
                $partner_params['action_text'] = 'We are working hard to get you more enquiries!';
                $ab_link = base_url('realnet/realnet-requests/list-pending-requests/');
                $encrypted_id = base64_encode($request_details['user_id'] . SALT);
                $partner_params['action_button_link'] = base_url('user/autoredirect?p=' . $encrypted_id . '&redirect=' . $ab_link);
                $partner_params['action_button_text'] = 'Pending Requests';
                $email_data_v = partner_email_data($partner_params);
                send_mail($email_data_v);

                $data['status'] = 'succsss';
                $data['msg'] = 'You have rejected the quote!';
                $data['redirect'] = base_url('user/quotes-received');
            } else {
                $data['status'] = 'error';
                $data['msg'] = 'Sorry! Counld not withdraw your request';
            }
            print json_encode($data);
        }
    }

    public function quote_accept_and_book() {
        //Getting all user data from user master list
        $user_data_detail = $this->get_all_user_details_without_login($this->session->userdata('user_email'));
        //check whether stripe token is not empty
        //print_r($this->input->post());die;
        if (!empty($this->input->post('stripeToken'))) {
            //get token, card and user info from the form
            $entity_hashval = $this->input->post('entity_hashval');
            $request_hashval = $this->input->post('request_hashval');
            $token = $this->input->post('stripeToken');
            $event_type = $this->input->post('event_type');
            $event_duration = $this->input->post('event_duration');
            $name = $this->input->post('cc_name');
            $card_num = $this->input->post('cc_number');
            $card_cvc = $this->input->post('cc_cvc');
            $card_exp_month = $this->input->post('cc_month');
            $card_exp_year = $this->input->post('cc_year');
            $card_postalcode = $this->input->post('cc_postalcode');
            $main_total = $this->input->post('main_total');
            $partial_pay = $this->input->post('partial_pay');
            $total_amount = $this->input->post('main_total');
            $paid_amount = $this->input->post('paying_total');
            $paying_total = $due_amount = $this->input->post('due_total');
            $due_date = $this->input->post('due_date');
            //print_r($this->input->post());die;
            //$paying_total = $this->input->post('paying_total');
            $stripe_pay = $due_amount * 100; // Stripe converts amount to cents
            $due_total = $this->input->post('due_total');

            //include Stripe PHP library
            require_once APPPATH . "third_party/stripe/init.php";

            //set api key
            $stripe = array(
                "secret_key" => STRIPE_SECRET,
                "publishable_key" => STRIPE_PUBLIC
            );

            \Stripe\Stripe::setApiKey($stripe['secret_key']);

            //add customer to stripe
            $customer = \Stripe\Customer::create(array(
                        'source' => $token
            ));

            //item information
            $itemName = "Stripe Donation";
            $itemNumber = $request_hashval;
            $itemPrice = $paying_total; // User is paying this amount at the moment
            $currency = "cad";
            $orderID = $request_hashval;

            //charge a credit or a debit card
            $charge = \Stripe\Charge::create(array(
                        'customer' => $customer->id,
                        'amount' => $stripe_pay, // converted in cents
                        'currency' => $currency,
                        'description' => $itemNumber,
                        'metadata' => array(
                            'item_id' => $itemNumber
                        )
            ));

            //retrieve charge details
            $chargeJson = $charge->jsonSerialize();
            //check whether the charge is successful
            if ($chargeJson['amount_refunded'] == 0 && empty($chargeJson['failure_code']) && $chargeJson['paid'] == 1 && $chargeJson['captured'] == 1) {
                //order details
                $amount = $chargeJson['amount'];
                $balance_transaction = $chargeJson['balance_transaction'];
                $currency = $chargeJson['currency'];
                $status = $chargeJson['status'];
                $date = date("Y-m-d H:i:s");

                // Update Customer request Table
                $params['request_hashval'] = $request_hashval;
                $params['part_pay_percent'] = $partial_pay;
                $params['total_paid'] = $paying_total;
                $params['total_due'] = $due_total;
                // Waqas modified code, This code is for modified database table.
                $params['total_amount'] = $total_amount;
                $params['paid_amount'] = $paid_amount;
                $params['due_amount'] = $due_amount;
                $params['due_date'] = $due_date;

                // Waqas code ends here
                $params['modified_by'] = $this->session->userdata('user_id');
                $fname = $this->session->userdata('user_fullname');
                $email = $this->session->userdata('user_email');
                $card_last4 = 'xxxxxxxxxxxx' . $chargeJson['source']['last4'];
                $params['status'] = 'BOOKING_CONFIRM_CUST';
                $req_hashval = $this->Entity_model->update_customer_request($params);
                if ($req_hashval != '') {
                    $payment_id = $this->Entity_model->payments_received($params);
                }
                if ($payment_id != '' && $payment_id > 0) {
                    $request_details = $this->Inquiry_model->get_customer_requests_details($request_hashval);
                    $request_items = $this->Inquiry_model->get_customer_requests_items($request_hashval);

                    /* $params['payment_details']['last4_digit'] = $card_last4;
                      $params['payment_details']['amount_paid'] = $paying_total;
                      $params['payment_details']['amount_due'] = $due_total;
                      $params['payment_details']['paid_on'] = $date; */

                    // Send booking request details to customer
                    $cust_params['email_type'] = 'BOOKING_CONFIRM_CUST';
                    $cust_params['request_details'] = $request_details;
                    $entity_disp = $cust_params['request_details']['entity_disp'];
                    $cust_params['request_items'] = $request_items;
                    $cust_params['top_text'] = 'We’re pleased to let you know ' . $entity_disp . ' has received your payment and confirmed your venue rental request. You can find the booking details below:';
                    $cust_params['action_text'] = 'Thank you for choosing Tenans for your rental. We hope you enjoy your venue and have a wonderful, memorable event!';
                    $ab_link = base_url('user/confirmed-bookings');
                    $encrypted_id = base64_encode($request_details['created_by'] . SALT);
                    $cust_params['action_button_link'] = base_url('user/autoredirect?p=' . $encrypted_id . '&redirect=' . $ab_link);
                    $cust_params['action_button_text'] = 'View my Bookings';
                    $email_data = customer_email_data($cust_params);
                    send_mail($email_data);

                    // Send booking request details to Venue provider
                    $partner_params['email_type'] = 'BOOKING_CONFIRM_CUST_PARTNER';
                    $partner_params['request_details'] = $request_details;
                    $partner_params['request_items'] = $request_items;
                    $partner_params['top_text'] = 'We’re pleased to inform you that you have a new customer! Payment has been collected from the customer and the rental request is confirmed. You can review the booking details below:';
                    $partner_params['action_text'] = 'Congratulations on a new customer, and thank you for using Tenans! You can view all of your bookings by clicking on the button below.';
                    $ab_link = base_url('realnet/realnet-requests/list-confirmed-bookings');
                    $encrypted_id = base64_encode($request_details['user_id'] . SALT);
                    $partner_params['action_button_link'] = base_url('user/autoredirect?p=' . $encrypted_id . '&redirect=' . $ab_link);
                    $partner_params['action_button_text'] = 'View all Bookings';
                    $email_data_v = partner_email_data($partner_params);
                    send_mail($email_data_v);
                }
                //Block Dates table data
                $user_data_param['hashval'] = $entity_hashval;
                $user_data_param['person_name'] = $fname;
                $user_data_param['person_email'] = $email;
                $user_data_param['person_phone'] = $user_data_detail['mobile'];
                $user_data_param['event_start'] = $date;
                $user_data_param['event_duration'] = $event_duration;
                $user_data_param['blocked_reason'] = $params['modified_by'];
                $user_data_param['event_type'] = $event_type;
                $user_data_param['blocked_reason'] = "VenueTop Booking";
                $this->Entity_model->addupdate_block_dates($user_data_param);
                $data['status'] = 'succsss';
                $data['msg'] = 'Thank you! Your payment was successful and your booking is </strong>CONFIRMED</strong>!';
                $data['redirect'] = '';
            } else {
                $data['status'] = 'error';
                $data['msg'] = 'Payment unsuccessful';
                $data['redirect'] = '';
            }
        }
        print json_encode($data);
    }

    public function confirmed_bookings() {
        /* Default User Page */
        if (is_login($this)) {
            $user_array = $this->User_model->get_user_details(array(
                'id' => $this->session->userdata('user_id')
            ));
            /* @var $user_array type */
            if (empty($user_array)) {
                redirect('404');
            }
            $data['heading'] = "Confirmed Bookings";
            $data['confirmed_requests'] = $this->Inquiry_model->get_confirmed_bookings_cust();
            $this->load->view('frontend/user/confirmed_bookings', $data);
        }
    }

    public function booking_details() {
        $request_hash = $this->uri->segment(3);
        /* Default User Page */
        if (is_login($this)) {
            $user_array = $this->User_model->get_user_details(array(
                'id' => $this->session->userdata('user_id')
            ));
            /* @var $user_array type */
            if (empty($user_array)) {
                redirect('404');
            }
            $data['request_details'] = $this->Inquiry_model->get_customer_requests_details($request_hash);
            $entity_hashval = $data['request_details']['entity_hashval'];
            $entity_id = $data['request_details']['entity_id'];
            $data['request_items'] = $this->Inquiry_model->get_customer_requests_items($request_hash);
            $data['entity_details'] = $this->Entity_model->get_entity_details($entity_id);
            $data['entity_policy'] = $this->Entity_model->get_entity_policy($entity_hashval);
            $data['chat_data'] = $this->User_model->my_chat($data['request_details']);
            $data['my_chat_id'] = $this->session->userdata('user_id');
            $data['heading'] = "Confirm Booking";
            $this->load->view('frontend/user/booking_details', $data);
        }
    }

    public function booking_history() {
        /* Default User Page */
        if (is_login($this)) {
            $user_array = $this->User_model->get_user_details(array(
                'id' => $this->session->userdata('user_id')
            ));
            /* @var $user_array type */
            if (empty($user_array)) {
                redirect('404');
            }
            $data['heading'] = "Booking History";
            $data['booking_history'] = $this->Inquiry_model->get_confirmed_bookings_cust_history();
            $this->load->view('frontend/user/booking_history', $data);
        }
    }

    public function history_booking_details() {
        $request_hash = $this->uri->segment(3);
        /* Default User Page */
        if (is_login($this)) {
            $user_array = $this->User_model->get_user_details(array(
                'id' => $this->session->userdata('user_id')
            ));
            /* @var $user_array type */
            if (empty($user_array)) {
                redirect('404');
            }
            $data['request_details'] = $this->Inquiry_model->get_customer_requests_details($request_hash);
            $entity_hashval = $data['request_details']['entity_hashval'];
            $entity_id = $data['request_details']['entity_id'];
            $data['request_items'] = $this->Inquiry_model->get_customer_requests_items($request_hash);
            $data['entity_details'] = $this->Entity_model->get_entity_details($entity_id);
            $data['entity_policy'] = $this->Entity_model->get_entity_policy($entity_hashval);
            $this->load->view('frontend/user/history_booking_details', $data);
        }
    }

    public function accept_terms() {
        // Set Session
        set_user_session_after_login($this, $_POST);
        $this->User_model->update_accept_terms($_POST['id']);
        // First time login Handler
        $data['redirect'] = base_url('frontend/user/profile');
        $this->User_model->update_user_login_time($_POST['id']);
        log_activity(get_session_user_id(), 'USER', 'LOGIN', '', getIP());
        $data['status'] = 'success';
        $data['msg'] = LOGIN_SUCC_MSG;
        print json_encode($data);
    }

    public function add_to_compare() {
        $params['entity_id'] = $this->input->post('entity_id');
        $params['session_id'] = session_id();
        $result = $this->entity_model->add_to_compare($params);
        if ($result['status'] == 1) {
            $data['status'] = 'succsss';
            $data['count'] = $result['count'];
            $data['msg'] = $result['count'] . ' venues added to compare list.';
            $data['redirect'] = '';
        } else {
            $data['status'] = 'error';
            $data['msg'] = 'Sorry! Could not add to compare list';
            $data['redirect'] = '';
        }
        print json_encode($data);
    }

    public function pending_payments_user() {
        $user_id = get_session_user_id();
        $user_type = get_session_user_type();

        if ($user_id == '') {
            logout();
        } else {
            $data['heading'] = "Pending Payment List";
            $data['due_payments'] = $this->User_model->pending_payment_list();
            $this->load->view('frontend/user/history_booking_details', $data);
        }
    }

    public function payments_log_user() {
        $user_id = get_session_user_id();
        $user_type = get_session_user_type();

        if ($user_id == '') {
            logout();
        } else {

            $data['heading'] = "Pending Payment List";
            $data['due_payments'] = $this->User_model->pending_payment_list();
            $this->load->view('frontend/user/history_booking_details', $data);
        }
    }

    public function apply_promo_code() {
        $user_id = get_session_user_id();
        if ($user_id == '') {
            logout();
        } else {
            $promo_code = $this->input->post('promo_code');
            $promo_data = $this->User_model->check_promo_code($promo_code);
            if (isset($promo_data)) {
                echo $promo_data['promo_data']['discount'];
            }
        }
    }

}
