<?php

class Data_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    function login($params = array()) {
        $email = $params['email'];
        $pass = md5($params['pass']);
        $user_login_query = "SELECT id, fname, lname, email, hashval,mobile, user_type, photo, sla, last_login, accept_terms, status "
                . " FROM `user_master` "
                . " WHERE email = ? AND mpwd = ? ";
        $user_login_stmt = $this->db->conn_id->prepare($user_login_query);
        $user_login_stmt->execute(array($email, $pass));
        //echo var_export($user_login_stmt->errorInfo()); exit;
        $user_data = $user_login_stmt->fetch(PDO::FETCH_ASSOC);
        $num_row = $user_login_stmt->rowCount();
        $user_data['num_row'] = $num_row;
        return $user_data;
    }

    function userData($email) {
        $user_data_query = "SELECT * FROM `user_master_list` WHERE email = ?";
        $user_data_stmt = $this->db->conn_id->prepare($user_data_query);
        $user_data_stmt->execute(array($email));
        $user_data = $user_data_stmt->fetch(PDO::FETCH_ASSOC);
        return $user_data;
    }

    function get_index_slide_pictures() {
        $slide_pics = "SELECT * FROM `main_slide` ORDER BY `id` DESC ";
        $slide_pics_stmt = $this->db->conn_id->prepare($slide_pics);
        $slide_pics_stmt->execute();
        $slide_data = $slide_pics_stmt->fetchAll(PDO::FETCH_ASSOC);
        return $slide_data;
    }

    function checkEmail($email) {
        $check_email_query = "SELECT email FROM `user_master` WHERE email = ?";
        $check_email_stmt = $this->db->conn_id->prepare($check_email_query);
        $check_email_stmt->execute(array($email));
        $num_row = $check_email_stmt->rowCount();
        $user_data['num_row'] = $num_row;
        return $user_data;
    }

    function register($params = array()) {
        $fname = $params['fname'];
        $lname = $params['lname'];
        $email = $params['email'];
        $phone = $params['phone'];
        $password = md5($params['password']);
        $user_type = $params['user_type'];
        $user_hash = get_random_md5(time());
        $referredby_id = isset($params['referredby_id']) ? $params['referredby_id'] : '';

        $user_register_query = "INSERT  INTO `user_master` (`fname`, `lname`, `email`, `hashval`, `phone`, `user_type`, mpwd, referredby_id, created_date, `status`) VALUES (?, ?, ?, ?, ?, ?,?, ?, NOW(),'ACTIVE')";
        $user_register_stmt = $this->db->conn_id->prepare($user_register_query);
        $user_register_stmt->execute(array($fname, $lname, $email, $user_hash, $phone, $user_type, $password, $referredby_id));
        //echo var_export($user_register_stmt->errorInfo()); exit;
        $generated_id = $this->db->insert_id();
        return $generated_id;
    }

    function register_partner($params = array()) {

        $first_name = $params['fname'];
        $email = $params['email'];
        $mpwd = $params['mpwd'];
        $user_type = $params['user_type'];
        $referredby_id = $params['referredby_id'];

        $user_register_query = "INSERT  INTO `partner_master` 
                (`fname`, `email`, `mpwd`, `user_type`, referredby_id, created_date, `status`) 
                VALUES (?, ?, ?, ?,  ? , NOW(),'INACTIVE'); ";

        $user_register_stmt = $this->db->conn_id->prepare($user_register_query);
        $user_register_stmt->execute(array($first_name, $email, $mpwd, $user_type,
            $referredby_id));
        // echo $test;
        //echo var_export($user_register_stmt->errorInfo()); exit;
        $generated_id = $this->db->insert_id();
        //$data['generated_id'] = $generated_id;
        return $generated_id;
    }

    function update_user_profile($params) {
        $user_id = $params['user_id'];
        $fname = $params['fname'];
        $lname = $params['lname'];
        $phone = $params['phone'];
        $photo = $params['photo'];
        /* $dob = date("Y-m-d", strtotime($params['dob']));
          $newsletter = isset($params['newsletter'])? $params['newsletter'] : '0';
          if($newsletter == 'on')
          $newsletter = 1; // checked */

        $user_update_query = "UPDATE `user_master` SET `fname` = ?, `lname` = ?, `phone` = ?, `photo` = ? WHERE `id` = ?";
        $user_update_stmt = $this->db->conn_id->prepare($user_update_query);
        $user_update_stmt->execute(array($fname, $lname, $phone, $photo, $user_id));
        $this->session->set_userdata('user_fullname', $fname . ' ' . $lname);
        $data['user_id'] = $user_id;
        return $data;
    }

    function update_user_login_time($user_id) {
        $user_last_login_update_query = "UPDATE user_master SET last_login = NOW() WHERE id = ?";
        $user_last_login_update_stmt = $this->db->conn_id->prepare($user_last_login_update_query);
        $user_last_login_update_stmt->execute(array($user_id));
    }

    function update_accept_terms($user_id) {
        $user_last_login_update_query = "UPDATE user_master SET accept_terms = 1 WHERE id = ?";
        $user_last_login_update_stmt = $this->db->conn_id->prepare($user_last_login_update_query);
        $user_last_login_update_stmt->execute(array($user_id));
    }

    function get_terms($user_type) {
        $user_last_login_update_query = "SELECT terms FROM `user_terms` WHERE user_type = '$user_type'";
        $user_last_login_update_stmt = $this->db->conn_id->prepare($user_last_login_update_query);
        $user_last_login_update_stmt->execute();
        return $user_last_login_update_stmt->fetch(PDO::FETCH_ASSOC);
    }

    function update_user_hashval($hashval, $user_id) {
        $user_hash_update_query = "UPDATE user_master SET hashval = ? WHERE id = ?";
        $user_hash_update_stmt = $this->db->conn_id->prepare($user_hash_update_query);
        $user_hash_update_stmt->execute(array($hashval, $user_id));
    }

    function update_partner_hashval($hashval, $user_id) {
        $user_hash_update_query = "UPDATE partner_master SET hashval = ? WHERE id = ?";
        $user_hash_update_stmt = $this->db->conn_id->prepare($user_hash_update_query);
        $user_hash_update_stmt->execute(array($hashval, $user_id));
    }

    function insert_user_profile_link($linktype, $hash, $user_id) {
        $verification_link_query = "INSERT INTO `user_profile_link` (`user_id`, `link_type`, `link_hash`, `expiry`, `created_date`, `modified_date`) "
                . " VALUES (?, ?, ?,  DATE_ADD(NOW(), INTERVAL 3 DAY), NOW(), NOW());";
        $verification_link_stmt = $this->db->conn_id->prepare($verification_link_query);
        $verification_link_stmt->execute(array($user_id, $linktype, $hash));
        $generated_id = $this->db->insert_id();
        return $generated_id;
    }

    function insert_chat($param) {
        $ref_number = $param['ref_number'];
        $sender_id = $param['sender_id'];
        $reciever_id = $param['receiver_id'];
        $message = $param['chat_message'];
        $date = date("Y-m-d h:i:sa");
        $query = "INSERT INTO `chat` (`ref_number`, `sender_id`, `receiver_id`, `message`, `date`) VALUES (?, ?, ?, ?,?)";
        $query_stmt = $this->db->conn_id->prepare($query);
        $query_stmt->execute(array($ref_number, $sender_id, $reciever_id, $message, $date));
        $generated_id = $this->db->insert_id();
        //Update all messages got read
        $this->read_message($sender_id);
        return $generated_id;
    }

    function read_message($sender_id) {
        $query = "UPDATE `chat` SET `status`='1' WHERE (`receiver_id`=? AND `status`='0')";
        $query_stmt = $this->db->conn_id->prepare($query);
        $query_stmt->execute(array($sender_id));
    }

    function my_chat($param) {
        $ref_number = $param['ref_num'];
        $my_id = $this->session->userdata('user_id');
        $query = "SELECT * FROM chat WHERE ref_number=? AND (sender_id= ? OR receiver_id=?) ORDER BY date ASC";
        $guery_stmt = $this->db->conn_id->prepare($query);
        $guery_stmt->execute(array($ref_number, $my_id, $my_id));
        $result = $guery_stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function insert_partner_profile_link($linktype, $hash, $user_id) {
        /* $update_query = "UPDATE `user_profile_link` SET STATUS = 'INACTIVE', "
          . " reset_ip = ? WHERE "
          . " link_type = ? AND user_id = ? AND STATUS = 'ACTIVE'";
          $update_stmt = $this->db->conn_id->prepare($update_query);
          $update_stmt->execute(array($this->input->ip_address(), $linktype, $user_id)); */

        $verification_link_query = "INSERT INTO `partner_profile_link` (`user_id`, `link_type`, `link_hash`, `expiry`, `created_date`, `modified_date`) "
                . " VALUES (?, ?, ?,  DATE_ADD(NOW(), INTERVAL 1 DAY), NOW(), ?);";
        $verification_link_stmt = $this->db->conn_id->prepare($verification_link_query);
        $verification_link_stmt->execute(array($user_id, $linktype, $hash, $user_id));
        $generated_id = $this->db->insert_id();
        return $generated_id;
    }

    function update_user_profile_link($status, $hash, $user_id) {
        $update_link_query = "UPDATE `user_profile_link` SET STATUS = ?  WHERE link_hash = ? AND user_id = ?";
        $update_link_stmt = $this->db->conn_id->prepare($update_link_query);
        $update_link_stmt->execute(array($status, $hash, $user_id));
        $row_count = $update_link_stmt->rowCount();
        //echo var_export($stmt->errorInfo());die;
        return $row_count;
    }

    function get_profile_link($link_type, $link_hash) {
        //'ACTIVATION'
        $get_link_query = "SELECT id, user_id FROM `user_profile_link` "
                . " WHERE link_hash = ? AND link_type = ?";
        $get_link_stmt = $this->db->conn_id->prepare($get_link_query);
        $get_link_stmt->execute(array($link_hash, $link_type));
        $result = $get_link_stmt->fetchAll(PDO::FETCH_ASSOC);

        return $result;
    }

    function new_password($params = array()) {
        $reset_email = $params['reset_email'];
        $reset_pass = md5($params['reset_pass']);
        if ($reset_email != '') {
            $stmt = $this->db->conn_id->prepare("UPDATE user SET mpwd = ? WHERE email = ? AND status != 'Active'");
            $stmt->execute(array($reset_pass, $reset_email));
            $data['status'] = 'success';
            //echo var_export($stmt->errorInfo());
        }
        return $data;
    }

    function get_user_details($params = array()) {
        $id = isset($params['id']) ? $params['id'] : '';
        $email = isset($params['email']) ? $params['email'] : '';
        // $stmt = $this->db->conn_id->prepare("SELECT * FROM user_master WHERE (id = ? OR email = ?)");
        $user_query = "SELECT * FROM user_master WHERE  (id = ? OR email = ?) ";
        $stmt = $this->db->conn_id->prepare($user_query);
        $stmt->execute(array($id, $email));
        //echo var_export($stmt->errorInfo());die;
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if (!empty($data)) {
            return $data[0];
        } else {
            return $data;
        }
    }

    function user_session($params = array()) {
        $user_id = $this->clean($params['id']);
        $ip_addr = $_SERVER['SERVER_ADDR'];
        $stmt = $this->db->conn_id->prepare("INSERT INTO user_session (user_id, ip_addr) VALUES (?, ?)");
        $stmt->execute(array($user_id, $ip_addr));
        $last_id = $this->db->insert_id();
        $data['last_id'] = $last_id;
        return $data;
    }

    function user_status_change($new_status, $user_id) {
        $user_status_query = "UPDATE `user_master` SET STATUS = ? WHERE id = ? AND STATUS != 'DELETED'";
        $user_status_stmt = $this->db->conn_id->prepare($user_status_query);
        $user_status_stmt->execute(array($new_status, $user_id));
        $num_row = $user_status_stmt->rowCount();
        //echo var_export($user_status_stmt->errorInfo());        
        return $num_row;
    }

    function user_activation($params = array()) {
        $email = $params['email'];
        $pass = md5($params['pass']);
        if ($email != '' && $pass != '') {
            $sql = "UPDATE user SET password = '" . $pass . "', status = 'ACTIVE' WHERE email = '" . $email . "'";
            $stmt = $this->db->conn_id->prepare($sql);
            $stmt->execute(array());
            //echo var_export($stmt->errorInfo());
            $num_row = $stmt->rowCount();
            $data['num_row'] = $num_row;
        } else {
            $data['num_row'] = '';
        }
        return $data;
    }

    function change_reset_password($params = array()) {
        $user_id = $params['user_id'];
        $is_reset = isset($params['is_reset']) ? $params['is_reset'] : '';
        $old_pass = md5($params['old_pass']);
        $new_pass = md5($params['new_pass']);
        if ($is_reset == '') {
            $stmt = $this->db->conn_id->prepare("SELECT email FROM user_master WHERE id=? AND mpwd=?");
            $stmt->execute(array($user_id, $old_pass));
            //echo var_export($stmt->errorInfo());
            $num_row = $stmt->rowCount();
            if ($num_row > 0) {
                $stmt = $this->db->conn_id->prepare("UPDATE user_master SET mpwd = ? WHERE id = ?");
                $stmt->execute(array($new_pass, $user_id));
                $data['status'] = 'success';
                $data['msg'] = 'Your password changed successfully';
            } else {
                $data['status'] = 'error';
                $data['msg'] = 'Wrong old password';
            }
        }
        return $data;
    }

    function user_change_photo($params = array()) {
        $user_id = isset($params['user_id']) ? $this->clean($params['user_id']) : '';
        $photo = isset($params['photo']) ? $params['photo'] : '';
        if ($user_id != '') {
            $stmt = $this->db->conn_id->prepare("UPDATE user SET photo = ? WHERE id = ?");
            $stmt->execute(array($photo, $user_id));
            //echo var_export($stmt->errorInfo());
            $num_row = $stmt->rowCount();
            $data['num_row'] = $num_row;
        } else {
            $data['num_row'] = '';
        }
        return $data;
    }

    function check_email($params = array()) {
        $email = $params['email'];
        $stmt = $this->db->conn_id->prepare("SELECT COUNT(*) AS email_count FROM user WHERE email=?");
        $stmt->execute(array($email));
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        //echo var_export($stmt->errorInfo());
        return $data[0];
    }

    function get_email_template($params = array()) {
        $email_type = $params['email_type'];
        $stmt = $this->db->conn_id->prepare("SELECT id, created_date, email_type, email_from, email_cc, email_subject, email_body
			FROM email_template WHERE email_type = ? AND status = 'Active'");
        $stmt->execute(array($email_type));
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        //echo var_export($stmt->errorInfo());
        return $data[0];
    }

    function email_log($params = array()) {
        $email_type = $this->clean($params['email_type']);
        $email_to = $params['email_to'];
        $email_cc = $params['email_cc'];
        $email_subject = $this->clean($params['email_subject']);
        $stmt = $this->db->conn_id->prepare("INSERT INTO email_log (email_type, email_to, email_cc, email_subject) VALUES (?, ?, ?, ?)");
        $stmt->execute(array($email_type, $email_to, $email_cc, $email_subject));
        //echo var_export($stmt->errorInfo());die;
        $last_id = $this->db->insert_id();
        $data['last_id'] = $last_id;
        return $data;
    }

    function check_promo_code($promo_code) {
        $today_date = date('Y-m-d');
        $stmt = $this->db->conn_id->prepare("SELECT * FROM promo WHERE promo_code = ?");
        $stmt->execute(array($promo_code));
        $data['promo_data'] = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($data && $data['promo_data']['end_date'] > $today_date) {
            $data['promo_msg'] = 'Promo code is valid';
            return $data;
        } else {
            $data['promo_msg'] = 'Promo Code is invalid';
            return $data;
        }
        //echo var_export($stmt->errorInfo());die;
    }

}
