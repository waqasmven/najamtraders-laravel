<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Generic_Model
 *
 * @author Sunil Khandade
 */
class Entity_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    function get_partner_entity_after_login($partner_id) {
        $sql = "SELECT * from `entity_master` WHERE user_id = :user_id AND status = 'ACTIVE'";
        $stmt = $this->db->conn_id->prepare($sql);
        $stmt->execute(array(':user_id' => $partner_id));
        return $stmt->fetchALL(PDO::FETCH_ASSOC);
    }

    function get_entity_image_thumb_path($id) {
        $sql = "SELECT * from `entity_attachments_list` WHERE entity_id = " . $id;
        $stmt = $this->db->conn_id->prepare($sql);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    function get_partner_entity_sla() {

        $sql = "SELECT es.entity_id, em.entity_disp, es.sla_id,es.status AS entity_sla_status,
                    es.id AS entity_sla_id, sm.modified_date, sm.status AS sla_status, em.sla_check
                    FROM entity_sla es
                    JOIN sla_master sm ON es.sla_id = sm.id
                    JOIN entity_master em ON es.entity_id = em.id AND em.user_id = '" . get_session_user_id() . "'
                    ORDER BY es.created_date DESC";
        $stmt = $this->db->conn_id->prepare($sql);
        $stmt->execute();
        return $stmt->fetchALL(PDO::FETCH_ASSOC);
    }

    public function get_all_entities_count($params = array()) {
        $stmt = $this->db->conn_id->prepare("SELECT * from `entity_master_list` WHERE status = 'ACTIVE'");
        $stmt->execute();
        return $stmt->rowCount();
    }

    function get_all_entities($params = array()) {
        $sql = "SELECT * FROM `unit_master_list` WHERE STATUS = 1 ORDER BY user_id DESC";
        $stmt = $this->db->conn_id->prepare($sql);
        $stmt->execute();
        return $stmt->fetchALL(PDO::FETCH_ASSOC);
    }

    function get_all_trades_types() {
        $sql = "SELECT * from `trade_types` WHERE status = 1 ORDER BY id";
        $stmt = $this->db->conn_id->prepare($sql);
        $stmt->execute();
        return $stmt->fetchALL(PDO::FETCH_ASSOC);
    }

    function get_all_guest_capacity() {
        $sql = "SELECT * from `guest_capacity`";
        $stmt = $this->db->conn_id->prepare($sql);
        $stmt->execute();
        return $stmt->fetchALL(PDO::FETCH_ASSOC);
    }

    function get_all_email_templates() {
        $sql = "SELECT * from `email_template` WHERE status = 'ACTIVE'";
        $stmt = $this->db->conn_id->prepare($sql);
        $stmt->execute();
        return $stmt->fetchALL(PDO::FETCH_ASSOC);
    }

    function get_partner_entities($user_id) {
        $sql = "SELECT * from `unit_master_list` WHERE user_id = ? AND status = '1'";
        $stmt = $this->db->conn_id->prepare($sql);
        $stmt->execute(array($user_id));
        return $stmt->fetchALL(PDO::FETCH_ASSOC);
    }

    function get_block_hall_list($entity_hashval) {
        $sql = "SELECT * from `entity_master_list` WHERE $entity_hashval = ? ";
        $stmt = $this->db->conn_id->prepare($sql);
        $stmt->execute(array($entity_hashval));
        return $stmt->fetchALL(PDO::FETCH_ASSOC);
    }

    function customer_requests($user_id) {
        $sql = "SELECT * from `customer_request_list` WHERE user_id = ?";
        $stmt = $this->db->conn_id->prepare($sql);
        $stmt->execute(array($user_id));
        return $stmt->fetchALL(PDO::FETCH_ASSOC);
    }

    function get_my_service_requests($user_id) {
        $sql = "SELECT * from `service_request_list` WHERE customer_id = ?";
        $stmt = $this->db->conn_id->prepare($sql);
        $stmt->execute(array($user_id));
        return $stmt->fetchALL(PDO::FETCH_ASSOC);
    }

    function get_featured_entity($city_name = null) {
        $featured_entity = "SELECT DISTINCT unit_id FROM `unit_master_list` WHERE city_name = ? AND place_type = 1 ORDER BY RAND() LIMIT 3"; // Pull only residential properties
        $stmt = $this->db->conn_id->prepare($featured_entity);
        $stmt->execute(array($city_name));
        return $stmt->fetchALL(PDO::FETCH_ASSOC);
    }

    function get_hot_entity() {
        $hot_entity_query = "SELECT DISTINCT entity_id AS id,  total_views FROM `entity_master_list` order by total_views desc limit 1";
        $stmt = $this->db->conn_id->prepare($hot_entity_query);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    function get_place_type() {
        $query = "SELECT * FROM `property_type_master` order by venue_type";
        $stmt = $this->db->conn_id->prepare($query);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    function get_my_block_dates($user_id) {
        $my_property_query = "SELECT em.hashval, em.entity_name, eod.id, eod.* FROM entity_master AS em,	entity_occupied_dates AS eod WHERE em.user_id = ?  AND em.hashval = eod.entity_hashval";
        $stmt = $this->db->conn_id->prepare($my_property_query);
        $stmt->execute(array($user_id));
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $data;
    }

    function get_entity_property_type($entity_hashval = null) {
        $entity_id = $this->get_entity_id_by_hashval($entity_hashval);
        $select = "SELECT * FROM `entity_property_type` WHERE entity_id = :entity_id";
        $stmt = $this->db->conn_id->prepare($select);
        $stmt->execute(array(
            ':entity_id' => $entity_id
        ));
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    function get_entity_service($entity_hashval = null) {
        $entity_id = $this->get_entity_id_by_hashval($entity_hashval);
        $select = "SELECT * FROM `entity_service_list` WHERE entity_id = :entity_id";
        $stmt = $this->db->conn_id->prepare($select);
        $stmt->execute(array(
            ':entity_id' => $entity_id
        ));
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    function get_entity_amenity($entity_hashval = null) {
        $unit_id = $this->get_entity_id_by_hashval($entity_hashval);
        $select = "SELECT * FROM `entity_amenity_list` WHERE unit_id = ?";
        $stmt = $this->db->conn_id->prepare($select);
        $stmt->execute(array($unit_id));
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    function get_entity_tax($entity_hashval = null) {
        $entity_id = $this->get_entity_id_by_hashval($entity_hashval);
        $select = "SELECT * FROM `entity_tax_list` WHERE entity_id = :entity_id and status = 'ACTIVE'";
        $stmt = $this->db->conn_id->prepare($select);
        $stmt->execute(array(
            ':entity_id' => $entity_id
        ));
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    function get_entity_thumb($unit_id = null) {
        $select = "SELECT * FROM `entity_attatchments` WHERE unit_id = ? AND type = 0 AND status = 1 LIMIT 1";
        $stmt = $this->db->conn_id->prepare($select);
        $stmt->execute(array($unit_id));
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    function get_sask_cities($params) {
        $keyword = $params['keyword'];
        $select = "SELECT * FROM `city_master` WHERE `state_id` = 674 AND city_name LIKE '%" . $keyword . "%' ORDER BY city_name LIMIT 5";
        $stmt = $this->db->conn_id->prepare($select);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    function get_neighbourhoods($params) {
        $city_id = $params['city_id'];
        $keyword = $params['keyword'];
        $select = "SELECT * FROM `neighbourhoods` WHERE city_id = ? AND neighbourhood LIKE '%" . $keyword . "%' LIMIT 5";
        $stmt = $this->db->conn_id->prepare($select);
        $stmt->execute(array($city_id));
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    function get_entity_services_for_quote($entity_hashval, $not_service_id_array = array()) {
        $entity_id = $this->get_entity_id_by_hashval($entity_hashval);
        $not_in = '';
        if (count($not_service_id_array) > 0) {
            $not_in = "   AND service_id NOT IN  ('" . implode(',', $not_service_id_array) . "')";
        }
        $entity_id = $this->get_entity_id_by_hashval($entity_hashval);
        $select = "SELECT * FROM entity_service_list WHERE entity_id = $entity_id " . $not_in;
        $stmt = $this->db->conn_id->prepare($select);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    //To Add new amenity in amenity list
    function insert_amenity($params = array()) {
        $user_id = get_session_user_id();
        // print_r($params);die;
        $select = "INSERT INTO `amenity_master` (amenity,symbol,filter_status,status,created_by,created_date) values('" . $params . "','" . $params . "','" . $params . "','" . $params . "','" . $params . "')";
        $stmt = $this->db->conn_id->prepare($select);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    function get_entity_features($params) {
        $unit_hashval = $params['unit_hashval'];
        $unit_data = $this->get_entity_by_hash($unit_hashval);
        $unit_id = $unit_data['unit_id'];

        $select = "SELECT * FROM `entity_amenity_list` WHERE unit_id=? AND type = 'E' AND included=0";
        $stmt = $this->db->conn_id->prepare($select);
        $stmt->execute(array($unit_id));
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    
    function get_common_area_features($params) {
        $unit_hashval = $params['unit_hashval'];
        $unit_data = $this->get_entity_by_hash($unit_hashval);
        $unit_id = $unit_data['unit_id'];

        $select = "SELECT * FROM `entity_amenity_list` WHERE unit_id=? AND house_type = 'C' AND included =0";
        $stmt = $this->db->conn_id->prepare($select);
        $stmt->execute(array($unit_id));
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    

    function get_entity_appliances($params) {
        $unit_hashval = $params['unit_hashval'];
        $unit_data = $this->get_entity_by_hash($unit_hashval);
        $unit_id = $unit_data['unit_id'];

        $select = "SELECT * FROM `entity_amenity_list` WHERE unit_id=? AND type = 'A' AND included=0";
        $stmt = $this->db->conn_id->prepare($select);
        $stmt->execute(array($unit_id));
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    function get_utilities_included($params) {
        $unit_hashval = $params['unit_hashval'];
        $unit_data = $this->get_entity_by_hash($unit_hashval);
        $unit_id = $unit_data['unit_id'];

        $select = "SELECT * FROM `entity_amenity_list` WHERE unit_id=? AND type = 'U' AND included=0";
        $stmt = $this->db->conn_id->prepare($select);
        $stmt->execute(array($unit_id));
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    function get_utilities_notincluded($params) {
        $unit_hashval = $params['unit_hashval'];
        $unit_data = $this->get_entity_by_hash($unit_hashval);
        $unit_id = $unit_data['unit_id'];

        $select = "SELECT * FROM `entity_amenity_list` WHERE unit_id=? AND type = 'U' AND included=1";
        $stmt = $this->db->conn_id->prepare($select);
        $stmt->execute(array($unit_id));
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    function get_all_amenities() {
        $select = "SELECT * FROM amenity_master";
        $stmt = $this->db->conn_id->prepare($select);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    function get_all_features() {
        $select = "SELECT * FROM amenity_master WHERE type = 'E' ORDER BY amenity";
        $stmt = $this->db->conn_id->prepare($select);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    function get_all_appliances() {
        $select = "SELECT * FROM amenity_master WHERE type = 'A' ORDER BY amenity";
        $stmt = $this->db->conn_id->prepare($select);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    function get_all_uitilities() {
        $select = "SELECT * FROM amenity_master WHERE type = 'U' ORDER BY amenity";
        $stmt = $this->db->conn_id->prepare($select);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    
    function get_all_common_area() {
        $select = "SELECT * FROM amenity_master WHERE house_type = 'C' ORDER BY amenity";
        $stmt = $this->db->conn_id->prepare($select);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    
    function get_amenities_by_id($id) {
        $select = "SELECT * FROM amenity_master where id=" . $id;
        $stmt = $this->db->conn_id->prepare($select);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    function get_all_trade_types() {
        $select = "SELECT * FROM trade_types";
        $stmt = $this->db->conn_id->prepare($select);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    function get_trade_detail_by_id($id) {
        $select = "SELECT * FROM trades where id=" . $id;
        $stmt = $this->db->conn_id->prepare($select);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    function update_amentiy_info($id, $name, $symbol, $user_id) {
        $select = "UPDATE amenity_master SET amenity = '" . $name . "', symbol = '" . $symbol . "', modified_by=" . $user_id . " WHERE id=" . $id . "; ";
        $stmt = $this->db->conn_id->prepare($select);
        $stmt->execute();
    }

    function get_event_type_by_id($id) {
        $select = "SELECT * FROM event_type where id=" . $id;
        $stmt = $this->db->conn_id->prepare($select);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    function update_event_type_info($id, $name, $order) {
        $select = "UPDATE event_type SET type = '" . $name . "', display_order = '" . $order . "' WHERE id=" . $id;
        $stmt = $this->db->conn_id->prepare($select);
        $stmt->execute();
    }

    function get_service_by_id($id) {
        $select = "SELECT * FROM service_master where id=" . $id;
        $stmt = $this->db->conn_id->prepare($select);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    function update_service_data($id, $name, $symbol, $user_id) {
        $select = "UPDATE service_master SET service_title = '" . $name . "', service_description = '" . $symbol . "', modified_by=" . $user_id . " WHERE id=" . $id;
        $stmt = $this->db->conn_id->prepare($select);
        $stmt->execute();
    }

    function get_email_template_by_id($id) {
        $select = "SELECT * FROM email_template where id=" . $id;
        $stmt = $this->db->conn_id->prepare($select);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    function update_email_template($email_id, $email_type, $email_subject, $email_header, $email_footer) {
        $select = "UPDATE email_template SET email_type = '" . $email_type . "', email_subject = '" . $email_subject . "', email_header='" . $email_header . "', email_footer='" . $email_footer . "' WHERE id=" . $email_id;
        $stmt = $this->db->conn_id->prepare($select);
        $stmt->execute();
    }

    function get_tax_by_id($id) {
        $select = "SELECT * FROM tax_master where id=" . $id;
        $stmt = $this->db->conn_id->prepare($select);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    function update_tax_data($tax_id, $tax_name, $tax_description, $tax_rate, $tax_order, $user_id) {
        $select = "UPDATE tax_master SET tax_label = '" . $tax_name . "', tax_desc = '" . $tax_description . "',tax_rate='" . $tax_rate . "', display_order='" . $tax_order . "', modified_by=" . $user_id . " WHERE id=" . $tax_id;
        $stmt = $this->db->conn_id->prepare($select);
        $stmt->execute();
    }

    function get_guest_capacity_by_id($id) {
        $select = "SELECT * FROM guest_capacity where id=" . $id;
        $stmt = $this->db->conn_id->prepare($select);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    function update_guest_capacity($least_capacity, $high_capacity, $id) {
        $select = "UPDATE guest_capacity SET capacity = '" . $least_capacity . " - " . $high_capacity . "', least_capacity = '" . $least_capacity . "',high_capacity='" . $high_capacity . "' WHERE id=" . $id;
        $stmt = $this->db->conn_id->prepare($select);
        $stmt->execute();
    }

    function get_all_property_types() {
        $select = "SELECT * FROM property_type_master WHERE status = 'ACTIVE'";
        $stmt = $this->db->conn_id->prepare($select);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    function get_all_services() {
        $select = "SELECT * FROM service_master WHERE status = 'ACTIVE'";
        $stmt = $this->db->conn_id->prepare($select);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    function get_all_taxes($city_id) {
        if ($city_id != '') {
            $state = $this->get_state_from_city_id($city_id);
            $state_id = $state['state_id'];
            $select = "SELECT * FROM tax_master_list WHERE state_id = '" . $state_id . "' AND status = 'ACTIVE'";
        } else {
            $select = "SELECT * FROM tax_master_list WHERE status = 'ACTIVE'";
        }
        $stmt = $this->db->conn_id->prepare($select);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    function get_policy_categories() {
        $select = "SELECT * FROM policy_category_master WHERE status = 'ACTIVE'";
        $stmt = $this->db->conn_id->prepare($select);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    function get_entity_booking_notes($entity_hashval) {
        $entity_id = $this->get_entity_id_by_hashval($entity_hashval);
        $select = "SELECT * FROM entity_booking_notes WHERE entity_id = ? AND status = 'ACTIVE'";
        $stmt = $this->db->conn_id->prepare($select);
        $stmt->execute(array($entity_id));
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    function get_entity_policy($entity_hashval) {
        $entity_id = $this->get_entity_id_by_hashval($entity_hashval);
        $select = "SELECT * FROM entity_policy_list WHERE entity_id = ? AND status = 'ACTIVE'";
        $stmt = $this->db->conn_id->prepare($select);
        $stmt->execute(array($entity_id));
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    function get_entity_reviews_er($entity_id, $review_page = 1) {
        $select = "SELECT * FROM `entity_review_rating_list` WHERE  id = :entity_id  LIMIT " . REVIEW_PER_PAGE * ($review_page - 1) . "," . REVIEW_PER_PAGE;
        $stmt = $this->db->conn_id->prepare($select);
        $stmt->execute(array(
            ':entity_id' => $entity_id
        ));
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    function get_entity_reviews_social($entity_id, $review_page = 1) {
        $select = "SELECT * FROM `entity_review_rating_list` WHERE review_type != 'ER' AND id = :entity_id  LIMIT " . REVIEW_PER_PAGE * ($review_page - 1) . "," . REVIEW_PER_PAGE;
        $stmt = $this->db->conn_id->prepare($select);
        $stmt->execute(array(
            ':entity_id' => $entity_id
        ));

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    // Pull bydefault images only
    function get_entity_images_remove_later($entity_id_or_hash_list, $image_type = 'THUMB') {

        switch ($image_type) {
            case 'THUMB':
                $image_dir = THUMB_DIR;
                break;
            case 'MEDIUM':
                $image_dir = MEDIUM_DIR;
                break;
            default:
                $image_dir = LARGE_DIR;
        }

        $select_images = "SELECT entity_id, hashval, `type`, CONCAT( '" . base_url() . UPLOAD_DIR . ENTITY_IMAGE_DIR . $image_dir . "', entity_id, '/"
                . "' ,path) AS image FROM (SELECT ea.*,eml.hashval FROM `entity_master_list` eml
                                JOIN `entity_attachments_list` ea ON eml.entity_id = ea.entity_id) e_mas_att
                                WHERE (entity_id IN ($entity_id_or_hash_list) OR hashval IN ($entity_id_or_hash_list)  ) AND `type` = 'THUMB'
                                  UNION
                                  SELECT eml.entity_id, hashval, 'DEFAULT' as type,
                                  '" . base_url() . UPLOAD_DIR . DEFAULT_DIR . $image_dir . ENTITY_IMAGE_DIR . DEFAULT_ENTITY_IMAGE . "'
                                   AS image FROM  `entity_master_list` eml WHERE (eml.entity_id IN ($entity_id_or_hash_list)
                                   OR hashval IN ($entity_id_or_hash_list)) ";
        //echo $select_images;die;
        $select_stmt = $this->db->conn_id->prepare($select_images);
        $select_stmt->execute();
        return $select_stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    // Pull bydefault images only
    function get_entity_images($entity_id_or_hash_list) {
        $select_images = "SELECT * FROM `entity_attatchments` WHERE unit_id IN ($entity_id_or_hash_list)";
        $select_stmt = $this->db->conn_id->prepare($select_images);
        $select_stmt->execute();
        return $select_stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    function get_entity_details_multiple($comma_separated_list) {
        $featured_entity = "SELECT * FROM unit_master_list
                    WHERE unit_id " . build_sql_in_clause_from_csv($comma_separated_list) . " OR unit_hashval " . build_sql_in_clause_from_csv($comma_separated_list) . " OR permalink " . build_sql_in_clause_from_csv($comma_separated_list);
        $stmt = $this->db->conn_id->prepare($featured_entity);
        $stmt->execute();
        $hot_data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $hot_data;
    }

    function get_entity_details($params) {
        $unit_id = isset($params['unit_id']) ? $params['unit_id'] : '';
        $unit_hashval = isset($params['unit_hashval']) ? $params['unit_hashval'] : '';
        $permalink = isset($params['permalink']) ? $params['permalink'] : '';
        $featured_entity = "SELECT * FROM unit_master_list WHERE unit_id = ? OR unit_hashval = ? OR permalink = ?";
        $stmt = $this->db->conn_id->prepare($featured_entity);
        $stmt->execute(array($unit_id, $unit_hashval, $permalink));
        $hot_data = $stmt->fetch(PDO::FETCH_ASSOC);
        return $hot_data;
    }

    function get_entity_id_by_hashval($hashval) {
        $select = "SELECT * FROM unit_master_list WHERE unit_hashval = ?";
        $stmt = $this->db->conn_id->prepare($select);
        $stmt->execute(array($hashval));
        $data = $stmt->fetch(PDO::FETCH_ASSOC);
        return $data['unit_id'];
    }

    function set_entity_cart($entity_id) {
        $featured_entity = "INSERT INTO  `user_session_cart` (`user_id`, `entity_id`, `status`) VALUES (:user_id, :entity_id, 'IN_CART')";
        $stmt = $this->db->conn_id->prepare($featured_entity);
        $stmt->execute(array(
            ':user_id' => get_session_user_id(),
            ':entity_id' => $entity_id
        ));
    }

    function payments_received($params) {
        $request_hashval = $params['request_hashval'];
        $modified_by = $params['modified_by'];
        $total_amount = $params['total_amount'];
        $paid_amount = $params['paid_amount'];
        $due_amount = $params['due_amount'];
        $due_date = date('Y-m-d h:i:s', strtotime($params['due_date']));
        $pay_method = 'CARD';
        $query = "INSERT INTO `payments` (`request_hashval`, `total_amount`,`amount_paid`,`due_amount`,`due_date`, `pay_method`, `paid_by`) "
                . "VALUES (?, ?, ?, ?, ?, ?, ?)";
        $stmt = $this->db->conn_id->prepare($query);
        $stmt->execute(array($request_hashval, $total_amount, $paid_amount, $due_amount, $due_date, $pay_method, $modified_by));
        $payment_id = $this->db->insert_id();
        return $payment_id;
    }

    function pending_payment_list() {
        $sql = "select * from payment_list where due_amount>0 order by due_date";
        $stmt = $this->db->conn_id->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    function get_entity_cart($user_id) {
        $featured_entity = "SELECT * FROM `user_session_cart` WHERE user_id = :user_id AND status = 'IN_CART'";
        $stmt = $this->db->conn_id->prepare($featured_entity);
        $stmt->execute(array(
            ':user_id' => $user_id
        ));
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    function get_entity_services($entity_ids) {
        $featured_entity = "SELECT DISTINCT * FROM entity_service_list WHERE entity_id IN ($entity_ids)";
        $stmt = $this->db->conn_id->prepare($featured_entity);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    function get_entity_service_details($service_id) {
        $featured_entity = "SELECT * FROM entity_service_list WHERE service_id IN ($service_id)";
        $stmt = $this->db->conn_id->prepare($featured_entity);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    function remove_entity_cart($entity_id) {
        $featured_entity = "UPDATE `user_session_cart` SET
                              `status` = 'REMOVED',
                               `modified_date` = CURRENT_TIMESTAMP  WHERE user_id = :user_id AND entity_id = :entity_id";
        $stmt = $this->db->conn_id->prepare($featured_entity);
        $stmt->execute(array(
            ':user_id' => get_session_user_id(),
            ':entity_id' => $entity_id
        ));
    }

    function update_customer_request($params) {
        $request_hashval = $params['request_hashval'];
        $part_pay_percent = $params['part_pay_percent'];
        $total_paid = $params['total_paid'];
        $total_due = $params['total_due'];
        $modified_by = $params['modified_by'];
        $status = $params['status'];
        $featured_entity = "UPDATE customer_requests SET request_process = ?, part_pay_percent = ?, total_paid = ?, total_due = ?, modified_by = ?, modified_date = NOW() "
                . "WHERE request_hashval = ?";
        $stmt = $this->db->conn_id->prepare($featured_entity);
        $stmt->execute(array($status, $part_pay_percent, $total_paid, $total_due, $modified_by, $request_hashval));
        //echo var_export($stmt->errorInfo()); exit;
        return $request_hashval;
    }

    function update_entity_cart($new_status, $entity_id, $conn) {
        if ($conn === null) {
            $conn = $this->db->conn_id;
        }
        $featured_entity = "UPDATE `user_session_cart` SET
                              `status` = :new_status,
                               `modified_date` = CURRENT_TIMESTAMP
                              WHERE user_id = :user_id AND entity_id = :entity_id ";
        $stmt = $conn->prepare($featured_entity);
        $stmt->execute(array(
            ':user_id' => get_session_user_id(),
            ':entity_id' => $entity_id,
            ':new_status' => $new_status
        ));
    }

    function entity_details($entity_hashval) {
        $entity_details = "SELECT * FROM unit_master_list WHERE unit_hashval = ?";
        $stmt = $this->db->conn_id->prepare($entity_details);
        $stmt->execute(array($entity_hashval));
        //echo var_export($stmt->errorInfo()); exit;
        $entity_data = $stmt->fetch(PDO::FETCH_ASSOC);
        return $entity_data;
    }

    function building_details($building_id) {
        $entity_details = "SELECT * FROM building_master WHERE id = ?";
        $stmt = $this->db->conn_id->prepare($entity_details);
        $stmt->execute(array($building_id));
        //echo var_export($stmt->errorInfo()); exit;
        $entity_data = $stmt->fetch(PDO::FETCH_ASSOC);
        return $entity_data;
    }

    function get_unit_detail_by_hashval($unit_hashval, $building_id) {
        $unit_detail_query = "SELECT * FROM unit_master_list WHERE unit_hashval = ? AND building_id = ?";
        $stmt = $this->db->conn_id->prepare($unit_detail_query);
        $stmt->execute(array($unit_hashval, $building_id));
        //echo var_export($stmt->errorInfo()); exit;
        $entity_data = $stmt->fetch(PDO::FETCH_ASSOC);
        return $entity_data;
    }

    function get_building_units_detail_by_building_id($building_id) {
        $unit_detail_query = "SELECT * FROM unit_master_list WHERE building_id = ?";
        $stmt = $this->db->conn_id->prepare($unit_detail_query);
        $stmt->execute(array($building_id));
        //echo var_export($stmt->errorInfo()); exit;
        $entity_data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $entity_data;
    }

    function block_date_by_hashval($block_id) {
        $entity_details = "SELECT * FROM entity_occupied_dates WHERE id = ?";
        $stmt = $this->db->conn_id->prepare($entity_details);
        $stmt->execute(array($block_id));
        //echo var_export($stmt->errorInfo()); exit;
        $entity_data = $stmt->fetch(PDO::FETCH_ASSOC);
        return $entity_data;
    }

    function entity_search($params) {
        //print_r($params);die;
        $lat = $params['lat'];
        $long = $params['long'];
        $type = isset($params['type']) ? $params['type'] : '1';
        $bedrooms = $params['bedrooms'];
        $washrooms = $params['washrooms'];
        $place_sub_type = $params['place_sub_type'];
        $pets = $params['pets'];
        $price = $params['price'];

        $mapradiuscondi = '';
        $mapshavingcondi = '';
        $othersearch = '';
        if ($type != '') {
            $othersearch .= ' AND uml.place_type >= ' . $type;
        }
        if ($bedrooms != '') {
            $othersearch .= ' AND uml.bedrooms >= ' . $bedrooms;
        }
        if ($washrooms != '') {
            $othersearch .= ' AND uml.washrooms >= ' . $washrooms;
        }
        if ($place_sub_type != '') {
            $othersearch .= ' AND uml.place_sub_type = ' . $place_sub_type;
        }
        if ($pets != '') {
            $othersearch .= ' AND uml.pets = ' . $pets;
        }
        if ($price != '') {
            $othersearch .= ' AND uml.price <= ' . $price;
        }

        if ($lat != '' && $long != '') {
            $mapradiuscondi = ", (
            6371 * ACOS (
            COS ( RADIANS('" . $lat . "') )
            * COS( RADIANS( lati ) )
            * COS( RADIANS( longi ) - RADIANS('" . $long . "') )
            + SIN ( RADIANS('" . $lat . "') )
            * SIN( RADIANS( lati ) )
          ) ) AS distance ";
            $mapshavingcondi = " HAVING distance < '" . RADIUS . "' ORDER BY SUBSTRING(distance FROM 1 FOR 1) ASC";
        }

        $search_entity = "SELECT *" . $mapradiuscondi . " FROM `unit_master_list` uml WHERE 1 = 1 "
                . $othersearch . $mapshavingcondi;
        //echo $search_entity;die;
        $stmt = $this->db->conn_id->prepare($search_entity);
        $stmt->execute();
        $search_data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $search_data;
    }

    function entity_by_lat_long_search($params) {

        $placetype = 'lat_lng';
        $lat = $params['lat'];
        $long = $params['lng'];

        $mapradiuscondi = '';
        $mapshavingcondi = '';
        $othersearch = '';
        // if place type == geocode OR er  then search in radius

        if ($placetype == 'area') {
            $min_lat = $params['min_lat'];
            $max_lat = $params['max_lat'];
            $min_long = $params['min_long'];
            $max_long = $params['max_long'];

            $query_string = " AND lati BETWEEN " . $min_lat . " AND  " . $max_lat . "  AND longi BETWEEN  " . $min_long . "  AND  " . $max_long . "  ";
            $search_entity = "SELECT eml.*, eml.entity_id as id,
                                CASE WHEN CONCAT('" . base_url() . UPLOAD_DIR . ENTITY_IMAGE_DIR . THUMB_DIR . "',eml.entity_id, '" . DIR_SEP . "',path)
                                     IS NOT NULL
                                     THEN CONCAT('" . base_url() . UPLOAD_DIR . ENTITY_IMAGE_DIR . THUMB_DIR . "',eml.entity_id, '" . DIR_SEP . "',path)
                                     ELSE '" . base_url() . UPLOAD_DIR . DEFAULT_DIR . ENTITY_IMAGE_DIR . THUMB_DIR . DEFAULT_ENTITY_IMAGE . "'
                                END AS path, '" . base_url() . "' as base_url" . $mapradiuscondi . " FROM `entity_master_list` eml JOIN
                                  `entity_attachments_list` eal ON eml.entity_id = eal.entity_id  WHERE 1=1
                                 AND eal.type = 'THUMB' " . $query_string;
            $stmt = $this->db->conn_id->prepare($search_entity);
            $stmt->execute();
            $search_data = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $search_data;
        }

        if ($lat != '' && $long != '') {
            $mapradiuscondi = " ,(
      6371 * ACOS (
      COS ( RADIANS('" . $lat . "') )
      * COS( RADIANS( lati ) )
      * COS( RADIANS( longi ) - RADIANS('" . $long . "') )
      + SIN ( RADIANS('" . $lat . "') )
      * SIN( RADIANS( lati ) )
    ) ) AS distance ";
            $mapshavingcondi = " HAVING distance < '" . CITY_RADIUS . "' ";
        }

        $search_entity = "SELECT eml.*, eml.entity_id as id,
                                CASE WHEN CONCAT('" . base_url() . UPLOAD_DIR . ENTITY_IMAGE_DIR . THUMB_DIR . "',eml.entity_id, '" . DIR_SEP . "',path)
                                     IS NOT NULL
                                     THEN CONCAT('" . base_url() . UPLOAD_DIR . ENTITY_IMAGE_DIR . THUMB_DIR . "',eml.entity_id, '" . DIR_SEP . "',path)
                                     ELSE '" . base_url() . UPLOAD_DIR . DEFAULT_DIR . ENTITY_IMAGE_DIR . THUMB_DIR . DEFAULT_ENTITY_IMAGE . "'
                                END AS path, '" . base_url() . "' as base_url" . $mapradiuscondi . " FROM `entity_master_list` eml JOIN
                                  `entity_attachments_list` eal ON eml.entity_id = eal.entity_id  WHERE 1=1
                                 AND eal.type = 'THUMB'" . $othersearch . $mapshavingcondi;

        $stmt = $this->db->conn_id->prepare($search_entity);
        $stmt->execute();
        $search_data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $search_data;
    }

    function get_currency_code($word) {
        $curr_code = "select * from country_state where title like '" . $word . "' and parentid='0'";
        $stmt = $this->db->conn_id->prepare($curr_code);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    function get_amenities($params) {
        $entity_id = $params['unit_id'];
        $amenities = "SELECT * FROM `entity_amenity_list`  WHERE unit_id = '" . $entity_id . "' OR hashval = '" . $entity_id . "'";
        $stmt = $this->db->conn_id->prepare($amenities);
        $stmt->execute();
        return $stmt->fetchALL(PDO::FETCH_ASSOC);
    }

    function get_state_from_city_id($city_id) {
        $get_state_id = "SELECT * FROM `city_master` WHERE id = ?";
        $stmt = $this->db->conn_id->prepare($get_state_id);
        $stmt->execute(array($city_id));
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    function entity_attatchments($hashval) {
        $entity_data = $this->entity_details($hashval);
        $unit_id = isset($entity_data['unit_id']) ? $entity_data['unit_id'] : '';
        $amenities = "SELECT * FROM `entity_attatchments` WHERE unit_id = ? AND status = '1' AND (type = '0' OR type = '1')";
        $stmt = $this->db->conn_id->prepare($amenities);
        $stmt->execute(array($unit_id));
        return $stmt->fetchALL(PDO::FETCH_ASSOC);
    }

    function get_entity_view_count_today($params) {
        $ip_address = $params['ip_address'];
        $today = date('Y-m-d');
        $entity_view_count = "SELECT count(*) as view_count FROM entity_view_log WHERE ip_address = ? AND DATE(created_date) = ?";
        $stmt = $this->db->conn_id->prepare($entity_view_count);
        $stmt->execute(array($ip_address, $today));
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    function insert_view_log($params = array()) {
        $entity_id = $params['id'];
        $user_id = $params['user_id'];
        $email = $params['email'];
        $ip_address = $params['ip_address'];

        // Get count to avoid duplicate views
        $count = $this->get_entity_view_count_today($params);
        if ($count['view_count'] <= 0) {
            $inquiry_query = "INSERT INTO entity_view_log(user_id, email, ip_address, entity_id) VALUES (?, ?, ?, ?)";
            $stmt = $this->db->conn_id->prepare($inquiry_query);
            $stmt->execute(array($user_id, $email, $ip_address, $entity_id));
            $generated_id = $this->db->insert_id();
            $this->update_entity_view_count($params);
            return $generated_id;
        }
        return;
    }

    function update_rating($params) {
        $entity_id = $params['id'];
        $ratings = $params['ratings'];
        $count_view_query = "UPDATE entity_master SET ratings = ? WHERE id = ?";
        $stmt = $this->db->conn_id->prepare($count_view_query);
        $stmt->execute(array($ratings, $entity_id));
    }

    function update_entity_view_count($params) {
        $entity_id = $params['id'];
        $count_view_query = "UPDATE unit_master SET visits = (visits + 1) WHERE id = ?";
        $stmt = $this->db->conn_id->prepare($count_view_query);
        $stmt->execute(array($entity_id));
    }

    function get_event_types() {
        $amenities = "SELECT id, `type` FROM event_type  WHERE STATUS = 'ACTIVE'  ORDER BY display_order ASC ";
        $stmt = $this->db->conn_id->prepare($amenities);
        $stmt->execute();
        return $stmt->fetchALL(PDO::FETCH_ASSOC);
    }

    function get_entity_by_hash($hash) {
        $query = "SELECT * FROM `unit_master_list` WHERE unit_hashval = ?";
        $stmt = $this->db->conn_id->prepare($query);
        $stmt->execute(array($hash));
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    function get_trade_by_id($hash) {
        $query = "SELECT * FROM `trades` WHERE id = ?";
        $stmt = $this->db->conn_id->prepare($query);
        $stmt->execute(array($hash));
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    function get_entity_by_user($user_id) {
        $query = "SELECT * FROM `entity_master_list` WHERE user_id = :user_id";
        $stmt = $this->db->conn_id->prepare($query);
        $stmt->execute(array(':user_id' => $user_id));
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    function send_inquiry($params = array()) {
        $name = $params['name'];
        $contact = $params['contact'];
        $email = $params['email'];
        $comments = $params['comments'];

        $inquiry_query = "INSERT  INTO inquiry
                (name, email, contact, comments, created_date)
                VALUES (?, ?, ?, ?, NOW()); ";
        $inquiry_stmt = $this->db->conn_id->prepare($inquiry_query);
        $inquiry_stmt->execute(array($name, $contact, $email, $comments));
        //echo var_export($user_register_stmt->errorInfo()); exit;
        $generated_id = $this->db->insert_id();
        //$data['generated_id'] = $generated_id;
        return $generated_id;
    }

    function update_entity_hashval($entity_hash, $entity_id) {
        $entity_hash_update_query = "UPDATE entity_master SET hashval = ? WHERE id = ?";
        $entity_hash_update_stmt = $this->db->conn_id->prepare($entity_hash_update_query);
        $entity_hash_update_stmt->execute(array($entity_hash, $entity_id));
    }

    function update_permalink($unit_hashval) {
        $changed_by = get_session_user_id();
        $entity_data = $this->get_entity_by_hash($unit_hashval);
        $entity_name = $entity_data['addr_placeholder'];
        $title = $entity_data['addr_placeholder'] . ' - ' . SITETITLE;
        if ($entity_data['place_type'] == 1) {
            $type = 'residential property';
        } else {
            $type = 'property';
        }
        $default_keywords = $type . ' for rent at ' . $entity_data['addr_placeholder'];
        /* if (trim($entity_data['searchtags']) != '') {
          $keyword = strtolower($entity_data['searchtags'] . ', ' . $default_keywords);
          } else {
          $keyword = strtolower($default_keywords);
          } */
        $keyword = $default_keywords;
        $unit_id = $entity_data['unit_id'];
        $description = $entity_data['building_desc'];
        $permalink = get_entity_permalink($entity_name);
        $permalink_update_query = "UPDATE unit_master SET permalink = ? WHERE id = ?";
        $permalink_update_stmt = $this->db->conn_id->prepare($permalink_update_query);
        $permalink_update_stmt->execute(array($permalink, $unit_id));
        //echo var_export($permalink_update_stmt->errorInfo());die;
        // Insert/Update SEO Master
        $query = "SELECT count(*) seo_master_count FROM `seo_master` WHERE unique_id = ?";
        $stmt = $this->db->conn_id->prepare($query);
        $stmt->execute(array($unit_id));
        $seo_data = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($seo_data['seo_master_count'] == 0) {
            // Insert
            $insert_query = "INSERT INTO seo_master (unique_id, method, parameter, title, keyword, description, created_by, created_date) "
                    . "VALUES (?, ?, ?, ?, ?, ?, ?, NOW())";
            $insert_stmt = $this->db->conn_id->prepare($insert_query);
            $insert_stmt->execute(array($unit_id, 'property_details', $permalink, $title, $keyword, $description, $changed_by));
            //echo var_export($insert_stmt->errorInfo());die;
        } else {
            // Update
            $update_query = "UPDATE seo_master SET parameter = ?, title = ?, keyword = ?, description = ?, modified_by = ?, modified_date = NOW() "
                    . " WHERE unique_id = ?";
            $update_query = $this->db->conn_id->prepare($update_query);
            $update_query->execute(array($permalink, $title, $keyword, $description, $changed_by, $unit_id));
            //echo var_export($update_query->errorInfo());die;
        }
    }

    function update_trade_permalink($trade_id) {
        $changed_by = get_session_user_id();
        $trade_data = $this->get_trade_by_id($trade_id);
        $trade_name = $trade_data['addr_placeholder'];
        $title = $trade_data['addr_placeholder'] . ' - ' . SITETITLE;
        $type = 'trades';

        $default_keywords = $type . ' for rent at ' . $trade_data['addr_placeholder'];
        /* if (trim($entity_data['searchtags']) != '') {
          $keyword = strtolower($entity_data['searchtags'] . ', ' . $default_keywords);
          } else {
          $keyword = strtolower($default_keywords);
          } */
        $keyword = $default_keywords;
        $trade_id = $trade_data['id'];
        $description = $trade_data['trade_desc'];
        $permalink = get_entity_permalink($trade_name);
        $permalink_update_query = "UPDATE trades SET permalink = ? WHERE id = ?";
        $permalink_update_stmt = $this->db->conn_id->prepare($permalink_update_query);
        $permalink_update_stmt->execute(array($permalink, $trade_id));
        //echo var_export($permalink_update_stmt->errorInfo());die;
    }

    function update_sla_map($sla_map_id) {
        $permalink_update_query = "UPDATE entity_sla SET
          status = 'ACCEPTED',
          accepted_date = CURRENT_TIMESTAMP
          WHERE id = :sla_map_id";
        $permalink_update_stmt = $this->db->conn_id->prepare($permalink_update_query);
        $permalink_update_stmt->execute(array(':sla_map_id' => $sla_map_id));
    }

    function update_entity_sla_status($entity_id, $value) {
        $permalink_update_query = "UPDATE entity_master SET sla_check = :sla_check WHERE id = :entity_id";
        $permalink_update_stmt = $this->db->conn_id->prepare($permalink_update_query);
        $permalink_update_stmt->execute(array(':entity_id' => $entity_id, ':sla_check' => $value));
    }

    function addupdate_entity_taxes($params, $entity_id) {
        // First set everything to inactive
        $update_query = "UPDATE entity_tax SET status = 'INACTIVE' WHERE entity_id = ?";
        $update_stmt = $this->db->conn_id->prepare($update_query);
        $update_stmt->execute(array($entity_id));
        $changed_by = get_session_user_id();
        for ($i = 0; $i < count($params); $i++) {
            $tax_id = $params[$i];
            $select = "SELECT * FROM entity_tax WHERE tax_id = ? AND entity_id = ?";
            $stmt = $this->db->conn_id->prepare($select);
            $stmt->execute(array($tax_id, $entity_id));
            $num_row = $stmt->rowCount();
            if ($num_row >= 1) {
                $update_query = "UPDATE entity_tax SET status = 'ACTIVE', modified_by = ? WHERE tax_id = ? AND entity_id = ?";
                $update_stmt = $this->db->conn_id->prepare($update_query);
                $update_stmt->execute(array($changed_by, $tax_id, $entity_id));
            } else {
                $insert_query = "INSERT INTO entity_tax (tax_id, entity_id, status, created_by, created_date) "
                        . "VALUES (?, ?, 'ACTIVE', ?, NOW())";
                $insert_stmt = $this->db->conn_id->prepare($insert_query);
                $insert_stmt->execute(array($tax_id, $entity_id, $changed_by));
            }
        }
        //echo var_export($permalink_update_stmt->errorInfo());die;
    }

    function addupdate_basic_entity($params = array()) {
        $building_id = __echo($params, 'building_id');
        $unit_hashval = __echo($params, 'unit_hashval');
        $partner_hashval = __echo($params, 'partner_hashval');
        $neighbourhood_name = $params['neighbourhood_name'];
        $building_desc = $params['building_desc'];
        $place_type = $params['place_type'];
        $place_sub_type = $params['place_sub_type'];
        $unit_name = $params['unit_name'];
        $price = $params['price'];
        $sqft = $params['sqft'];
        $bedrooms = $params['bedrooms'];
        $washrooms = $params['washrooms'];
        $smoking = $params['smoking'];
        $pets = $params['pets'];
        $parking = $params['parking'];
        $from_date = date('Y-m-d', strtotime($params['from_date']));
        $agreement_type = $params['agreement_type'];
        $features = isset($params['features']) ? $params['features'] : '';
        $appliances = isset($params['appliances']) ? $params['appliances'] : '';
        $utilities_inc = isset($params['utilities_inc']) ? $params['utilities_inc'] : '';
        $paid_utilities = isset($params['paid_utilities']) ? $params['paid_utilities'] : '';
        $input_address = $params['input_address'];
        $street_number = $params['street_number'];
        $route = $params['route'];
        $city = $params['locality'];
        $province = $params['administrative_area_level_1'];
        $country = $params['country'];
        $city_id = $params['c_id'];
        $postalcode = $params['postal_code'];
        $lati = $params['lati'];
        $longi = $params['longi'];
        $contact_person = $params['contact_person'];
        $email = $params['email'];
        $phone = $params['phone'];
        $user_id = get_session_user_id();
        if ($unit_hashval == '') {
            // Insert into building details table
            $insert_building_details = "INSERT INTO building_master(user_id, neighbourhood_name, building_desc, place_type, place_sub_type, addr_placeholder, addr_street, addr_area, city_id, "
                    . "postalcode, lati, longi, contact_person, email, phone, created_by, created_date) "
                    . "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, NOW())";
            $insert_building_details_stmt = $this->db->conn_id->prepare($insert_building_details);
            $insert_building_details_stmt->execute(array($user_id, $neighbourhood_name, $building_desc, $place_type, $place_sub_type, $input_address, $street_number, $route, $city_id,
                $postalcode, $lati, $longi, $contact_person, $email, $phone, $user_id));
            //echo var_export($insert_building_details_stmt->errorInfo()); exit;
            $generated_id = $this->db->insert_id();
            $last_updated_id = $generated_id;

            // Insert into unit master table
            $unit_hash = get_random_md5($generated_id . time()); // This is just a hash
            $insert_unit_master = "INSERT INTO unit_master(unit_name, unit_hashval, building_id, sqft, bedrooms, washrooms, smoking, pets, parking, price, from_date, agreement_type, status) "
                    . "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, 1)";
            $insert_unit_master_stmt = $this->db->conn_id->prepare($insert_unit_master);
            $insert_unit_master_stmt->execute(array($unit_name, $unit_hash, $generated_id, $sqft, $bedrooms, $washrooms, $smoking, $pets, $parking, $price, $from_date, $agreement_type));
            //echo var_export($insert_unit_master_stmt->errorInfo()); exit;
            $generated_unit_id = $this->db->insert_id();
            $this->update_permalink($unit_hash);

            //Insert tax details as it is missing
            if (!empty($features)) {
                foreach ($features as $amenity) {
                    $insert_amenities = "INSERT INTO entity_amenity (unit_id, amenity_id, included, status, created_by, created_date) VALUES(?, ?, 0, 1, ?, NOW())";
                    $insert_amenities_stmt = $this->db->conn_id->prepare($insert_amenities);
                    $insert_amenities_stmt->execute(array($last_updated_id, $amenity, $user_id));
                    //echo var_export($insert_amenities_stmt->errorInfo()); exit;
                }
            }
            if (!empty($appliances)) {
                foreach ($appliances as $amenity) {
                    $insert_amenities = "INSERT INTO entity_amenity (unit_id, amenity_id, included, status, created_by, created_date) VALUES(?, ?, 0, 1, ?, NOW())";
                    $insert_amenities_stmt = $this->db->conn_id->prepare($insert_amenities);
                    $insert_amenities_stmt->execute(array($last_updated_id, $amenity, $user_id));
                    //echo var_export($insert_amenities_stmt->errorInfo()); exit;
                }
            }
            if (!empty($utilities_inc)) {
                foreach ($utilities_inc as $amenity) {
                    $insert_amenities = "INSERT INTO entity_amenity (unit_id, amenity_id, included, status, created_by, created_date) VALUES(?, ?, 0, 1, ?, NOW())";
                    $insert_amenities_stmt = $this->db->conn_id->prepare($insert_amenities);
                    $insert_amenities_stmt->execute(array($last_updated_id, $amenity, $user_id));
                    //echo var_export($insert_amenities_stmt->errorInfo()); exit;
                }
            }
            if (!empty($paid_utilities)) {
                foreach ($paid_utilities as $amenity) {
                    $insert_amenities = "INSERT INTO entity_amenity (unit_id, amenity_id, included, status, created_by, created_date) VALUES(?, ?, 1, 1, ?, NOW())";
                    $insert_amenities_stmt = $this->db->conn_id->prepare($insert_amenities);
                    $insert_amenities_stmt->execute(array($last_updated_id, $amenity, $user_id));
                    //echo var_export($insert_amenities_stmt->errorInfo()); exit;
                }
            }
            $data['generated_id'] = $generated_unit_id;
            $data['unit_hashval'] = $unit_hash;
        } else {
            $update_query = "UPDATE `building_master` SET user_id = ?, neighbourhood_name = ?, building_desc =?, place_type =?, place_sub_type =?, addr_placeholder =?, "
                    . "addr_street =?, addr_area =?, city_id =?, postalcode =?, lati =?, longi =?, contact_person =?, email =?, phone =?, modified_by =?, "
                    . "modified_date = NOW() WHERE `id` = ?";
            $update_stmt = $this->db->conn_id->prepare($update_query);
            $update_stmt->execute(array($user_id, $neighbourhood_name, $building_desc, $place_type, $place_sub_type, $input_address, $street_number, $route, $city_id,
                $postalcode, $lati, $longi, $contact_person, $email, $phone, $user_id, $building_id));
            //echo var_export($update_stmt->errorInfo()); exit;
            $this->update_permalink($unit_hashval);

            $update_unit_master = "UPDATE unit_master SET unit_name=?, sqft=?, bedrooms=?, washrooms=?, smoking=?, pets=? , parking=?, price=?, from_date=?, agreement_type=? WHERE unit_hashval=?";
            $update_unit_master_stmt = $this->db->conn_id->prepare($update_unit_master);
            $update_unit_master_stmt->execute(array($unit_name, $sqft, $bedrooms, $washrooms, $smoking, $pets, $parking, $price, $from_date, $agreement_type, $unit_hashval));
            //echo var_export($update_unit_master_stmt->errorInfo()); exit;

            $unit_data = $this->get_entity_by_hash($unit_hashval);
            $last_updated_id = $unit_data['unit_id'];
            $delete_amenity = "DELETE FROM entity_amenity WHERE unit_id = ?";
            $delete_amenity_stmt = $this->db->conn_id->prepare($delete_amenity);
            $delete_amenity_stmt->execute(array($last_updated_id));
            //echo var_export($delete_amenity_stmt->errorInfo()); exit;

            if (!empty($features)) {
                foreach ($features as $amenity) {
                    $insert_amenities = "INSERT INTO entity_amenity (unit_id, amenity_id, included, status, created_by, created_date) VALUES(?, ?, 0, 1, ?, NOW())";
                    $insert_amenities_stmt = $this->db->conn_id->prepare($insert_amenities);
                    $insert_amenities_stmt->execute(array($last_updated_id, $amenity, $user_id));
                    //echo var_export($insert_amenities_stmt->errorInfo()); exit;
                }
            }
            if (!empty($appliances)) {
                foreach ($appliances as $amenity) {
                    $insert_amenities = "INSERT INTO entity_amenity (unit_id, amenity_id, included, status, created_by, created_date) VALUES(?, ?, 0, 1, ?, NOW())";
                    $insert_amenities_stmt = $this->db->conn_id->prepare($insert_amenities);
                    $insert_amenities_stmt->execute(array($last_updated_id, $amenity, $user_id));
                    //echo var_export($insert_amenities_stmt->errorInfo()); exit;
                }
            }
            if (!empty($utilities_inc)) {
                foreach ($utilities_inc as $amenity) {
                    $insert_amenities = "INSERT INTO entity_amenity (unit_id, amenity_id, included, status, created_by, created_date) VALUES(?, ?, 0, 1, ?, NOW())";
                    $insert_amenities_stmt = $this->db->conn_id->prepare($insert_amenities);
                    $insert_amenities_stmt->execute(array($last_updated_id, $amenity, $user_id));
                    //echo var_export($insert_amenities_stmt->errorInfo()); exit;
                }
            }
            if (!empty($paid_utilities)) {
                foreach ($paid_utilities as $amenity) {
                    $insert_amenities = "INSERT INTO entity_amenity (unit_id, amenity_id, included, status, created_by, created_date) VALUES(?, ?, 1, 1, ?, NOW())";
                    $insert_amenities_stmt = $this->db->conn_id->prepare($insert_amenities);
                    $insert_amenities_stmt->execute(array($last_updated_id, $amenity, $user_id));
                    //echo var_export($insert_amenities_stmt->errorInfo()); exit;
                }
            }

            $data['generated_id'] = $last_updated_id;
            $data['unit_hashval'] = $unit_hashval;
        }
        return $data;
    }

    function addupdate_building_unit($params = array()) {
        $last_updated_id = '';
        $building_id = __echo($params, 'building_id');
        $parking_id = __echo($params, 'parking_id');
        $unit_hashval = __echo($params, 'unit_hashval');
        $unit_name = $params['unit_name'];
        $price = $params['price'];
        $sqft = $params['sqft'];
        $bedrooms = $params['bedrooms'];
        $washrooms = $params['washrooms'];
        $smoking = $params['smoking'];
        $pets = $params['pets'];
        $parking = $params['parking'];
        $exclusive = $params['exclusive'];
        $titled = $params['titled'];
        $surface = $params['surface'];
        $underground = $params['underground'];
        $parking_desc = $params['parking_desc'];
        $from_date = date('Y-m-d', strtotime($params['from_date']));
        $agreement_type = $params['agreement_type'];
        $features = isset($params['features']) ? $params['features'] : '';
        $appliances = isset($params['appliances']) ? $params['appliances'] : '';
        $utilities_inc = isset($params['utilities_inc']) ? $params['utilities_inc'] : '';
        $paid_utilities = isset($params['paid_utilities']) ? $params['paid_utilities'] : '';
        $common_area_amenities = isset($params['common_area_amenities']) ? $params['common_area_amenities'] : '';
        $user_id = get_session_user_id();
        if ($unit_hashval == '') {
            // Insert into unit master table
            $insert_unit_master = "INSERT INTO unit_master(unit_name, building_id, sqft, bedrooms, washrooms, smoking, pets, parking, price, from_date, agreement_type, status) "
                    . "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, 1)";
            $insert_unit_master_stmt = $this->db->conn_id->prepare($insert_unit_master);
            $insert_unit_master_stmt->execute(array($unit_name, $building_id, $sqft, $bedrooms, $washrooms, $smoking, $pets, $parking, $price, $from_date, $agreement_type));
            //echo var_export($insert_unit_master_stmt->errorInfo()); exit;
            $unit_id = $this->db->insert_id();
            //Insert into Parking_master table for unit
            $insert_parking_master = "INSERT INTO `parking_master`(`building_id`, `unit_id`, `parking`, `exclusive`, `titled`, `surface`, `underground`, `parking_desc`) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
            $insert_parking_master_stmt = $this->db->conn_id->prepare($insert_parking_master);
            $insert_parking_master_stmt->execute([$building_id, $unit_id, $parking, $exclusive, $titled, $surface, $underground, $parking_desc]);
            //echo var_export($insert_parking_master_stmt->errorInfo()); exit;
            //update unit hashval
            $unit_hash = get_random_md5($unit_id . time()); // This is just a hash
            $this->update_unit_hash($unit_hash, $unit_id);
            //end
            $this->update_permalink($unit_hash);

            //Insert tax details as it is missing
            if (!empty($features)) {
                foreach ($features as $amenity) {
                    $insert_amenities = "INSERT INTO entity_amenity (unit_id, amenity_id, included, status, created_by, created_date) VALUES(?, ?, 0, 1, ?, NOW())";
                    $insert_amenities_stmt = $this->db->conn_id->prepare($insert_amenities);
                    $insert_amenities_stmt->execute(array($unit_id, $amenity, $user_id));
                    //echo var_export($insert_amenities_stmt->errorInfo()); exit;
                }
            }
            if (!empty($appliances)) {
                foreach ($appliances as $amenity) {
                    $insert_amenities = "INSERT INTO entity_amenity (unit_id, amenity_id, included, status, created_by, created_date) VALUES(?, ?, 0, 1, ?, NOW())";
                    $insert_amenities_stmt = $this->db->conn_id->prepare($insert_amenities);
                    $insert_amenities_stmt->execute(array($unit_id, $amenity, $user_id));
                    //echo var_export($insert_amenities_stmt->errorInfo()); exit;
                }
            }
            if (!empty($utilities_inc)) {
                foreach ($utilities_inc as $amenity) {
                    $insert_amenities = "INSERT INTO entity_amenity (unit_id, amenity_id, included, status, created_by, created_date) VALUES(?, ?, 0, 1, ?, NOW())";
                    $insert_amenities_stmt = $this->db->conn_id->prepare($insert_amenities);
                    $insert_amenities_stmt->execute(array($unit_id, $amenity, $user_id));
                    //echo var_export($insert_amenities_stmt->errorInfo()); exit;
                }
            }
            if (!empty($common_area_amenities)) {
                foreach ($common_area_amenities as $amenity) {
                    $insert_amenities = "INSERT INTO entity_amenity (unit_id, amenity_id, included, status, created_by, created_date) VALUES(?, ?, 0, 1, ?, NOW())";
                    $insert_amenities_stmt = $this->db->conn_id->prepare($insert_amenities);
                    $insert_amenities_stmt->execute(array($unit_id, $amenity, $user_id));
                    //echo var_export($insert_amenities_stmt->errorInfo()); exit;
                }
            }
            if (!empty($paid_utilities)) {
                foreach ($paid_utilities as $amenity) {
                    $insert_amenities = "INSERT INTO entity_amenity (unit_id, amenity_id, included, status, created_by, created_date) VALUES(?, ?, 1, 1, ?, NOW())";
                    $insert_amenities_stmt = $this->db->conn_id->prepare($insert_amenities);
                    $insert_amenities_stmt->execute(array($unit_id, $amenity, $user_id));
                    //echo var_export($insert_amenities_stmt->errorInfo()); exit;
                }
            }
            $data['generated_id'] = $unit_id;
            $data['unit_hashval'] = $unit_hash;
        } else {

            // Update Unit Master Query
            $update_unit_master = "UPDATE unit_master SET unit_name=?, sqft=?, bedrooms=?, washrooms=?, smoking=?, pets=? , parking=?, price=?, from_date=?, agreement_type=? WHERE unit_hashval=?";
            $update_unit_master_stmt = $this->db->conn_id->prepare($update_unit_master);
            $update_unit_master_stmt->execute(array($unit_name, $sqft, $bedrooms, $washrooms, $smoking, $pets, $parking, $price, $from_date, $agreement_type, $unit_hashval));
            //echo var_export($update_unit_master_stmt->errorInfo()); exit;
            $this->update_permalink($unit_hashval);

            // Update Parking_master Query
            $update_propery_master = "UPDATE `parking_master` SET `building_id` = ?, `unit_id` = ?, `parking` = ?, `exclusive` = ?, `titled` = ?, `surface` = ?, `underground` = ?, `parking_desc` = ? WHERE `id` = ?";
            $update_propery_master_stmt = $this->db->conn_id->prepare($update_propery_master);
            $update_propery_master_stmt->execute([$building_id, $unit_id, $parking, $exclusive, $titled, $surface, $underground, $parking_desc, $parking_id]);
            //echo var_export($update_propery_master_stmt->errorInfo()); exit;

            $unit_data = $this->get_entity_by_hash($unit_hashval);
            $last_updated_id = $unit_data['unit_id'];
            $delete_amenity = "DELETE FROM entity_amenity WHERE unit_id = ?";
            $delete_amenity_stmt = $this->db->conn_id->prepare($delete_amenity);
            $delete_amenity_stmt->execute(array($last_updated_id));
            //echo var_export($delete_amenity_stmt->errorInfo()); exit;

            if (!empty($features)) {
                foreach ($features as $amenity) {
                    $insert_amenities = "INSERT INTO entity_amenity (unit_id, amenity_id, included, status, created_by, created_date) VALUES(?, ?, 0, 1, ?, NOW())";
                    $insert_amenities_stmt = $this->db->conn_id->prepare($insert_amenities);
                    $insert_amenities_stmt->execute(array($last_updated_id, $amenity, $user_id));
                    //echo var_export($insert_amenities_stmt->errorInfo()); exit;
                }
            }
            if (!empty($appliances)) {
                foreach ($appliances as $amenity) {
                    $insert_amenities = "INSERT INTO entity_amenity (unit_id, amenity_id, included, status, created_by, created_date) VALUES(?, ?, 0, 1, ?, NOW())";
                    $insert_amenities_stmt = $this->db->conn_id->prepare($insert_amenities);
                    $insert_amenities_stmt->execute(array($last_updated_id, $amenity, $user_id));
                    //echo var_export($insert_amenities_stmt->errorInfo()); exit;
                }
            }
            if (!empty($utilities_inc)) {
                foreach ($utilities_inc as $amenity) {
                    $insert_amenities = "INSERT INTO entity_amenity (unit_id, amenity_id, included, status, created_by, created_date) VALUES(?, ?, 0, 1, ?, NOW())";
                    $insert_amenities_stmt = $this->db->conn_id->prepare($insert_amenities);
                    $insert_amenities_stmt->execute(array($last_updated_id, $amenity, $user_id));
                    //echo var_export($insert_amenities_stmt->errorInfo()); exit;
                }
            }
            if (!empty($paid_utilities)) {
                foreach ($paid_utilities as $amenity) {
                    $insert_amenities = "INSERT INTO entity_amenity (unit_id, amenity_id, included, status, created_by, created_date) VALUES(?, ?, 1, 1, ?, NOW())";
                    $insert_amenities_stmt = $this->db->conn_id->prepare($insert_amenities);
                    $insert_amenities_stmt->execute(array($last_updated_id, $amenity, $user_id));
                    //echo var_export($insert_amenities_stmt->errorInfo()); exit;
                }
            }
        }
        if ($unit_id != '')
            return $unit_id;
        else
            return $last_updated_id;
    }

    function update_unit_hash($unit_hash, $id) {

        $update_query = "UPDATE `unit_master` SET `unit_hashval` = ? WHERE `id` = ?";
        $update_stmt = $this->db->conn_id->prepare($update_query);
        $update_stmt->execute([$unit_hash, $id]);
    }

    function addupdate_building($params = array()) {
        $user_id = get_session_user_id();
        $building_id = __echo($params, 'building_id');
        $building_name = __echo($params, 'building_name');
        $city_id = __echo($params, 'c_id');
        $city_name = isset($params['city_name']) ? $params['city_name'] : '';
        $neighbourhood_name = isset($params['neighbourhood_name']) ? $params['neighbourhood_name'] : '';
        $building_desc = isset($params['building_desc']) ? $params['building_desc'] : '';
        $place_type = isset($params['place_type']) ? $params['place_type'] : '';
        $place_sub_type = isset($params['place_sub_type']) ? $params['place_sub_type'] : '';
        $input_address = isset($params['input_address']) ? $params['input_address'] : '';
        $street_number = isset($params['street_number']) ? $params['street_number'] : '';
        $route = isset($params['route']) ? $params['route'] : '';
        $city = isset($params['locality']) ? $params['locality'] : '';
        $province = isset($params['administrative_area_level_1']) ? $params['administrative_area_level_1'] : '';
        $country = isset($params['country']) ? $params['country'] : '';
        $postalcode = isset($params['postal_code']) ? $params['postal_code'] : '';
        $lati = isset($params['lati']) ? $params['lati'] : '';
        $longi = isset($params['longi']) ? $params['longi'] : '';
        $contact_person = isset($params['contact_person']) ? $params['contact_person'] : '';
        $email = isset($params['email']) ? $params['email'] : '';
        $phone = isset($params['phone']) ? $params['phone'] : '';
        if ($building_id == '') {
            // Insert into building details table
            $insert_building_details = "INSERT INTO `building_master`(`user_id`, city_name, `neighbourhood_name`, building_name, `building_desc`, `place_type`, `place_sub_type`, `addr_placeholder`, `addr_street`, `addr_area`, `city_id`, `postalcode`, `lati`, `longi`, `contact_person`, `email`, `phone`, `created_by`, `modified_by`) "
                    . "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            $insert_building_details_stmt = $this->db->conn_id->prepare($insert_building_details);
            $insert_building_details_stmt->execute([$user_id, $city_name, $neighbourhood_name, $building_name, $building_desc, $place_type, $place_sub_type, $input_address, $street_number, $route, $city_id, $postalcode, $lati, $longi, $contact_person, $email, $phone, $user_id, $user_id]);
            //echo var_export($insert_building_details_stmt->errorInfo()); exit;
            $building_id = $this->db->insert_id();
        } else {
            //echo $update_query = "UPDATE `building_master` SET `city_name` = $city_name,`neighbourhood_name` = $neighbourhood_name, `building_desc` = $building_desc, `place_type` = $place_type, `place_sub_type` = $place_sub_type, `addr_placeholder` = $input_address, `addr_street` = $street_number, `addr_area` = $route, `city_id` = $city, `postalcode` = $postalcode, `lati` = $lati, `longi` = $longi, `contact_person` = $contact_person, `email` = $email, `phone` = $phone, `modified_by` = $user_id, `modified_date` = NOW() WHERE `id` = $building_id"; die;
            $update_query = "UPDATE `building_master` SET `city_name` = ?,`neighbourhood_name` = ?, building_name = ?, `building_desc` = ?, `place_type` = ?, `place_sub_type` = ?, `addr_placeholder` = ?, `addr_street` = ?, `addr_area` = ?, `city_id` = ?, `postalcode` = ?, `lati` = ?, `longi` = ?, `contact_person` = ?, `email` = ?, `phone` = ?, `modified_by` = ?, `modified_date` = NOW() WHERE `id` = ?";
            $update_stmt = $this->db->conn_id->prepare($update_query);
            $update_stmt->execute([$city_name, $neighbourhood_name, $building_name, $building_desc, $place_type, $place_sub_type, $input_address
                , $street_number, $route, $city_id, $postalcode, $lati, $longi, $contact_person, $email, $phone, $user_id, $building_id]);
            //echo var_export($update_stmt->errorInfo()); exit;
        }
        return $building_id;
    }

    function addupdate_trade($params = array()) {
        $trade_id = __echo($params, 'trade_id');
        $trade_type_id = __echo($params, 'trade_type_id');
        $trade_name = __echo($params, 'trade_name');
        $trade_desc = __echo($params, 'trade_desc');
        //$permalink = __echo($params, 'permalink');
        $contact_person = __echo($params, 'contact_person');
        $email = __echo($params, 'email');
        $phone = __echo($params, 'phone');
        $facebook = __echo($params, 'facebook');
        $instagram = __echo($params, 'instagram');
        $addr_placeholder = __echo($params, 'input_address');
        $addr_street = __echo($params, 'street_number');
        $addr_area = __echo($params, 'route');
        $postalcode = __echo($params, 'postal_code');
        $city_id = __echo($params, 'city_id');
        //$locality = __echo($params, 'locality');
        $state_id = __echo($params, 'state_id');
        $county_id = __echo($params, 'country_id');
        $charge_type = __echo($params, 'charge_type');
        $charges = __echo($params, 'charges');
        $photo = __echo($params, 'photo');

        $user_id = get_session_user_id();
        if ($trade_id == '') {
            // Insert into building details table
            $insert_trade_details = "INSERT INTO `trades`(`trade_type_id`, `trade_name`, `trade_desc`,  `contact_person`, `email`, `phone`, `facebook`, `instagram`, `addr_placeholder`, `addr_street`, `addr_area`, `postalcode`, `city_id`, `state_id`, `county_id`, `charge_type`, `charges`, `photo`, `status`) "
                    . "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, 1)";
            $insert_trade_details_stmt = $this->db->conn_id->prepare($insert_trade_details);
            $insert_trade_details_stmt->execute([$trade_type_id, $trade_name, $trade_desc, $contact_person, $email, $phone, $facebook, $instagram, $addr_placeholder, $addr_street, $addr_area, $postalcode, $city_id, $state_id, $county_id, $charge_type, $charges, $photo]);
            //echo var_export($insert_trade_details_stmt->errorInfo()); exit;
            $generated_id = $this->db->insert_id();
            $this->update_trade_permalink($trade_id);

            $data['generated_id'] = $generated_id;
        } else {
            $update_query = "UPDATE `trades` SET `trade_type_id` = ?, `trade_name` = ?, `trade_desc` = ?, `contact_person` = ?, `email` = ?, `phone` = ?, `facebook` = ?, `instagram` = ?, `addr_placeholder` = ?, `addr_street` = ?, `addr_area` = ?, `postalcode` = ?, `city_id` = ?, `state_id` = ?, `county_id` = ?, `charge_type` = ?, `charges` = ?, `photo` = ? WHERE `id` = ?";
            $update_stmt = $this->db->conn_id->prepare($update_query);
            $update_stmt->execute([$trade_type_id, $trade_name, $trade_desc, $contact_person, $email, $phone, $facebook, $instagram, $addr_placeholder, $addr_street, $addr_area, $postalcode, $city_id, $state_id, $county_id, $charge_type, $charges, $photo, $trade_id]);
            // echo var_export($update_stmt->errorInfo()); exit;
            $this->update_trade_permalink($trade_id);

            $data['trade_id'] = $trade_id;
        }
        return $data;
    }

    function count_building_id($building_id) {
        $country_query = "SELECT  COUNT(*) building_id FROM unit_master WHERE building_id = ?";
        $stmt = $this->db->conn_id->prepare($country_query);
        $stmt->execute([$building_id]);
        //echo var_export($stmt->errorInfo());
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    function upload_photos_entity($params = array()) {
        $unit_id = $params['unit_id'];
        $type = $params['type'];
        $path = $params['path'];
        $order = $params['order'];
        $changed_by = get_session_user_id();
        if ($unit_id != '') {
            $insert_query = "INSERT INTO entity_attatchments(unit_id, type, path, orderby, status, created_by, created_date)
                VALUES (?, ?, ?, ?, 1, ?, NOW()); ";
            $insert_stmt = $this->db->conn_id->prepare($insert_query);
            $insert_stmt->execute(array($unit_id, $type, $path, $order, $changed_by));
            //echo var_export($insert_stmt->errorInfo()); exit;
            $generated_id = $this->db->insert_id();
            $data['last_id'] = $generated_id;
        }
        return $data;
    }

    function check_tumb_attatchment($params = array()) {
        $unit_id = $params['unit_id'];
        $select_query = "SELECT unit_id FROM entity_attatchments WHERE unit_id = ? AND type = 0";
        $select_stmt = $this->db->conn_id->prepare($select_query);
        $select_stmt->execute(array($unit_id));
        $num_row = $select_stmt->rowCount();
        if ($num_row >= 1) {
            $data['num_row'] = 1;
        } else {
            $data['num_row'] = 0;
        }
        return $data;
    }

    function share_log($params = array()) {
        $hashval = $params['hashval'];
        $fromname = $params['fromname'];
        $fromemail = $params['fromemail'];
        $toemail = $params['toemail'];
        $shared_by = get_session_user_id();
        $entity_id = $this->get_entity_id_by_hashval($hashval);

        if ($entity_id != '') {
            $insert_query = "INSERT INTO `entity_shared` (`entiy_id`, `fromname`, `fromemail`, `toemail`, `shared_by`, `shared_date`) "
                    . "VALUES (?, ?, ?, ?, ?, NOW());";
            $insert_stmt = $this->db->conn_id->prepare($insert_query);
            $insert_stmt->execute(array($entity_id, $fromname, $fromemail, $toemail, $shared_by));
            //echo var_export($insert_stmt->errorInfo()); exit;
            $generated_id = $this->db->insert_id();
            $data['generated_id'] = $generated_id;
        }
        return $data;
    }

    function get_entity_preview($params = array()) {
        return $this->load->view('frontend/_share_entity_details', $params, true);
    }

    function addupdate_block_dates($params = array()) {
        $hashval = __echo($params, 'hashval');
        $entity_id = $this->get_entity_id_by_hashval($hashval);
        $person_name = $params['person_name'];
        $person_email = $params['person_email'];
        $person_phone = $params['person_phone'];
        $event_time = date("H:i:s", strtotime($params['event_start']));
        $event_date = date("Y-m-d", strtotime($params['event_start']));
        $event_start = $event_date . ' ' . $event_time;
        $event_duration = $params['event_duration'];
        $blocked_reason = $params['blocked_reason'];
        $changed_by = get_session_user_id();
        $event_type = $params['event_type'];
        $block_id = $params['blocked_reason'];

        $occupied = $this->check_entity_occupied($params);

        // Adding / updating entity policy
        if (count($occupied) <= 0 && $block_id = '') {
            $insert_query = "INSERT INTO entity_occupied_dates (entity_hashval, customer_name, customer_email, customer_phone, from_date, event_duration, "
                    . "event_type, blocked_reason, created_by, created_date) "
                    . "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, NOW());";
            $insert_stmt = $this->db->conn_id->prepare($insert_query);
            $insert_stmt->execute(array($hashval, $person_name, $person_email, $person_phone, $event_start, $event_duration,
                $event_type, $blocked_reason, $changed_by));
            //echo var_export($insert_stmt->errorInfo());exit;
        } else {
            $update_query = "UPDATE entity_occupied_dates SET
                        customer_name = ?, customer_email = ?, customer_phone = ?, from_date = ?, event_duration = ?, event_type = ?,
                        blocked_reason = ?, created_by = ?, blocked_reason = ? WHERE entity_hashval = ? AND id = ?";
            $update_stmt = $this->db->conn_id->prepare($update_query);
            $update_stmt->execute(array($person_name, $person_email, $person_phone, $event_start, $event_duration, $event_type,
                $blocked_reason, $changed_by, $blocked_reason, $hashval, $block_id));
            //echo var_export($update_stmt->errorInfo());exit;
        }
        $data['hashval'] = $hashval;
        return $data;
    }

    function check_entity_occupied($params) {
        $hashval = __echo($params, 'hashval');
        $event_time = date("H:i:s", strtotime($params['event_start']));
        $event_date = date("Y-m-d", strtotime($params['event_start']));
        $event_start = $event_date . ' ' . $event_time;
        $query = "SELECT * FROM entity_occupied_dates WHERE entity_hashval = ? AND from_date = ?";
        $stmt = $this->db->conn_id->prepare($query);
        $stmt->execute(array($hashval, $event_start));
        //echo var_export($stmt->errorInfo());exit;
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    function get_blocked_entity_hashval($params) {
        $event_start = date("Y-m-d", strtotime($params['event_start']));
        $query = "SELECT * FROM entity_occupied_dates WHERE DATE_FORMAT(from_date, '%Y-%m-%d') = ?";
        $stmt = $this->db->conn_id->prepare($query);
        $stmt->execute(array($event_start));
        //echo var_export($stmt->errorInfo());exit;
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    function send_enquiry($params = array()) {
        $unit_id = $params['unit_id'];
        $user_id = $params['user_id'];
        $comments = $params['comments'];
        $max_request_id = $this->get_max_customer_requests();
        $req_num = 'CR' . date('Y') . '-' . sprintf('%05d', INTVAL($max_request_id['max_id']) + 1);
        if ($unit_id != '' && $user_id != '') {
            $req_hashval = get_random_md5($max_request_id['max_id'] + 1);
            $insert_query = "INSERT INTO customer_request (req_num, req_hashval, user_id, unit_id, comments, created_by, created_date) "
                    . "VALUES (?, ?, ?, ?, ?, ?, NOW());";
            $insert_stmt = $this->db->conn_id->prepare($insert_query);
            $insert_stmt->execute(array($req_num, $req_hashval, $user_id, $unit_id, $comments, $user_id));
        }
        $data['req_hashval'] = $req_hashval;
        return $data;
    }

    function send_service_inquiry($params = array()) {
        $trade_id = $params['trade_id'];
        $user_id = $params['user_id'];
        $comments = $params['comments'];
        $max_request_id = $this->get_max_service_requests();
        $req_num = 'SR' . date('Y') . '-' . sprintf('%05d', INTVAL($max_request_id['max_id']) + 1);
        if ($trade_id != '' && $user_id != '') {
            $req_hashval = get_random_md5($max_request_id['max_id'] + 1);
            $insert_query = "INSERT INTO service_request (req_num, req_hashval, user_id, trade_id, comments, created_by, created_date) "
                    . "VALUES (?, ?, ?, ?, ?, ?, NOW());";
            $insert_stmt = $this->db->conn_id->prepare($insert_query);
            $insert_stmt->execute(array($req_num, $req_hashval, $user_id, $trade_id, $comments, $user_id));
        }
        $data['req_hashval'] = $req_hashval;
        return $data;
    }

    function get_all_pending_requests() {
        $query = "SELECT * FROM customer_requests_list WHERE partner_status != 'QUOTE_CONFIRMED'";
        $stmt = $this->db->conn_id->prepare($query);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    function get_max_customer_requests() {
        $query = "SELECT max(id) max_id FROM customer_request";
        $stmt = $this->db->conn_id->prepare($query);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    function get_max_service_requests() {
        $query = "SELECT max(id) max_id FROM service_request";
        $stmt = $this->db->conn_id->prepare($query);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    function get_all_block_dates() {
        $query = "SELECT	em.entity_name,	eoc.* FROM	entity_occupied_dates AS eoc,	entity_master AS em WHERE	eoc.entity_hashval = em.hashval";
        $stmt = $this->db->conn_id->prepare($query);
        $stmt->execute();
        //echo var_export($stmt->errorInfo());
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $data;
    }

    function get_partner_pending_requests($user_id) {
        $query = "SELECT * FROM customer_requests_list WHERE user_id = ? AND request_process = 'REQ_SENT_CUST'";
        $stmt = $this->db->conn_id->prepare($query);
        $stmt->execute(array($user_id));
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    function get_all_confirmed_requests() {
        $query = "SELECT * FROM customer_requests_list WHERE request_process = 'BOOKING_CONFIRM_CUST'";
        $stmt = $this->db->conn_id->prepare($query);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    function get_partner_confirmed_requests($user_id) {
        $res = $this->get_entity_by_user($user_id);
        $entity_id = $res['entity_id'];
        $query = "SELECT * FROM customer_requests_list WHERE entity_id = ? AND request_process = 'BOOKING_CONFIRM_CUST'";
        $stmt = $this->db->conn_id->prepare($query);
        $stmt->execute(array($entity_id));
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    function addBlockedDates($params) {
        $entity_hashval = $params['entity_hashval'];
        $customer_name = $params['customer_name'];
        $customer_email = $params['customer_email'];
        $from_date = $params['from_date'];
        $to_date = $params['to_date'];
        $event_type = $params['event_type'];
        $created_by = $params['created_by'];
        $blocked_reason = $params['blocked_reason'];
        $created_date = date('Y-m-d h:i:s A');

        // Generate Ref_no
        $query = "SELECT max(id) as max_no FROM block_dates";
        $stmt = $this->db->conn_id->prepare($query);
        $stmt->execute();
        $max_data = $stmt->fetch(PDO::FETCH_ASSOC);
        $max_id = $max_data['max_no'] + 1;
        $ref_no = date('Y') . '-' . sprintf('%09d', INTVAL($max_id));
        $query = "INSERT INTO `block_dates` (`ref_no`, `entity_hashval`, `customer_name`, `customer_email`, `from_date`, `to_date`, `event_type`, `blocked_reason`, `created_date`, `created_by`) "
                . "VALUES (?,?,?,?,?,?,?,?,?,?)";
        $stmt = $this->db->conn_id->prepare($query);
        $stmt->execute(array($ref_no, $entity_hashval, $customer_name, $customer_email, $from_date, $to_date, $event_type, $blocked_reason, $created_date, $created_by));
        $blocked_id = $this->db->insert_id();
        return $ref_no;
    }

    function updateBlockedDates($params) {
        $ref_no = $params['ref_no'];
        $entity_hashval = $params['entity_hashval'];
        $customer_name = $params['customer_name'];
        $customer_email = $params['customer_email'];
        $from_date = $params['from_date'];
        $to_date = $params['to_date'];
        $event_type = $params['event_type'];
        $created_by = $params['created_by'];
        $blocked_reason = $params['blocked_reason'];
        $created_date = date('Y-m-d h:i:s A');

        $query = "UPDATE block_dates SET entity_hashval=?, customer_name=?, customer_email=?, from_date=?, to_date=?, event_type=?, "
                . "blocked_reason=?, created_date=?, created_by=? WHERE ref_no = ?";
        $stmt = $this->db->conn_id->prepare($query);
        $stmt->execute(array($entity_hashval, $customer_name, $customer_email, $from_date, $to_date, $event_type, $blocked_reason, $created_date, $created_by, $ref_no));
        return $ref_no;
    }

    function add_to_compare($params = array()) {
        $entity_id = $params['entity_id'];
        $location = $params['location'];
        $lati = $params['lati'];
        $longi = $params['longi'];
        $eventdate = date('Y-m-d H:i:s A', strtotime($params['eventdate']));
        $session_id = $params['session_id'];
        $changed_by = get_session_user_id();
        $created_date = date('Y-m-d h:i:s');
        $count = 0;
        $data['status'] = '';
        if ($entity_id != '') {
            $query = "SELECT count(id) as compare_count FROM entity_compare WHERE session_id = ?";
            $stmt = $this->db->conn_id->prepare($query);
            $stmt->execute(array($session_id));
            $items = $stmt->fetch(PDO::FETCH_ASSOC);
            $count = $items['compare_count'];
            if ($count <= MAX_CART) { // Count starts from 0
                // Checking if the item is already added or not
                $query = "SELECT count(id) as same_count FROM entity_compare WHERE entity_id = ? AND session_id = ?";
                $stmt = $this->db->conn_id->prepare($query);
                $stmt->execute(array($entity_id, $session_id));
                $items = $stmt->fetch(PDO::FETCH_ASSOC);
                $same_count = $items['same_count'];
                if ($same_count == 0) {
                    $insert_query = "INSERT INTO entity_compare (entity_id, location, lati, longi, event_date, session_id, created_by, created_date) "
                            . "VALUES (?, ?, ?, ?, ?, ?, ?, ?);";
                    $insert_stmt = $this->db->conn_id->prepare($insert_query);
                    $insert_stmt->execute(array($entity_id, $location, $lati, $longi, $eventdate, $session_id, $changed_by, $created_date));
                    $data['status'] = '1';
                    $data['count'] = $count + 1;
                }
            } else {
                $query = "DELETE FROM entity_compare WHERE session_id = ?";
                $stmt = $this->db->conn_id->prepare($query);
                $stmt->execute(array($session_id));
                $data['status'] = '';
            }
        }
        return $data;
    }

    function get_compare_list($session_id) {
        $query = "SELECT * FROM entity_compare_list WHERE session_id=?";
        $stmt = $this->db->conn_id->prepare($query);
        $stmt->execute(array($session_id));
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    function get_country_by_id($country_id) {
        $query = "SELECT * FROM country_master WHERE id=?";
        $stmt = $this->db->conn_id->prepare($query);
        $stmt->execute(array($country_id));
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    function get_seo_data($uri_segment) {
        $query = "SELECT * FROM seo_master WHERE parameter=?";
        $stmt = $this->db->conn_id->prepare($query);
        $stmt->execute(array($uri_segment));
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    function remove_compare($params = array()) {
        $entity_id = $params['entity_id'];
        $session_id = $params['session_id'];
        $data['status'] = '';
        if ($entity_id != '') {
            $query = "DELETE FROM entity_compare WHERE entity_id = ? AND session_id = ?";
            $stmt = $this->db->conn_id->prepare($query);
            $stmt->execute(array($entity_id, $session_id));

            $query = "SELECT count(id) as compare_count FROM entity_compare WHERE session_id = ?";
            $stmt = $this->db->conn_id->prepare($query);
            $stmt->execute(array($session_id));
            $items = $stmt->fetch(PDO::FETCH_ASSOC);
            $count = $items['compare_count'];

            $data['status'] = 'success';
            $data['count'] = $count;
        }
        return $data;
    }

    function delete_entity_photo($params) {
        $photo_id = $params['photo_id'];
        $unit_id = $params['unit_id'];
        $query = "DELETE FROM entity_attatchments WHERE id = ?";
        $stmt = $this->db->conn_id->prepare($query);
        $stmt->execute(array($photo_id));

        // Check if THUMB is available or not
        $query = "SELECT count(*) as thumb_count FROM entity_attatchments WHERE unit_id = ? AND type = 0";
        $stmt = $this->db->conn_id->prepare($query);
        $stmt->execute(array($unit_id));
        $thumb_count = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($thumb_count['thumb_count'] <= 0) {
            // Pick new value and update it THUMB
            $query = "SELECT * FROM entity_attatchments WHERE unit_id = ? ORDER BY id LIMIT 1";
            $stmt = $this->db->conn_id->prepare($query);
            $stmt->execute(array($unit_id));
            $ea_data = $stmt->fetch(PDO::FETCH_ASSOC);
            $ea_id = $ea_data['id'];

            $query = "UPDATE entity_attatchments SET type = 0 WHERE id = ? AND unit_id = ? ";
            $stmt = $this->db->conn_id->prepare($query);
            $stmt->execute(array($ea_id, $unit_id));
        }

        $data['photo_id'] = $photo_id;
        return $data;
    }

    function get_all_search_log() {
        $select = "SELECT * FROM search_log";
        $stmt = $this->db->conn_id->prepare($select);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    function get_all_email_log() {
        $select = "SELECT * FROM email_log";
        $stmt = $this->db->conn_id->prepare($select);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    function get_all_payment_log() {

        $select = "SELECT payments.*,user_master.fname from user_master,payments where user_master.id=payments.paid_by";
        $stmt = $this->db->conn_id->prepare($select);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    function get_all_shared_entity_log() {

        $select = "SELECT * FROM `entity_shared`";
        $stmt = $this->db->conn_id->prepare($select);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    function get_all_testomonial_log() {

        $select = "SELECT * FROM testimonials";
        $stmt = $this->db->conn_id->prepare($select);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    function partner_pending_payments($id) {
        $select = "SELECT * FROM payment_list WHERE user_id=" . $id . " AND due_amount>0";
        $stmt = $this->db->conn_id->prepare($select);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    function partner_payment_log($id) {
        $select = "SELECT * FROM payment_list WHERE user_id=" . $id;
        $stmt = $this->db->conn_id->prepare($select);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    function get_testimonials() {
        $select = "SELECT * FROM testimonials WHERE status = 'ACTIVE' ORDER BY RAND()";
        $stmt = $this->db->conn_id->prepare($select);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    function get_services($trade_type) {
        $select = "SELECT * FROM trade_list WHERE trade_type = ? AND status = '1' ORDER BY RAND()";
        $stmt = $this->db->conn_id->prepare($select);
        $stmt->execute(array($trade_type));
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    function get_service_details($permalink) {
        $select = "SELECT * FROM trade_list WHERE permalink = ? AND status = '1'";
        $stmt = $this->db->conn_id->prepare($select);
        $stmt->execute(array($permalink));
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    function get_city_property_count() {
        $select = "SELECT city_id, city_name, state_iso, country_name, COUNT(*) AS num_of_properties FROM unit_master_list GROUP BY city_id ";
        $stmt = $this->db->conn_id->prepare($select);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

}
