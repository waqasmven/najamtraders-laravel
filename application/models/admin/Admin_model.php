<?php

class Admin_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    function login($params = array()) {
        $email = $params['username'];
        $pass = md5($params['pass']);
        $user_login_query = "SELECT * FROM `user_view`"
                . " WHERE email = ? AND password = ? AND active = 1";
        $user_login_stmt = $this->db->conn_id->prepare($user_login_query);
        $user_login_stmt->execute(array($email, $pass));
        $user_data = $user_login_stmt->fetch(PDO::FETCH_ASSOC);
        $num_row = $user_login_stmt->rowCount();
        $user_data['num_row'] = $num_row;
        return $user_data;
    }

    function mainPageData() {
        $main_page_data_query = "SELECT * FROM `index_text` WHERE id = 1 ";
        $main_page_data_query_stmt = $this->db->conn_id->prepare($main_page_data_query);
        $main_page_data_query_stmt->execute();
        $main_page_data = $main_page_data_query_stmt->fetch(PDO::FETCH_ASSOC);
        return $main_page_data;
    }

    function get_all_slide_show_data() {
        $slide_pics = "SELECT * FROM `main_slide` ORDER BY `pic_title` ASC ";
        $slide_pics_stmt = $this->db->conn_id->prepare($slide_pics);
        $slide_pics_stmt->execute();
        $slide_data = $slide_pics_stmt->fetchAll(PDO::FETCH_ASSOC);
        return $slide_data;
    }

    function get_all_albums_data() {
        $albums_query = "SELECT * FROM `albums` ORDER BY `id` ASC ";
        $albums_query_stmt = $this->db->conn_id->prepare($albums_query);
        $albums_query_stmt->execute();
        $albums_data = $albums_query_stmt->fetchAll(PDO::FETCH_ASSOC);
        return $albums_data;
    }

    function get_all_gallery_data() {
        $gallery_query = "SELECT * FROM `gallery` ORDER BY `id` ASC ";
        $gallery_query_stmt = $this->db->conn_id->prepare($gallery_query);
        $gallery_query_stmt->execute();
        $gallery_data = $gallery_query_stmt->fetchAll(PDO::FETCH_ASSOC);
        return $gallery_data;
    }

    function get_gallery_data_by_album_id($id) {
        $gallery_query = "SELECT * FROM `gallery` WHERE album_id = ?";
        $gallery_query_stmt = $this->db->conn_id->prepare($gallery_query);
        $gallery_query_stmt->execute(array($id));
        $gallery_data = $gallery_query_stmt->fetchAll(PDO::FETCH_ASSOC);
        return $gallery_data;
    }

    function get_slide_show_pic_detail($pic_id) {
        $slide_pics = "SELECT * FROM `main_slide` WHERE id = ?";
        $slide_pics_stmt = $this->db->conn_id->prepare($slide_pics);
        $slide_pics_stmt->execute(array($pic_id));
        $slide_data = $slide_pics_stmt->fetch(PDO::FETCH_ASSOC);
        return $slide_data;
    }

    function get_gallery_pic_detail($pic_id) {
        $gallery_pic_data_query = "SELECT * FROM `gallery` WHERE id = ?";
        $gallery_pic_stmt = $this->db->conn_id->prepare($gallery_pic_data_query);
        $gallery_pic_stmt->execute(array($pic_id));
        $gallery_data = $gallery_pic_stmt->fetch(PDO::FETCH_ASSOC);
        return $gallery_data;
    }

    function update_main_page_text_data($params = array()) {
        $user_last_login_update_query = "UPDATE `index_text`"
                . " SET `h1` = '" . $params['h1'] . "', `h1_detail` = '" . $params['h1_detail'] . "', `h2` = '" . $params['h2'] . "',"
                . " `h2_detail` = '" . $params['h2_detail'] . "', `h3` = '" . $params['h3'] . "',"
                . " `h3_detail` = '" . $params['h3_detail'] . "', `h4` = '" . $params['h4'] . "'"
                . " WHERE `index_text`.`id` = 1;";
        $user_last_login_update_stmt = $this->db->conn_id->prepare($user_last_login_update_query);
        $user_last_login_update_stmt->execute();
        return 1;
    }

    function enable_disable_user($user_id, $staus) {
        $user_last_login_update_query = "UPDATE `users` SET `active` = ? WHERE `users`.`id` = ?";
        $user_last_login_update_stmt = $this->db->conn_id->prepare($user_last_login_update_query);
        $user_last_login_update_stmt->execute(array($staus, $user_id));
        return 1;
    }

    function update_slide_show_pic($params = array()) {
        $pic_data = $this->get_slide_show_pic_detail($params['id']);
        //print_r($pic_data);die;
        if ($params['pic_title'] != '' && $params['pic_path'] != '') {
            $slide_pic_update_query = "UPDATE `main_slide` SET `pic_title` = ?, `pic_path` = ? WHERE `id` = ?";
            $data = array($params['pic_title'], $params['pic_path'], $params['id']);
            unlink(UPLOADIMAGESPATH . $pic_data['pic_path']);
        } else if ($params['pic_path'] == '') {
            $slide_pic_update_query = "UPDATE `main_slide` SET `pic_title` = ? WHERE `id` = ?";
            $data = array($params['pic_title'], $params['id']);
        }
        $slide_pic_update_stmt = $this->db->conn_id->prepare($slide_pic_update_query);
        $slide_pic_update_stmt->execute($data);
        return $params['id'];
    }

    function update_gallery_pic($params = array()) {
        $pic_data = $this->get_gallery_pic_detail($params['id']);
        //print_r($pic_data);die;
        $data = '';
        if ($params['pic_title'] != '' && $params['pic_path'] != '') {
            $gallery_update_query = "UPDATE `gallery` SET `pic_title` = ?, `pic_path` = ? WHERE `id` = ?";
            $data = array($params['pic_title'], $params['pic_path'], $params['id']);
            unlink($pic_data['pic_path']);
        } else if ($params['pic_path'] == '') {
            $gallery_update_query = "UPDATE `gallery` SET `pic_title` = ? WHERE `id` = ?";
            $data = array($params['pic_title'], $params['id']);
        }
        $gallery_update_stmt = $this->db->conn_id->prepare($gallery_update_query);
        $gallery_update_stmt->execute($data);
        //echo var_export($gallery_update_stmt->errorInfo());

        return $params['id'];
    }

    function edit_user($params = array()) {
        $user_data = $this->get_all_from_table_by_id('users', $params['id']);
        if (isset($params['new_pic_name'])) {
            unlink($params['new_pic_name']);
        } else {
            $params['new_pic_name'] = $params['p_pic'];
        }
        $user_update_query = "UPDATE `users` SET `Full_Name` = ?, `password` = ?, `address` = ?, `phone` = ?,"
                . " `email` = ?, `p_pic` = ? WHERE `users`.`id` = ?";
        $user_update_stmt = $this->db->conn_id->prepare($user_update_query);
        $data = array($params['full_name'], md5($params['password']), $params['address'], $params['phone'], $params['email'], $params['new_pic_name'], $params['id']);

        $user_update_stmt->execute($data);
        //echo var_export($user_update_stmt->errorInfo());        exit();

        return $params['id'];
    }

    function add_new_slide_pic($params = array()) {
        $insert_main_slide_query = "INSERT INTO `main_slide` (`pic_title`, `pic_path`) VALUES (?, ?)";
        $insert_main_slide__stmt = $this->db->conn_id->prepare($insert_main_slide_query);
        $insert_main_slide__stmt->execute(array($params['pic_title'], $params['new_pic_name']));
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }

    function add_new_user($params = array()) {
        //print_r($params);die;
        $username = $params['email'];
        $pass = md5($params['password']);
        $fullname = $params['full_name'];
        $address = $params['address'];
        $about = $params['address'];
        $phone = $params['phone'];
        $designation = $params['user_type'];
        $pPic = $params['new_pic_name'];
        $add_user_query = "INSERT INTO `users` (`username`, `password`, `Full_Name`, `about`, `address`, `phone`, `email`, `user_type`, `p_pic`, `active`)"
                . " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, 1);";
        $add_user_stmt = $this->db->conn_id->prepare($add_user_query);
        $add_user_stmt->execute(array($username, $pass, $fullname, $about, $address, $phone, $username, $designation, $pPic));
        $insert_id = $this->db->insert_id();
        
        //echo var_export($add_user_stmt->errorInfo()); exit();
        return $insert_id;
    }

    function delete_slide_show_pic($id) {
        $pic_data = $this->get_slide_show_pic_detail($id);
        $delete_query = "DELETE FROM `main_slide` WHERE `main_slide`.`id` = ?";
        $delete_query_stmt = $this->db->conn_id->prepare($delete_query);
        $delete_query_stmt->execute(array($id));
        unlink(UPLOADIMAGESPATH . $pic_data['pic_path']);
        return $id;
    }

    function delete_album_by_id($id) {
        $this->delete_gallery_images_by_album_id($id);
        $delete_query = "DELETE FROM `albums` WHERE `id` = ?";
        $delete_query_stmt = $this->db->conn_id->prepare($delete_query);
        $delete_query_stmt->execute(array($id));
        // echo var_export($delete_query_stmt->errorInfo());
        return $id;
    }

    function delete_gallery_photo_by_id($id) {
        $gallery_data = $this->get_gallery_pic_detail($id);
        unlink(GALLERYIMAGESPATH . $gallery_data['pic_title']);
        $delete_query = "DELETE FROM `gallery` WHERE `id` = ?";
        $delete_query_stmt = $this->db->conn_id->prepare($delete_query);
        $delete_query_stmt->execute(array($id));
        // echo var_export($delete_query_stmt->errorInfo());
        return $id;
    }

    function delete_gallery_images_by_album_id($id) {

        $delete_query = "DELETE FROM `gallery` WHERE album_id = ?";
        $gallery_data = $this->get_gallery_data_by_album_id($id);
        foreach ($gallery_data as $gall_row) {
            unlink(GALLERYIMAGESPATH . $gall_row['pic_title']);
        }
        $delete_query_stmt = $this->db->conn_id->prepare($delete_query);
        $delete_query_stmt->execute(array($id));
        //echo var_export($delete_query_stmt->errorInfo());
        return $id;
    }

    function change_reset_password($params = array()) {
        $user_id = $params['user_id'];
        $is_reset = isset($params['is_reset']) ? $params['is_reset'] : '';
        $old_pass = md5($params['old_pass']);
        $new_pass = md5($params['new_pass']);
        if ($is_reset == '') {
            $stmt = $this->db->conn_id->prepare("SELECT email FROM user_master WHERE id=? AND mpwd=?");
            $stmt->execute(array($user_id, $old_pass));
            //echo var_export($stmt->errorInfo());
            $num_row = $stmt->rowCount();
            if ($num_row > 0) {
                $stmt = $this->db->conn_id->prepare("UPDATE user_master SET mpwd = ? WHERE id = ?");
                $stmt->execute(array($new_pass, $user_id));
                $data['status'] = 'success';
                $data['msg'] = 'Your password changed successfully';
            } else {
                $data['status'] = 'error';
                $data['msg'] = 'Wrong old password';
            }
        }
        return $data;
    }

    function user_change_photo($params = array()) {
        $user_id = isset($params['user_id']) ? $this->clean($params['user_id']) : '';
        $photo = isset($params['photo']) ? $params['photo'] : '';
        if ($user_id != '') {
            $stmt = $this->db->conn_id->prepare("UPDATE user SET photo = ? WHERE id = ?");
            $stmt->execute(array($photo, $user_id));
            //echo var_export($stmt->errorInfo());
            $num_row = $stmt->rowCount();
            $data['num_row'] = $num_row;
        } else {
            $data['num_row'] = '';
        }
        return $data;
    }

    function check_promo_code($promo_code) {
        $today_date = date('Y-m-d');
        $stmt = $this->db->conn_id->prepare("SELECT * FROM promo WHERE promo_code = ?");
        $stmt->execute(array($promo_code));
        $data['promo_data'] = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($data && $data['promo_data']['end_date'] > $today_date) {
            $data['promo_msg'] = 'Promo code is valid';
            return $data;
        } else {
            $data['promo_msg'] = 'Promo Code is invalid';
            return $data;
        }
        //echo var_export($stmt->errorInfo());die;
    }

    //Pagination
    public function get_count($table_name) {
        return $this->db->count_all($table_name);
    }

    public function get_table_data_for_pagination($table_name, $limit, $start) {
        $this->db->limit($limit, $start);
        $query = $this->db->get($table_name);        
        return $query->result();
    }

    public function get_table_data_for_pagination_by_where($table_name, $limit, $start, $active) {
        $this->db->limit($limit, $start);
        $query = $this->db->query("Select * from $table_name where active = $active");        
        return $query->result();
    }

    public function get_all_from_table($table_name) {
        $query = $this->db->get($table_name);
        return $query->result();
    }

    public function get_all_from_table_by_id($table_name, $id) {
        $query = $this->db->get($table_name);
        $where = "u_id = " . $id;
        $this->db->where($where);
        return $query->row();
    }

}
