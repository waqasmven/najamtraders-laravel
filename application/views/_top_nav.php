<div class="w3_agileits_banner_main_grid">
    <div class="w3_agile_logo">
        <h1><a href="<?= base_url()?>"><span>Qazi</span>Agriculture<i>Farms</i></a></h1>
    </div>
    <div class="agile_social_icons_banner">
        <ul class="agileits_social_list">
            <li><a target="_blank" href="https://www.facebook.com/QaziAgriFarms/" class="w3_agile_facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
            <!--<li><a href="#" class="agile_twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
            <li><a href="#" class="w3_agile_dribble"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
            <li><a href="#" class="w3_agile_vimeo"><i class="fa fa-vimeo" aria-hidden="true"></i></a></li> -->
        </ul>
    </div>
    <div class="agileits_w3layouts_menu">
        <div class="shy-menu">
            <a class="shy-menu-hamburger">
                <span class="layer top"></span>
                <span class="layer mid"></span>
                <span class="layer btm"></span>
            </a>
            <div class="shy-menu-panel">
                <nav class="menu menu--horatio link-effect-8" id="link-effect-8">
                    <ul class="w3layouts_menu__list">
                        <li id="home"><a href="<?= base_url()?>">Home</a></li>
                        <li id="about"><a href="<?= base_url('About-us')?>">About Us</a></li>
                        <li id="service"><a href="<?= base_url('Services')?>">Services</a></li>
                        <li id="gallery"><a href="<?= base_url('Gallery')?>">Gallery</a></li>
                        <li id="contact"><a href="<?= base_url('Contact-us')?>">Contact Us</a></li>
                    </ul>
                </nav>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
    <div class="clearfix"> </div>
</div>