<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('admin/_header'); ?>

    <body class="hold-transition sidebar-mini">

        <div class="wrapper">
            <!-- Main Sidebar Container -->
            <?php $this->load->view('admin/_side_bar'); ?>
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <?php $this->load->view('admin/_bread_crumbs'); ?>
                <!-- /.content-header -->
                <!-- Main content -->
                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card">
                                    <div class="card-header border-0">
                                        <div class="d-flex justify-content-between">
                                            <h3 class="card-title">Add Gallery</h3>
                                            <!--<a href="javascript:void(0);">View Report</a>-->
                                        </div>
                                    </div>

                                    <div class="card">
                                        <div class="card-body table-responsive p-0">
                                            <table class="table table-striped table-valign-middle">
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <div class='content'>
                                                                <!-- Dropzone -->
                                                                <div class='content'>
                                                                    <!-- Dropzone -->
                                                                    <form action="" class="dropzone" id="fileupload">
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="row">
                                            <div class="col-8">
                                            </div>

                                            <!-- /.col -->
                                        </div>
                                        <div id="msg" class="h-75""></div>

                                    </div>
                                    <!-- /.card -->

                                    <!-- /End Main Page form -->
                                </div>
                                <!-- /.col-md-6 -->
                                <!-- /.col-md-6 -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.container-fluid -->
                    </div>
                    <!-- /.content -->
                </div>
                <!-- /.content-wrapper -->
            </div>
        </div>
        <?php $this->load->view('admin/_footer') ?>
        <link href='https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/dropzone.css' type='text/css' rel='stylesheet'>
        <script src='https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/dropzone.js' type='text/javascript'></script>
        <script type="text/javascript">

            Dropzone.autoDiscover = false;

            var foto_upload = new Dropzone(".dropzone", {
                url: "<?= base_url('admin/Drop_zone/process_upload') ?>",
                maxFilesize: 2,
                method: "post",
                acceptedFiles: "image/*",
                paramName: "userfile",
                dictInvalidFileType: "Type file not allowed",
                addRemoveLinks: true,
            });


            //Event ketika Memulai mengupload
            foto_upload.on("sending", function (a, b, c) {
                a.token = Math.random();
                c.append("token_foto", a.token); //Menmpersiapkan token untuk masing masing foto
                //c.append("album_name", document.getElementById('album_name').value); // If you want to send a parameter
            });


            //Event ketika foto dihapus
            foto_upload.on("removedfile", function (a) {
                var token = a.token;
                $.ajax({
                    type: "post",
                    data: {token: token},
                    url: "<?php echo base_url('admin/Drop_zone/remove_foto') ?>",
                    cache: false,
                    dataType: 'json',
                    success: function () {
                        console.log("remove poto success");
                    },
                    error: function () {
                        console.log("Error");

                    }
                });
            });


        </script>
        <script>
            $(document).ready(function () {
                $("#gallery").addClass(" active");
                $("#add_gallery").addClass(" active");
            });
        </script>


    </body>
</html>
