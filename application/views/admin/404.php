<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('admin/_header'); ?>
    <body class="hold-transition sidebar-mini">
        <div class="wrapper">
            <!-- /.navbar -->
            <!-- Main Sidebar Container -->

                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-sm-6">
                                <h1>404 Error Page</h1>
                            </div>
                            <div class="col-sm-6">
                                <ol class="breadcrumb float-sm-right">
                                    <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
                                    <li class="breadcrumb-item active">404 Error Page</li>
                                </ol>
                            </div>
                        </div>
                    </div><!-- /.container-fluid -->
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="error-page">
                        <h2 class="headline text-warning"> 404</h2>

                        <div class="error-content">
                            <h3><i class="fas fa-exclamation-triangle text-warning"></i> Oops! Page not found.</h3>

                            <p>
                                We could not find the page you were looking for.
                                Meanwhile, you may <a href="<?php echo base_url(); ?>">return to Home</a>
                            </p>

                        </div>
                        <!-- /.error-content -->
                    </div>
                    <!-- /.error-page -->
                </section>
                <!-- /.content -->
 
        </div>
        <script>
         setTimeout(function(){
            window.location.href = '<?=base_url()?>';
         }, 5000);
      </script>
    </body>
</html>
