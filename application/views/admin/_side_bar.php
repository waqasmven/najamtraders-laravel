<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->   
    <a href="<?= base_url('logout')?>" class="brand-link">
        <img src="<?php echo base_url(SITETHEME.'images/'); ?>logout.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light">Logout</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <a href="<?php echo base_url($this->session->userdata('P_pic')); ?>"><img src="<?php echo base_url($this->session->userdata('P_pic')); ?>" class="img-circle elevation-2" alt="User Image"></a>
            </div>
            <div class="info">
                <a href=" <?= 'http://fb.me/hwr.waqas' ?>" class="d-block"><?php echo get_session_user_name(); ?></a>
            </div>
        </div>
        <!-- SidebarSearch Form -->
        <div class="form-inline">
            <div class="input-group" data-widget="sidebar-search">
                <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
                <div class="input-group-append">
                    <button class="btn btn-sidebar">
                        <i class="fas fa-search fa-fw"></i>
                    </button>
                </div>
            </div>
        </div>
        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->
                <li class="nav-item">
                    <a id="Dashboard" href="<?= base_url('admin') ?>" class="nav-link">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                            Dashboard
                            <i class="right fas fa-angle"></i>
                        </p>
                    </a>                    
                </li>
                <li class="nav-item menu-open">
                    <a id="slideshow" href="" class="nav-link">
                        <i class="nav-icon fas fa-remove"></i>
                        <p>
                            Slideshow
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a id='view_ss' href="<?= base_url('dashboard/view/slide-show/') ?>" class="nav-link">
                                <i class="nav-icon fas fa-eye"></i>
                                <p>View SlideShow</p>
                            </a>
                        </li>                        
                    </ul>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a id="insert_ss" href="<?= base_url('dashboard/add/new/slide-show-pic') ?>" class="nav-link">
                                <i class="nav-icon fas fa-images fa-plus"></i>
                                <p>Add Slideshow Pic</p>
                            </a>
                        </li>                        
                    </ul>
                </li>
                <li class="nav-item menu-open">
                    <a id="gallery" href="" class="nav-link">
                        <i class="nav-icon fas fa-images"></i>
                        <p>
                            Gallery
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a id='view_gallery' href="<?= base_url('dashboard/view/gallery/') ?>" class="nav-link">
                                <i class="nav-icon fas fa-eye"></i>
                                <p>View Gallery</p>
                            </a>
                        </li>                        
                    </ul>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a id="add_gallery" href="<?= base_url('dashboard/add/new/gallery') ?>" class="nav-link">
                                <i class="nav-icon fas fa-images fa-plus"></i>
                                <p>Add New Gallery</p>
                            </a>
                        </li>                        
                    </ul>
                </li>   
                
                <li class="nav-item menu-open">
                    <a id="user_manage" href="" class="nav-link">
                        <i class="nav-icon fas fa-users"></i>
                        <p>
                            User Management
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a id='add_user' href="<?= base_url('dashboard/add-new-user') ?>" class="nav-link">
                                <i class="nav-icon fas fa-eye"></i>
                                <p>Add User</p>
                            </a>
                        </li>                        
                    </ul>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a id="view_all_user" href="<?= base_url('dashboard/users-list') ?>" class="nav-link">
                                <i class="nav-icon fas fa-images fa-users"></i>
                                <p>Users List</p>
                            </a>
                        </li>                        
                    </ul>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a id="disabled_user" href="<?= base_url('dashboard/disabled-user-list') ?>" class="nav-link">
                                <i class="nav-icon fas fa-images fa-user-alt-slash"></i>
                                <p>Disabled Users</p>
                            </a>
                        </li>                        
                    </ul>
                </li>   
                
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>