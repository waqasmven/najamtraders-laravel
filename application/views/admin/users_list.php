<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('admin/_header') ?>
    <body class="hold-transition sidebar-mini">
        <!-- Site wrapper -->
        <div class="wrapper">
            <!-- Main Sidebar Container -->
            <?php $this->load->view('admin/_side_bar'); ?>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <?php $this->load->view('admin/_bread_crumbs'); ?>

                <!-- Main content -->
                <section class="content">

                    <!-- Default box -->
                    <div class="card card-solid">
                        <div class="card-body pb-0">
                            <?php if(!empty($users_data)){ ?>
                            <div class="row d-flex align-items-stretch">
                                <?php foreach ($users_data as $user_row) { ?>
                                    <div class="col-12 col-sm-6 col-md-4 d-flex align-items-stretch">
                                        <div class="card bg-light">

                                            <div class="card-header text-muted border-bottom-0">
                                                <?= $user_row->type ?>
                                            </div>
                                            <div class="card-body pt-0">
                                                <div class="row">
                                                    <div class="col-7">
                                                        <h2 class="lead"><b><?= $user_row->Full_Name ?></b></h2>
                                                        <p class="text-muted text-sm"><b>About: </b> <?= $user_row->about ?></p>
                                                        <ul class="ml-4 mb-0 fa-ul text-muted">
                                                            <li class="small"><span class="fa-li"><i class="fas fa-lg fa-building"></i></span> <?= $user_row->address ?></li>
                                                            <li class="small"><span class="fa-li"><i class="fas fa-lg fa-phone"></i></span><a href="tel:<?= $user_row->phone ?>"> <?= $user_row->phone ?></a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="col-5 text-center">
                                                        <img src="<?= base_url() . $user_row->p_pic ?>" alt="user-avatar" class="img-circle img-fluid">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="card-footer">
                                                <div class="text-right">
                                                    <?php if ($menu_id != 'disabled_user') { ?>
                                                        <a href="<?= base_url('dashboard/edit-user/' . $user_row->u_id) ?>" class="btn btn-sm bg-teal">
                                                            <i class="fas fa-edit"></i>
                                                        </a>
                                                        <a href="javascript:void(0)" class="btn btn-sm btn-primary" onclick="disable_enable_user('<?= $user_row->u_id ?>', '<?= $menu_id ?>')">
                                                            <i class="fas fa-trash-alt" ></i>
                                                        </a>
                                                    <?php } else { ?>
                                                        <a href="javascript:void(0)" class="btn btn-sm btn-primary" onclick="disable_enable_user('<?= $user_row->u_id ?>', '<?= $menu_id ?>')">
                                                            <i class="fas fa-recycle" ></i>
                                                        </a>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            <?php }
                                ?> 
                                
                            </div>
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            <nav aria-label="Contacts Page Navigation">
                                <?php echo $links; ?>
                            </nav>
                        </div>
                        <!-- /.card-footer -->
                            <?php } else{?><div ><?=NORECORD?></div> <?php } ?>
                    </div>
                    <!-- /.card -->

                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->

            <?php $this->load->view('admin/_footer') ?>            
            <script>
                $(document).ready(function () {
                    $("#user_manage").addClass(" active");
                    $("#<?= $menu_id ?>").addClass(" active");
                });
            </script>
            <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
            <script>
                function disable_enable_user(user_id, menu_id) {
                    var _status = 0;
                    var answer = '';
                    var s_msg = '';
                    if (menu_id == 'disabled_user') {
                        _status = 1;
                        answer = "Are you sure you want to enable this user? You can always disable a user from admin menu";
                        s_msg = 'Congratulation, User is Enabled.';
                    } else {
                        answer = "User will be disabled, not deleted. Administrator will be required to enable this user to use service. You can always activate users from admin menu";
                        s_msg = 'User is now Disabled.';
                    }
                    Swal.fire({
                        title: answer,
                        showDenyButton: true,
                        showCancelButton: true,
                        confirmButtonText: `Confirm`,
                        denyButtonText: `Cancel`,
                        customClass: {
                        confirmButton: 'btn btn-danger',
                    },
                    }).then((result) => {
                        if (result.value === true) {
                            $.ajax({
                                type: "POST",
                                data: {id: user_id, status: _status},
                                url: '<?= base_url('admin/Admin_con/enable_disable_user/') ?>',
                                success: function (result)
                                {
                                    Swal.fire(s_msg, '', 'success');
                                    setTimeout(function () {
                                        location.reload();
                                    }, 1000);
                                }
                            });
                        } else {
                            Swal.fire('Changes are not saved', '', 'info')
                        }

                    })

                }
            </script>
    </body>
</html>
