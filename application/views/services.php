
<html lang="en">
<head>
<title>QAZI AGRI FARMS</title>
<?php $this->load->view('_header') ?>
</head>
	
<body>
<!-- banner -->
	<div class="banner1">
		<div class="container">
			<?php $this->load->view('_top_nav'); ?>
		</div>
	</div>
<!-- banner -->
<!-- bootstrap-pop-up -->
	<div class="modal video-modal fade" id="fertilizingModal" tabindex="-1" role="dialog" aria-labelledby="fertilizingModal">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					Fertilizing
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>						
				</div>
				<section>
					<div class="modal-body">
						<img src="<?= base_url(SITETHEME)?>images/4.jpg" alt=" " class="img-responsive" />
						<p>
							<i>This maintains the soil fertility, so the farmer can continue to grow nutritious crops and healthy crops. Farmers turn to fertilizers because these substances contain plant nutrients such as nitrogen, phosphorus, and potassium</i></p>
					</div>
				</section>
			</div>
		</div>
	</div>
<!-- //bootstrap-pop-up -->
<!-- bootstrap-pop-up -->
	<div class="modal video-modal fade" id="tunnelFarmingModal" tabindex="-1" role="dialog" aria-labelledby="tunnelFarmingModal">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					Tunnel Farming
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>						
				</div>
				<section>
					<div class="modal-body">
						<img src="<?= base_url(SITETHEME)?>images/4.jpg" alt=" " class="img-responsive" />
						<p>
							<i>Advantages of hydroponic tunnel farming. High quality product. No soil is needed. Plants get a constant supply of nutrients through an automatic irrigation system. Able to produce larger yields of vegetables on a small area of land.</i></p>
					</div>
				</section>
			</div>
		</div>
	</div>
<!-- //bootstrap-pop-up -->
<!-- bootstrap-pop-up -->
	<div class="modal video-modal fade" id="organicModal" tabindex="-1" role="dialog" aria-labelledby="organicModal">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					Organic Produce
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>						
				</div>
				<section>
					<div class="modal-body">
						<img src="<?= base_url(SITETHEME)?>images/4.jpg" alt=" " class="img-responsive" />
						<p>
							<i>Organic food products are grown under a system of agriculture without the use of harmful chemical fertilizers and pesticides with an environmentally and socially responsible approach.</i></p>
					</div>
				</section>
			</div>
		</div>
	</div>
<!-- //bootstrap-pop-up -->
<!-- breadcrumbs -->
	<div class="breadcrumbs">
		<div class="container">
			<div class="w3layouts_breadcrumbs_left">
				<ul>
                                    <li><i class="fa fa-home" aria-hidden="true"></i><a href="<?= base_url()?>">Home</a><span>/</span></li>
					<li><i class="fa fa-cogs" aria-hidden="true"></i>Services</li>
				</ul>
			</div>
			<div class="w3layouts_breadcrumbs_right">
				<h2>Services</h2>
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
<!-- //breadcrumbs -->
<!-- services -->
	<div class="welcome">
		<div class="container">
			<h3 class="agileits_w3layouts_head">Our <span>Featured</span> Services</h3>
			<div class="w3_agile_image">
				<img src="<?= base_url(SITETHEME)?>images/1.png" alt=" " class="img-responsive" />
			</div>
			
			<div class="w3_agile_services_grids"> 
				<div class="col-md-6 w3_agile_services_grid"> 
					<div class="col-xs-4 w3_agile_services_grid_left"> 
						<div class="agile_services_grid_left1 hvr-radial-out"> 
							<img src="<?= base_url(SITETHEME)?>images/13.png" alt=" " class="img-responsive" />
						</div>
					</div>
					<div class="col-xs-8 w3_agile_services_grid_right"> 
						<h4>Fertilizer</h4>
						<p>Any material of natural or synthetic origin that is applied to crops to supply  nutrients essential for growth .</p>
					</div>
					<div class="clearfix"> </div>
				</div>
				<div class="col-md-6 w3_agile_services_grid"> 
					<div class="col-xs-4 w3_agile_services_grid_left"> 
						<div class="agile_services_grid_left1 hvr-radial-out"> 
							<img src="<?= base_url(SITETHEME)?>images/14.png" alt=" " class="img-responsive" />
						</div>
					</div>
					<div class="col-xs-8 w3_agile_services_grid_right"> 
						<h4>Agronomic Crops</h4>
						<p>Occupy large acreage, and are the bases of the world's food and fiber production systems, often mechanized.</p>
					</div>
					<div class="clearfix"> </div>
				</div>
				<div class="col-md-6 w3_agile_services_grid"> 
					<div class="col-xs-4 w3_agile_services_grid_left"> 
						<div class="agile_services_grid_left1 hvr-radial-out"> 
							<img src="<?= base_url(SITETHEME)?>images/15.png" alt=" " class="img-responsive" />
						</div>
					</div>
					<div class="col-xs-8 w3_agile_services_grid_right"> 
						<h4>Fresh Quality Produce</h4>
						<p>We sell high quality, fresh produce as well as a fine selection of fruit and vegetable products.</p>
					</div>
					<div class="clearfix"> </div>
				</div>
				<div class="col-md-6 w3_agile_services_grid"> 
					<div class="col-xs-4 w3_agile_services_grid_left"> 
						<div class="agile_services_grid_left1 hvr-radial-out"> 
							<img src="<?= base_url(SITETHEME)?>images/16.png" alt=" " class="img-responsive" />
						</div>
					</div>
					<div class="col-xs-8 w3_agile_services_grid_right"> 
						<h4>Tunnel farming</h4>
						<p>Off-season vegetable cultivation is recommended with the use of high tunnels of bamboo structure on the basis of its low construction cost.</p>
					</div>
					<div class="clearfix"> </div>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
<!-- //services -->
<!-- services-bottom -->
	<div class="services-bottom">
		<div class="container">
			<h3 class="agileits_w3layouts_head agileinfo_head w3_head"><span>What</span> we do</h3>
			<div class="w3_agile_image">
				<img src="<?= base_url(SITETHEME)?>images/17.png" alt=" " class="img-responsive">
			</div>
			
			<div class="w3ls_news_grids">
				<div class="col-md-4 w3_agileits_services_bottom_grid">
					<div class="wthree_services_bottom_grid1">
						<img src="<?= base_url(SITETHEME)?>images/5.jpg" alt=" " class="img-responsive" />
						<div class="wthree_services_bottom_grid1_pos">
							<h4>Fertilizing</h4>
						</div>
					</div>
					<div class="agileinfo_services_bottom_grid2">
						<p>Fertilizers enhance the natural fertility of the soil or replace the chemical elements taken from the soil by previous crops.</p>
						<div class="agileits_w3layouts_learn_more hvr-radial-out">
							<a href="#" data-toggle="modal" data-target="#fertilizingModal">Read More</a>
						</div>
					</div>
				</div>
				<div class="col-md-4 w3_agileits_services_bottom_grid">
					<div class="wthree_services_bottom_grid1">
						<img src="<?= base_url(SITETHEME)?>images/6.jpg" alt=" " class="img-responsive" />
						<div class="wthree_services_bottom_grid1_pos">
							<h4>Tunnel Farming</h4>
						</div>
					</div>
					<div class="agileinfo_services_bottom_grid2">
						<p>Off-season vegetable cultivation is recommended with the use of high tunnels of bamboo structure on the basis of its low construction cost.</p>
						<div class="agileits_w3layouts_learn_more hvr-radial-out">
							<a href="#" data-toggle="modal" data-target="#tunnelFarmingModal">Read More</a>
						</div>
					</div>
				</div>
				<div class="col-md-4 w3_agileits_services_bottom_grid">
					<div class="wthree_services_bottom_grid1">
						<img src="<?= base_url(SITETHEME)?>images/3.jpg" alt=" " class="img-responsive" />
						<div class="wthree_services_bottom_grid1_pos">
							<h4>Organic Produce</h4>
						</div>
					</div>
					<div class="agileinfo_services_bottom_grid2">
						<p>Ingredients are grown without the use of pesticides, synthetic fertilizers, sewage sludge, genetically modified organisms</p>
						<div class="agileits_w3layouts_learn_more hvr-radial-out">
							<a href="#" data-toggle="modal" data-target="#organicModal">Read More</a>
						</div>
					</div>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
<!-- //services-bottom -->

<!-- flexSlider -->
	<script defer src="<?= base_url(SITETHEME)?>js/jquery.flexslider.js"></script>
	<script type="text/javascript">
		$(window).load(function(){
		  $('.flexslider').flexslider({
			animation: "slide",
			start: function(slider){
			  $('body').removeClass('loading');
			}
		  });
		});
  </script>
<!-- //flexSlider -->
<!-- footer -->
	<?php $this->load->view('_footer') ?>
<!-- //footer -->
<!-- menu -->
	<script>
		$(function() {
			
			initDropDowns($("div.shy-menu"));

		});

		function initDropDowns(allMenus) {

			allMenus.children(".shy-menu-hamburger").on("click", function() {
				
				var thisTrigger = jQuery(this),
					thisMenu = thisTrigger.parent(),
					thisPanel = thisTrigger.next();

				if (thisMenu.hasClass("is-open")) {

					thisMenu.removeClass("is-open");

				} else {			
					
					allMenus.removeClass("is-open");	
					thisMenu.addClass("is-open");
					thisPanel.on("click", function(e) {
						e.stopPropagation();
					});
				}
				
				return false;
			});
		}
	</script>
<!-- //menu -->
<!-- start-smoth-scrolling -->
<script type="text/javascript" src="<?= base_url(SITETHEME)?>js/move-top.js"></script>
<script type="text/javascript" src="<?= base_url(SITETHEME)?>js/easing.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});
</script>
<!-- start-smoth-scrolling -->
<!-- for bootstrap working -->
	<script src="<?= base_url(SITETHEME)?>js/bootstrap.js"></script>
<!-- //for bootstrap working -->
<!-- here stars scrolling icon -->
	<script type="text/javascript">
		$(document).ready(function() {
			/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
				};
			*/
								
			$().UItoTop({ easingType: 'easeOutQuart' });
								
			});
	</script>
<!-- //here ends scrolling icon -->
<script>
            $(document).ready(function () {
                $("#service").addClass(" active");
            });
        </script>
</body>
</html>