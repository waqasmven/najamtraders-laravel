<?php
$this->load->view('frontend/dashboard/_dash_header');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="Ansonika">
        <title><?= $heading ?> - <?= SITETITLE ?></title>

        <!-- Favicons-->
        <link rel="shortcut icon" href="<?= base_url() . DASHTHEME ?>img/favicon.ico" type="image/x-icon">
        <link rel="apple-touch-icon" type="image/x-icon" href="<?= base_url() . DASHTHEME ?>img/apple-touch-icon-57x57-precomposed.png">
        <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="<?= base_url() . DASHTHEME ?>img/apple-touch-icon-72x72-precomposed.png">
        <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="<?= base_url() . DASHTHEME ?>img/apple-touch-icon-114x114-precomposed.png">
        <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="<?= base_url() . DASHTHEME ?>img/apple-touch-icon-144x144-precomposed.png">

        <!-- Bootstrap core CSS-->
        <link href="<?= base_url() . DASHTHEME ?>vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <!-- Main styles -->
        <link href="<?= base_url() . DASHTHEME ?>css/admin.css" rel="stylesheet">
        <!-- Icon fonts-->
        <link href="<?= base_url() . DASHTHEME ?>vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <!-- Plugin styles -->
        <link href="<?= base_url() . DASHTHEME ?>vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
        <!-- Your custom styles -->
        <link href="<?= base_url() . DASHTHEME ?>css/custom.css" rel="stylesheet">
    </head>

    <body class="fixed-nav sticky-footer" id="page-top">
        <?php
        $this->load->view('frontend/dashboard/_dash_nav');
        ?>
        <!-- /Navigation-->
        <div class="content-wrapper">
            <div class="container-fluid">
                <!-- Breadcrumbs-->
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="<?= base_url('dashboard') ?>">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item active">Profile</li>
                </ol>
                <div class="box_general padding_bottom">
                    <div class="header_box version_2">
                        <h2><i class="fa fa-user"></i>Profile details</h2>
                    </div>

                    <!-- /box_general-->
                    <div class="row">
                        <div class="col-md-6">
                            <form id="profile_form" name="profile_form" class="form-horizontal" method="post">
                                <input type="hidden" name="user_id" id="user_id" value="<?= __echo($user_array, 'id') ?>">
                                <div class="box_general padding_bottom">
                                    <div class="header_box version_2">
                                        <h2><i class="fa fa-envelope"></i>Profile</h2>
                                    </div>
                                    <div class="form-group">
                                        <label>First Name</label>
                                        <input class="form-control"name="fname" id="fname" value="<?= __echo($user_array, 'fname') ?>" type="text">
                                    </div>
                                    <div class="form-group">
                                        <label>Last Name</label>
                                        <input class="form-control" name="lname" id="lname" value="<?= __echo($user_array, 'lname') ?>" type="text">
                                    </div>
                                    <div class="form-group">
                                        <label>Phone</label>
                                        <input class="form-control" name="phone" id="phone" value="<?= __echo($user_array, 'phone') ?>" type="text">
                                    </div>
                                    <div class="form-group">
                                        <label>Photo / Logo</label>
                                        <input type="file" name="photo" id="photo" class="form-control"><br>
                                        <?php
                                            if($user_array['photo'] == ''){
                                        ?>
                                        <img width="150" src="<?= base_url('uploads/profile-images/default-profile.png')?>">
                                        <?php
                                            }else{
                                        ?>
                                        <img width="150" src="<?= base_url('uploads/profile-images/') . $user_array['photo'] ?>">
                                        <?php
                                            }
                                        ?>
                                    </div>
                                    <div class="status_msg"></div>
                                    <button class="btn_1 medium pull-right">Update Profile</button>
                                </div>
                            </form>
                        </div>
                        <div class="col-md-6">
                            <div class="box_general padding_bottom">
                                <form id="change_password" name="change_password" class="form-horizontal" method="post">
                                    <input type="hidden" name="user_id" id="user_id" value="<?= __echo($user_array, 'id') ?>">
                                    <div class="header_box version_2">
                                        <h2><i class="fa fa-lock"></i>Change password</h2>
                                    </div>
                                    <div class="form-group">
                                        <label>Old password</label>
                                        <input class="form-control" type="password" name="old_pass" id="old_pass" placeholder="Old Password">
                                    </div>
                                    <div class="form-group">
                                        <label>New password</label>
                                        <input class="form-control" type="password" name="new_pass" id="new_pass" placeholder="New Password">
                                    </div>
                                    <div class="form-group">
                                        <label>Confirm new password</label>
                                        <input class="form-control" type="password" name="conf_pass" id="conf_pass" placeholder="Confirm New Password">
                                    </div>
                                    <div class="status_msg1"></div>
                                    <button class="btn_1 medium pull-right">Update Password</button>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- /row-->
                </div>
            </div>
            <!-- /.container-fluid-->
        </div>

        <?php
        $this->load->view('frontend/dashboard/_dash_footer');
        ?>
        <!-- Bootstrap core JavaScript-->
        <script src="<?= base_url() . DASHTHEME ?>vendor/jquery/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
        <script src="<?= base_url() . DASHTHEME ?>vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <!-- Core plugin JavaScript-->
        <script src="<?= base_url() . DASHTHEME ?>vendor/jquery-easing/jquery.easing.min.js"></script>
        <!-- Page level plugin JavaScript-->
        <script src="<?= base_url() . DASHTHEME ?>vendor/chart.js/Chart.js"></script>
        <script src="<?= base_url() . DASHTHEME ?>vendor/datatables/jquery.dataTables.js"></script>
        <script src="<?= base_url() . DASHTHEME ?>vendor/datatables/dataTables.bootstrap4.js"></script>
        <script src="<?= base_url() . DASHTHEME ?>vendor/jquery.selectbox-0.2.js"></script>
        <script src="<?= base_url() . DASHTHEME ?>vendor/retina-replace.min.js"></script>
        <script src="<?= base_url() . DASHTHEME ?>vendor/jquery.magnific-popup.min.js"></script>
        <!-- Custom scripts for all pages-->
        <script src="<?= base_url() . DASHTHEME ?>js/admin.js"></script>
        <script type="text/javascript" src="<?= base_url() . COMMONTHEME ?>js/my_script.js"></script>
    </body>
    <script>
        $(document).ready(function () {
            $("#dash_profile").addClass(" active");
        });
    </script>
    <script>
    /*$(document).ready(function () {
     $('#dob').datepicker({
     format: 'dd-M-yyyy',
     todayHighlight: true,
     showMeridian: true,
     endDate: '+0d',
     autoclose: true
     });
     $('#dob').datepicker('setStartDate', '01-01-1990');
     });*/

    $(document).ready(function () {
        $("#profile_form").validate({
            rules: {
                fname: {required: true, minlength: 3},
                lname: {required: true, minlength: 3},
                phone: {required: true, minlength: 10},
                photo: {required: true}
            },
            highlight: function (element, errorClass) {
                $(element).css({borderColor: '#FF0000'});
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).css({borderColor: '#CCCCCC'});
            },
            errorPlacement: function (error, element) {
                $.validator.messages.required = '';
            },
            //messages: {email: "Please enter a valid email address"},
            //invalidHandler: function(form, validator) {},
            submitHandler: function (form) {
                var formData = new FormData($('#profile_form')[0]);
                $.ajax({
                    url: '<?= base_url() ?>user/profile_save',
                    type: 'POST',
                    data: formData,
                    async: false,
                    processData: false, // tell jQuery not to process the data
                    contentType: false, // tell jQuery not to set contentType
                    success: function (data) {
                        try {
                            var response = jQuery.parseJSON(data);
                            if (response.status == 'success') {
                                $('.status_msg').html(getSuccessHTML(response.msg));
                                windowRedirect(response.redirect, 1000);
                            } else {
                                $('.status_msg').html(getErrorHTML(response.msg));
                            }
                        } catch (err) {
                            $('#login_form .status_msg').html(getErrorHTML(AJAX_ERR));
                        }
                    }
                });
                return false;
            }
        });
    });
</script>

<script>
    $(document).ready(function () {
        $("#change_password").validate({
            rules: {
                old_pass: {required: true},
                new_pass: {required: true, minlength: 5},
                conf_new_pass: {required: true, minlength: 5, equalTo: "#new_pass"},
            },
            highlight: function (element, errorClass) {
                $(element).css({borderColor: '#FF0000'});
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).css({borderColor: '#CCCCCC'});
            },
            //errorPlacement: function (error, element) {$.validator.messages.required = '';},
            messages: {conf_new_pass: "New pssword and Confirm password do not match!"},
            //invalidHandler: function(form, validator) {},
            submitHandler: function (form) {
                showprocess();
                $.ajax({
                    type: "POST",
                    url: "<?= base_url() ?>user/update_password",
                    data: $('#change_password').serialize(),
                    success: function (data) {
                        removeprocess();
                        try {
                            var response = jQuery.parseJSON(data);
                            if (response.status == 'success') {
                                $('.status_msg1').html(getSuccessHTML(response.msg));
                                windowRedirect(response.redirect, 1000);
                            } else {
                                $('.status_msg1').html(getErrorHTML(response.msg));
                            }
                        } catch (err) {
                            $('#login_form .status_msg1').html(getErrorHTML(AJAX_ERR));
                        }
                    }
                });
                return false;
            }
        });
    });
</script>
</html>
