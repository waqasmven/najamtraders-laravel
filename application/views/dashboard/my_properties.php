<?php
$this->load->view('frontend/dashboard/_dash_header');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="Ansonika">
        <title><?= $heading ?> - <?= SITETITLE ?></title>

        <!-- Favicons-->
        <link rel="shortcut icon" href="<?= base_url() . DASHTHEME ?>img/favicon.ico" type="image/x-icon">
        <link rel="apple-touch-icon" type="image/x-icon" href="<?= base_url() . DASHTHEME ?>img/apple-touch-icon-57x57-precomposed.png">
        <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="<?= base_url() . DASHTHEME ?>img/apple-touch-icon-72x72-precomposed.png">
        <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="<?= base_url() . DASHTHEME ?>img/apple-touch-icon-114x114-precomposed.png">
        <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="<?= base_url() . DASHTHEME ?>img/apple-touch-icon-144x144-precomposed.png">

        <!-- Bootstrap core CSS-->
        <link href="<?= base_url() . DASHTHEME ?>vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <!-- Main styles -->
        <link href="<?= base_url() . DASHTHEME ?>css/admin.css" rel="stylesheet">
        <!-- Icon fonts-->
        <link href="<?= base_url() . DASHTHEME ?>vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <!-- Plugin styles -->
        <link href="<?= base_url() . DASHTHEME ?>vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
        <!-- Your custom styles -->
        <link href="<?= base_url() . DASHTHEME ?>css/custom.css" rel="stylesheet">
    </head>

    <body class="fixed-nav sticky-footer" id="page-top">
        <?php
        $this->load->view('frontend/dashboard/_dash_nav');
        ?>
        <!-- /Navigation-->
        <div class="content-wrapper">
            <div class="container-fluid">
                <!-- Breadcrumbs-->
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="<?= base_url('dashboard') ?>">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item active"><?= $heading ?></li>
                </ol>
                <div class="box_general">
                    <div class="header_box">
                        <h2 class="d-inline-block"><?= $heading ?></h2>
                    </div>
                    <div class="list_general">
                        <ul>
                            <?php
                            foreach ($propertiesData as $property) {
                                $address = $property['addr_placeholder'];
                                $building_id = $property['id'];
                                ?>
                                <li>
                                    <small><?= $property['place_sub_type'] ?></small>
                                    <h4><?= $property['building_name'] ?></h4>
                                    <p><?= substr($property['building_desc'], 0, 100) ?>.....</p>
                                    <p>Neighbourhood: <?= $property['neighbourhood_name']?></p> 
                                    <p>Contact Details: <?= $property['contact_person']?>, Email: <a href="mailto:<?= $property['email']?>"><?= $property['email']?></a>, Phone: <?= $property['phone']?></p> 
                                    <p><a href="<?= base_url('dashboard/view-building/' . $building_id)?>" class="btn_1 gray"><i class="fa fa-fw fa-eye"></i> View Details</a></p>
                                    <ul class="buttons">
                                        <li><a href="<?= base_url('dashboard/addedit-building/' . $building_id)?>" class="btn_1 gray edit"><i class="fa fa-fw fa-pencil"></i> Edit</a></li>
                                    </ul>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid-->
        </div>

        <?php
        $this->load->view('frontend/dashboard/_dash_footer');
        ?>
        <script src="<?= base_url() . SITETHEME ?>assets/js/jquery-2.2.0.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
        <!-- Bootstrap core JavaScript-->
        <script src="<?= base_url() . DASHTHEME ?>vendor/jquery/jquery.min.js"></script>
        <script src="<?= base_url() . DASHTHEME ?>vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <!-- Core plugin JavaScript-->
        <script src="<?= base_url() . DASHTHEME ?>vendor/jquery-easing/jquery.easing.min.js"></script>
        <!-- Page level plugin JavaScript-->
        <script src="<?= base_url() . DASHTHEME ?>vendor/chart.js/Chart.js"></script>
        <script src="<?= base_url() . DASHTHEME ?>vendor/datatables/jquery.dataTables.js"></script>
        <script src="<?= base_url() . DASHTHEME ?>vendor/datatables/dataTables.bootstrap4.js"></script>
        <script src="<?= base_url() . DASHTHEME ?>vendor/jquery.selectbox-0.2.js"></script>
        <script src="<?= base_url() . DASHTHEME ?>vendor/retina-replace.min.js"></script>
        <script src="<?= base_url() . DASHTHEME ?>vendor/jquery.magnific-popup.min.js"></script>
        <!-- Custom scripts for all pages-->
        <script src="<?= base_url() . DASHTHEME ?>js/admin.js"></script>
        <!-- Custom scripts for this page-->
        <script src="<?= base_url() . DASHTHEME ?>js/admin-charts.js"></script>
        <script>
            $(document).ready(function () {
                $("#dash_my_properties").addClass(" active");
            });
        </script>
    </body>
</html>
